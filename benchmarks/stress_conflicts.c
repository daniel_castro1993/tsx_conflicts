// #define DISABLE_LOCK

#include "tm.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef SAMPLES 
#define SAMPLES 10000
#endif

#ifndef DROP_INIT_SAMPLES 
#define DROP_INIT_SAMPLES 10
#endif

#ifndef DROP_END_SAMPLES 
#define DROP_END_SAMPLES 1000
#endif

#ifndef MAX_THREADS 
#define MAX_THREADS 64
#endif

#ifndef PLOT_FILE 
#define PLOT_FILE "./conf_bench.txt"
#endif

#if PROFILE == 1
unsigned long long nb_aborts[MAX_THREADS], time_aborts[MAX_THREADS],
time_commit[MAX_THREADS], time_xend[MAX_THREADS], time_xbegin[MAX_THREADS],
time_fallback[MAX_THREADS], ts_notx[MAX_THREADS], time_notx[MAX_THREADS];
#endif

#define TRANSACTION(pool, d, l, pw, seed) ({ \
    register int i; \
    register int aux = 1; /* in Intel remove this register */ \
    for (i = 0; i < l; ++i) { \
        if (IS_WRITE(pw, seed)) { \
            ALIGNED_RANDOM_INC(int, pool, seed, d, 1); \
            continue; \
        } \
        aux += ALIGNED_RANDOM_READ(int, pool, seed, d); \
    } \
})

static int budget = 4, threads = 2, D = 1000, L = 10;
static double PW = 0.5;
static int *pool;
static unsigned long long *times;

void run_txs(int tid, int samples) {
    int i;
    register int* p = pool;
    register double pw = PW;
    register int l = L;
    register int d = D;
    register unsigned seed = (unsigned) rdtsc();

    for (i = 0; i < samples; ++i) {
#if PROFILE == 1
        TM_begin_time_counters(tid, nb_aborts[tid], time_aborts[tid],
            time_commit[tid], time_xend[tid],
            time_xbegin[tid], time_fallback[tid],
            ts_notx[tid], time_notx[tid])
#else
        TM_begin(tid)
#endif
            TRANSACTION(p, d, l, pw, seed);
#if PROFILE == 1
        TM_commit_time_counters(tid, nb_aborts[tid], time_aborts[tid],
            time_commit[tid], time_xend[tid],
            time_xbegin[tid], time_fallback[tid],
            ts_notx[tid], time_notx[tid])
#else
        TM_commit(tid)
#endif
    }
}

void *run_xacts(void *arg) {
    int tid = *((int*) arg);
    unsigned long long ts1, ts2;

    TM_set_record_errors(tid, 0);
    run_txs(tid, DROP_INIT_SAMPLES);
    TM_set_record_errors(tid, 1);
    ts1 = rdtsc();
    run_txs(tid, SAMPLES); // actual experiment
    ts2 = rdtsc();
    TM_set_record_errors(tid, 0);
    run_txs(tid, DROP_END_SAMPLES);

    times[tid] = ts2 - ts1;

    return NULL;
}

int main(int argc, char ** argv) {
    int i = 1;

    while (i < argc) {
        if (strcmp(argv[i], "L") == 0) {
            L = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "D") == 0) {
            D = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "BUDGET") == 0) {
            budget = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "THREADS") == 0) {
            threads = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "PW") == 0) {
            PW = atof(argv[i + 1]);
        }
        i += 2;
    }

    times = (unsigned long long*) malloc(threads * sizeof (unsigned long long));

#if PROFILE == 1
    //    nb_aborts = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    //    time_aborts = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    //    time_commit = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    //    time_xend = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    //    time_xbegin = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    //    time_fallback = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    //    ts_notx = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    //    time_notx = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
#endif

    ALLOC_FN(pool, int, D);

    printf(" =========================================== \n");
    printf(" INPUT PARAMETERS: \n");
    printf("          THREADS: %i\n", threads);
    printf("           BUDGET: %i\n", budget);
    printf("                D: %i\n", D);
    printf("                L: %i\n", L);
    printf("               PW: %f\n", PW);
    printf(" GRANULE POOL START ADDRESS: %p\n", pool);
    printf(" =========================================== \n");

    for (i = 0; i < D; ++i) {
        ALIGNED_WRITE(int, pool, i, i);
    }

    TM_exec(threads, budget, D, run_xacts);

    double avg_time = 0;
    int successes = TM_get_error_count(SUCCESS);
    int aborts = TM_get_error_count(ABORT);
    int fallbacks = TM_get_error_count(FALLBACK);
    int conflicts = TM_get_error_count(CONFLICT);
    int capacities = TM_get_error_count(CAPACITY);
    int others = TM_get_error_count(OTHER);
    int explicit = TM_get_error_count(EXPLICIT);

    for (i = 0; i < threads; ++i) {
        avg_time += times[i];
    }

    avg_time /= (double) threads;

    double X = (double) (SAMPLES * threads) / avg_time;
    double P_A = (double) aborts / (double) (successes + aborts);
    double a_CONFLICTS = (double) conflicts / (double) aborts;
    double a_CAPACITY = (double) capacities / (double) aborts;

    printf("       COMMITS: %i\n", successes);
    printf("        ABORTS: %i\n", aborts);
    printf("         OTHER: %i\n", others);
    printf("      EXPLICIT: %i\n", explicit);
    printf("     CONFLICTS: %i\n", conflicts);
    printf("    CAPACITIES: %i\n", capacities);
    printf("     FALLBACKS: %i\n", fallbacks);
    printf(" =========================================== \n");
#if PROFILE == 1
    double avg_time_xend = 0, avg_time_aborts = 0, avg_time_commit = 0,
        avg_time_xbegin = 0, avg_time_fallback = 0, avg_time_notx = 0;
    for (i = 0; i < threads; ++i) {
        avg_time_xend += time_xend[i];
        avg_time_aborts += time_aborts[i];
        avg_time_commit += time_commit[i];
        avg_time_xbegin += time_xbegin[i];
        avg_time_fallback += time_fallback[i];
        avg_time_notx += time_notx[i];
    }

    avg_time_xend /= (double) successes;
    avg_time_commit /= (double) successes;
    avg_time_aborts /= (double) aborts;
    avg_time_xbegin /= (double) (successes + aborts);
    avg_time_fallback /= (double) fallbacks;
    avg_time_notx /= (double) (fallbacks + successes);

    printf("     TIME_XEND: %e\n", avg_time_xend);
    printf("   TIME_COMMIT: %e\n", avg_time_commit);
    printf("    TIME_ABORT: %e\n", avg_time_aborts);
    printf("   TIME_XBEGIN: %e\n", avg_time_xbegin);
    printf(" TIME_FALLBACK: %e\n", avg_time_fallback);
    printf("     TIME_NOTX: %e\n", avg_time_notx);
#endif
    printf(" =========================================== \n");
    printf("    THROUGHPUT: %e\n", X);
    printf("           P_A: %f\n", P_A);
    printf(" =========================================== \n");

    FILE *gnuplot_file = fopen(PLOT_FILE, "a");
    fseek(gnuplot_file, 0, SEEK_END);
    if (ftell(gnuplot_file) < 8) {
        // not created yet
#if PROFILE == 1
        fprintf(gnuplot_file, "#\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s"
            "\t%s\t%s\n", "THREADS", "BUDGET", "D", "L", "PW", "X", "P_A",
            "TIME_XEND", "TIME_COMMIT", "TIME_ABORT", "TIME_XBEGIN",
            "TIME_FALLBACK", "TIME_NOTX");
#else
        fprintf(gnuplot_file, "#\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
            "THREADS", "BUDGET", "D", "L", "PW", "X", "P_A",
            "a_CONFLICTS", "a_CAPACITY");
#endif
    }
#if PROFILE == 1
    fprintf(gnuplot_file, "\t%i\t%i\t%i\t%i\t%f\t%e\t%e\t%e\t%e\t%e\t%e"
        "\t%e\t%e\t%e\t%e\n", threads, budget, D, L, PW, X, P_A, a_CONFLICTS,
        a_CAPACITY, avg_time_xend, avg_time_commit, avg_time_aborts,
        avg_time_xbegin, avg_time_fallback, avg_time_notx);
#else
    fprintf(gnuplot_file, "\t%i\t%i\t%i\t%i\t%f\t%e\t%e\t%e\t%e\n",
        threads, budget, D, L, PW, X, P_A, a_CONFLICTS, a_CAPACITY);
#endif
    fclose(gnuplot_file);

    // validation check
    int total = 0;

    for (i = 0; i < D; ++i) {
        int val = ALIGNED_READ(int, pool, i) - i;
        total += val;
    }

    printf("total: %i (%f)\n", total, PW * L * threads
        * (SAMPLES + DROP_END_SAMPLES + DROP_INIT_SAMPLES));
}
