#include <signal.h>

#include "tm.h"

#ifndef MAX_THREADS
#define MAX_THREADS 2
#endif
#define MULT_TIME 100

#define POOL_SIZE 100000
#ifndef NB_ACCESSES
#define NB_ACCESSES 10
#endif
padded_scalar_t* shared_int;

int sync_enter, sync_exit;
int read_values[MAX_THREADS][EXPERIMENT_SAMPLES];

int errors[MAX_THREADS][ERRORS_T_SIZE];

void * T(void* me_ptr)
{
    int i;
    padded_scalar_t cpy, *si = shared_int;
    int me = *((int*) me_ptr), index = me - 1;
    register int j, k;

    //	printf("W%i -> spin %i / write / spin %i\n", me, me * MULT_TIME / 2,
    //			MAX_THREADS * MULT_TIME - (index + me) * MULT_TIME / 2);

    for (i = 0; i < EXPERIMENT_SAMPLES; i++) {

        __sync_add_and_fetch(&sync_enter, 1);
        while (!__sync_bool_compare_and_swap(&sync_enter,
                                             MAX_THREADS * (i + 1),
                                             MAX_THREADS * (i + 1)));

        // W1 spins 5000     |  W2 spins 10000
        // W1 writes         |
        // W1 spins 15000    |  W2 writes
        //                   |  W2 spins 5000
        //                   |  W2 commit
        // W1 commit         |

        for (j = 0; j < me * MULT_TIME; j++);

        START_TRANSAC(errors[index]);

        for (k = 0; k < NB_ACCESSES; ++k) {
            si[index + (k * MAX_THREADS)].counter = me;
            for (j = 0; j < MULT_TIME; j++);
        }

        END_TRANSAC(errors[index]);

        __sync_add_and_fetch(&sync_exit, 1);
        while (!__sync_bool_compare_and_swap(&sync_exit,
                                             MAX_THREADS * (i + 1),
                                             MAX_THREADS * (i + 1)));
    }

    return NULL;
}

int main()
{
    pthread_t thread_w[MAX_THREADS];
    pthread_attr_t attrs[MAX_THREADS];
    cpu_set_t cpuset[MAX_THREADS];
    void *res1;
    int i, j;
    int W_ID[MAX_THREADS];
    int cpuid = 0;

    shared_int = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * POOL_SIZE);
    //    memset(shared_int, 0, sizeof (padded_scalar_t));

    shared_int->counter = 1;

    for (j = 0; j < MAX_THREADS; ++j) {
        W_ID[j] = j + 1;
        CPU_ZERO(&(cpuset[j]));
        CPU_SET(cpuid, &(cpuset[j]));
        cpuid++; // different physical cores I guess
        pthread_attr_init(&(attrs[i]));
        pthread_attr_setaffinity_np(&(attrs[i]),
                                    sizeof (cpu_set_t), &(cpuset[j]));

        pthread_create((pthread_t*) &(thread_w[j]),
                       &(attrs[i]), T, (void*) &(W_ID[j]));
    }
    for (j = 0; j < MAX_THREADS; ++j) {
        pthread_join(thread_w[j], &res1);
    }

    check_file();
    for (j = 0; j < MAX_THREADS; ++j) {
        fprintf(fp, "W%i: ", j + 1);
        for (i = 0; i < ERRORS_T_SIZE; ++i) {
            fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
                    errors[j][i], (double) errors[j][i] /
                    (double) EXPERIMENT_SAMPLES * 100.0);
        }
        fprintf(fp, "\n");
    }
    fclose(fp);

    for (j = 0; j < MAX_THREADS; ++j) {
        fprintf(stdout, "W%i: ", j + 1);
        for (i = 0; i < ERRORS_T_SIZE; ++i) {
            fprintf(stdout, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
                    errors[j][i], (double) errors[j][i] /
                    (double) EXPERIMENT_SAMPLES * 100.0);
        }
        fprintf(stdout, "\n");
    }

    //	printf("final value counter: %i\n", (int) shared_int->counter);

    /* Last thing that main() should do */
    pthread_exit(NULL);

    return 0;
}
