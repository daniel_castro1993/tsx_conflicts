#include "tm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LARGE_NUMBER  0x2FFFFF

int errors_1[ERRORS_T_SIZE];

#ifndef WAY_SIZE 
#define WAY_SIZE 8
#endif

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t* mem_pool;

int max = 0;
int sum_i = 0;

void * W(void * arg) {
	register padded_scalar_t* begin = (padded_scalar_t*) arg;
	int status;
	register int i = 0;
	register int j = 0;
	int nb_accesses = 0, nb_capa_in_row = 0, nb_other_in_row = 0;

	i = 1;

	status = -1;
	while (nb_capa_in_row < 3 && nb_other_in_row < 5) {

		nb_accesses++;
		j = nb_accesses * WAY_SIZE;
		status = _xbegin();
		if (status != _XBEGIN_STARTED) {
			if (status == _XABORT_CAPACITY) {
				nb_capa_in_row++;
				nb_accesses--;
			} else {
				nb_other_in_row++;
			}
			TSX_ERROR_INC(status, errors_1);
			continue;
		}

		while (j > 0) {
			j -= WAY_SIZE;
			i += begin[j].counter;
		}

		_xend();
		nb_other_in_row = 0;
		nb_capa_in_row = 0;
		TSX_ERROR_INC(status, errors_1);
	}

	if (!(nb_other_in_row < 5) && nb_capa_in_row < 3) {
		printf("WARN: exceeded numb of others, nb_accesses=%i\n", nb_accesses);
	}

	if ((nb_accesses - 1) > max) {
		max = (nb_accesses - 1);
	}

	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w;
	cpu_set_t cpuset;

	int i;
	void * res;

	padded_scalar_t * begin_pool, * end_pool, * it;

	printf("USING ASSOC=%i\n\n", WAY_SIZE);

	mem_pool = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);
	begin_pool = (padded_scalar_t*) mem_pool;
	end_pool = begin_pool + LARGE_NUMBER;

	printf("START ADDR=%p, END ADDR=%p\n", begin_pool, end_pool);

	memset(begin_pool, 1, sizeof (padded_scalar_t) * LARGE_NUMBER);

	printf("\n\n");

	CPU_ZERO(&cpuset);
	CPU_SET(0, &cpuset);

	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpu_set_t), &cpuset);

	CPU_ZERO(&cpuset);
	CPU_SET(1, &cpuset);
	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpuset), &cpuset);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		pthread_create(&thread_w, NULL, W, (void *) begin_pool);
		pthread_join(thread_w, &res);
	}

	check_file();
	fprintf(fp, "W (WAY_SIZE=%i): ", WAY_SIZE);
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i]
				/ (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n");
	fprintf(fp, "max reads: %i\n", max);
	fprintf(fp, "\n\n");
	fclose(fp);

	printf("max nb_accesses = %i\n", max);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
