#!/bin/bash

rm *.txt

for t in 1 2 3 4 5 6 7 8
do
	for p in 1.0 0.9 0.5 0.1 0.0
	do
		for d in 8192
		do
			cd .. ;
			make clean && make SAMPLES=10000 USE_P8=1 FILE=./f_conf_L_THRS_$t.D_$d.pw$p.txt ;
			cd results ;
			for i in {1..50}
			do
			  ../stress_false_conflicts.out THREADS $t BUDGET 6 D $d L $i PW $p ;
			done
		done
	done
done
