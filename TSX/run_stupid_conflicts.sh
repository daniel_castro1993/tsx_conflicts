#!/bin/bash

for a in {1..10}
do
  for p in 1000 5000 10000 20000 100000
  do
    gcc -g -O0 -mrtm ./conflicts/case_NO_CONF2.c -o ./conflicts/case_NO_CONF2.out -lpthread -lrt -DPLOT_FILE="\"./conflicts/case_NO_CONF_2.out.plot\"" -I. -DNB_ACCESSES=$a -DMAX_THREADS=4 -DOFFSET=1 -DPOOL_SIZE=$p
    for m in {1..10}
    do
      ./conflicts/case_NO_CONF2.out ; wait ;
    done
  done
  wait ;
done
