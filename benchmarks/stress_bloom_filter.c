#define DISABLE_LOCK

#include "tm.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define __USE_XOPEN2K /* pthread_rwlock_t & pthread_barrier_t */
#include <pthread.h>

#ifndef SPIN 
#define SPIN 1000
#endif

#ifndef SAMPLES 
#define SAMPLES 50000
#endif

#ifndef DROP_INIT_SAMPLES 
#define DROP_INIT_SAMPLES 10
#endif

#ifndef DROP_END_SAMPLES 
#define DROP_END_SAMPLES 1000
#endif

#ifndef POOL_OFFSET 
#define POOL_OFFSET 8
#endif

#ifndef EXTRA_POOL 
#define EXTRA_POOL (131072 << 1)
#endif

#ifndef PLOT_FILE 
#define PLOT_FILE "./conf_bench.txt"
#endif

#if PROFILE == 1
unsigned long long *nb_aborts, *time_aborts, *time_commit,
*time_xend, *time_xbegin, *time_fallback, *ts_notx, *time_notx;
#endif

// last thread is the shooter

#define DISTANCE_CL  16 // CL_SIZE / sizeof(int)
#define SHOOTER_IDX    (i*2 + 2) * DISTANCE_CL
#define READER_IDX     (i*2 + 1) * DISTANCE_CL

#define TRANSACTION_R(tid, nb_threads, pool, d, l1, l2) ({ \
    register int i; \
    int aux /* = ALIGNED_READ(int, pool, 0)*/; \
    register int aux2; \
    if (tid == (nb_threads - 1)) { \
        for (i = 0; i < SPIN/2; ++i); \
        /* ALIGNED_RANDOM_INC(int, pool, seed, d, 1); */ \
        for (i = 0; i < l2; ++i) { \
            pool[SHOOTER_IDX]++; \
        } \
    } \
    else { \
        for (i = 0; i < l1; ++i) { \
            aux += pool[READER_IDX]; \
        } \
        for (i = 0; i < SPIN; ++i); \
    } \
})

static pthread_barrier_t barrier;
static int budget = 4, threads = 2, D = 1000, L = 10, L2 = 10;
static double PW = 0.5;
static int *pool, *r_pool, *r_pool2, *w_pool;
static unsigned long long *times, shooter_dead = 0, counter_pool = 0;

void run_txs(int tid, int samples) {
    int i;
    register int* p;
    register int l1 = L;
    register int l2 = L2;
    register int d = D;
    register int t_id = tid;
    register int nb_threads = threads;

    int inc_cl = 64;
    
    p = pool; // shooter

    for (i = 0; i < samples; ++i) {

        pthread_barrier_wait(&barrier);
        if (tid == (nb_threads - 1)) {
            // cannot pass EXTRA_POOL * DISTANCE_CL
            if (counter_pool < EXTRA_POOL) {
                pool[D * DISTANCE_CL] = D + counter_pool;
                pool += DISTANCE_CL * inc_cl; // use lock
                counter_pool += inc_cl;
            } else {
                pool -= DISTANCE_CL * EXTRA_POOL; // use lock
                counter_pool = 0;
            }

//            for (i = 0; i < D; ++i) {
//                ALIGNED_WRITE(int, pool, i, i);
//            }
        }
        pthread_barrier_wait(&barrier);

        register int TM_budget_var = TM_get_budget();
        TM_STATUS_TYPE TM_status_var;
        if (START_TRANSACTION(TM_status_var) != CODE_SUCCESS) {
            TM_update_budget(tid, TM_budget_var, TM_status_var); // updates errors
            if (tid == (nb_threads - 1)) {
                shooter_dead++;
            }
            continue;
        }

        //        TM_begin(tid)
        TRANSACTION_R(t_id, nb_threads, p, d, l1, l2);
        //        TM_commit(tid)

        TM_COMMIT();
        TM_update_budget(tid, TM_budget_var, TM_status_var); // updates errors
    }
}

void *run_xacts(void *arg) {
    int tid = *((int*) arg);
    unsigned long long ts1, ts2;

    TM_set_record_errors(tid, 0);
    run_txs(tid, DROP_INIT_SAMPLES);
    if (tid != (threads - 1)) {
        TM_set_record_errors(tid, 1);
    }
    ts1 = rdtsc();
    run_txs(tid, SAMPLES); // actual experiment
    if (tid != (threads - 1)) {
        printf("shooter dead %i times!\n", shooter_dead);
    }
    ts2 = rdtsc();
    TM_set_record_errors(tid, 0);
    run_txs(tid, DROP_END_SAMPLES);

    times[tid] = ts2 - ts1;

    return NULL;
}

int main(int argc, char ** argv) {
    int i = 1;

    while (i < argc) {
        if (strcmp(argv[i], "L") == 0) {
            L = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "D") == 0) {
            D = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "L2") == 0) {
            L2 = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "BUDGET") == 0) {
            budget = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "THREADS") == 0) {
            threads = atoi(argv[i + 1]);
        } else if (strcmp(argv[i], "PW") == 0) {
            PW = atof(argv[i + 1]);
        }
        i += 2;
    }

    times = (unsigned long long*) malloc(threads * sizeof (unsigned long long));
    ALLOC_FN(pool, int, D + EXTRA_POOL); // readers + shooter + readers

    // HACK THE POOL
    r_pool = pool;
    w_pool = pool; // pool + (D * CACHE_LINE_SIZE / sizeof (int)); //
    r_pool2 = pool; // pool + (2 * D * CACHE_LINE_SIZE / sizeof (int)); //

    if (pthread_barrier_init(&barrier, NULL, threads)) {
        printf("Barrier init fail\n");
        return EXIT_FAILURE;
    }

#if PROFILE == 1
    nb_aborts = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    time_aborts = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    time_commit = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    time_xend = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    time_xbegin = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    time_fallback = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    ts_notx = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
    time_notx = (unsigned long long *) calloc(threads, sizeof (unsigned long long));
#endif

    printf(" =========================================== \n");
    printf(" INPUT PARAMETERS: \n");
    printf("          THREADS: %i\n", threads);
    printf("           BUDGET: %i\n", budget);
    printf("                D: %i\n", D);
    printf("                L: %i\n", L);
    printf("               L2: %i\n", L2);
    printf(" GRANULE POOL START ADDRESS: %p\n", pool);
    printf(" =========================================== \n");

    for (i = 0; i < D; ++i) {
        ALIGNED_WRITE(int, pool, i, i);
        //        ALIGNED_WRITE(int, pool, i + D, i);
        //        ALIGNED_WRITE(int, pool, i + 2 * D, i);
    }

    //    for (i = 0; i < L; ++i) {
    //        unsigned long long ptr_s = (unsigned long long) &(pool[SHOOTER_IDX]);
    //        unsigned long long ptr_r = (unsigned long long) &(pool[READER_IDX]);
    //
    //        printf("Bits in common: %o\n", ptr_s ^ ptr_r);
    //    }

    TM_exec(threads, budget, D, run_xacts);

    double avg_time = 0;
    int successes = TM_get_error_count(SUCCESS);
    int aborts = TM_get_error_count(ABORT);
    int fallbacks = TM_get_error_count(FALLBACK);
    int conflicts = TM_get_error_count(CONFLICT);
    int capacities = TM_get_error_count(CAPACITY);
    int others = TM_get_error_count(OTHER);
    int explicit = TM_get_error_count(EXPLICIT);

    for (i = 0; i < threads; ++i) {
        avg_time += times[i];
    }

    avg_time /= (double) threads;

    double X = (double) (SAMPLES * threads) / avg_time;
    double P_A = (double) aborts / (double) (successes + aborts);
    double a_CONFLICTS = (double) conflicts / (double) aborts;
    double a_CAPACITY = (double) capacities / (double) aborts;

    printf("       COMMITS: %i\n", successes);
    printf("        ABORTS: %i\n", aborts);
    printf("         OTHER: %i\n", others);
    printf("      EXPLICIT: %i\n", explicit);
    printf("     CONFLICTS: %i\n", conflicts);
    printf("    CAPACITIES: %i\n", capacities);
    printf("     FALLBACKS: %i\n", fallbacks);
    printf(" =========================================== \n");
#if PROFILE == 1
    double avg_time_xend = 0, avg_time_aborts = 0, avg_time_commit = 0,
        avg_time_xbegin = 0, avg_time_fallback = 0, avg_time_notx = 0;
    for (i = 0; i < threads; ++i) {
        avg_time_xend += time_xend[i];
        avg_time_aborts += time_aborts[i];
        avg_time_commit += time_commit[i];
        avg_time_xbegin += time_xbegin[i];
        avg_time_fallback += time_fallback[i];
    }

    avg_time_xend /= (double) successes;
    avg_time_commit /= (double) successes;
    avg_time_aborts /= (double) aborts;
    avg_time_xbegin /= (double) (successes + aborts);
    avg_time_fallback /= (double) fallbacks;

    printf("     TIME_XEND: %e\n", avg_time_xend);
    printf("   TIME_COMMIT: %e\n", avg_time_commit);
    printf("    TIME_ABORT: %e\n", avg_time_aborts);
    printf("   TIME_XBEGIN: %e\n", avg_time_xbegin);
    printf(" TIME_FALLBACK: %e\n", avg_time_fallback);
#endif
    printf(" =========================================== \n");
    printf("    THROUGHPUT: %e\n", X);
    printf("           P_A: %f\n", P_A);
    printf(" =========================================== \n");

    FILE *gnuplot_file = fopen(PLOT_FILE, "a");
    fseek(gnuplot_file, 0, SEEK_END);
    if (ftell(gnuplot_file) < 8) {
        // not created yet
#if PROFILE == 1
        fprintf(gnuplot_file, "#\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s"
            "\t%s\t%s\n", "THREADS", "BUDGET", "D", "L", "PW", "X", "P_A",
            "TIME_XEND", "TIME_COMMIT", "TIME_ABORT", "TIME_XBEGIN",
            "TIME_FALLBACK", "TIME_NOTX");
#else
        fprintf(gnuplot_file, "#\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
            "THREADS", "BUDGET", "D", "L", "PW", "X", "P_A",
            "a_CONFLICTS", "a_CAPACITY");
#endif
    }
#if PROFILE == 1
    fprintf(gnuplot_file, "\t%i\t%i\t%i\t%i\t%f\t%e\t%e\t%e\t%e\t%e\t%e"
        "\t%e\t%e\t%e\t%e\n", threads, budget, D, L, PW, X, P_A, a_CONFLICTS,
        a_CAPACITY, avg_time_xend, avg_time_commit, avg_time_aborts,
        avg_time_xbegin, avg_time_fallback, avg_time_notx);
#else
    fprintf(gnuplot_file, "\t%i\t%i\t%i\t%i\t%f\t%e\t%e\t%e\t%e\n",
        threads, budget, D, L, PW, X, P_A, a_CONFLICTS, a_CAPACITY);
#endif
    fclose(gnuplot_file);

    pthread_barrier_destroy(&barrier);
}
