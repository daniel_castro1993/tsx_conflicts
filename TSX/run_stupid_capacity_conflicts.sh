#!/bin/bash

for n in 0x10000 0x15000 0x20000 0x25000 0x30000 0x35000 0x40000 0x45000 0x50000 0x55000 0x60000 0x80000
do
  gcc -O0 -mrtm ./capacity/TSX_stress_cap.c -o assoc.out -lpthread -lrt -I. -DPLOT_FILE="\"stress_cap.txt\"" -march=native -DEXPERIMENT_SAMPLES=50000 -DLARGE_NUMBER=$n -DOFFSET=$n -std=c11 # -S
  for m in 25 50 100 150 200 250 300 350 400 500
  do
    ./assoc.out PROB_READ 1.0 NUM_ACCESSES $m ; wait ;
  done ;
  wait ;
done
