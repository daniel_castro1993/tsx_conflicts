#include "P8_tm.h"

#define MAX_THREADS 2
#define MULT_TIME 10000

padded_scalar_t* shared_int;

int sync_enter, sync_exit;
int read_values[MAX_THREADS][EXPERIMENT_SAMPLES];

int errors[MAX_THREADS][ERRORS_T_SIZE];

void * T(void* me_ptr)
{
	int i, j;
	padded_scalar_t cpy, *si = shared_int;
	int me = *((int*) me_ptr), index = me - 1;
	int is_write = index % 2;
	TM_buff_type TM_buff;
	int started = 1;

	printf("%c%i -> spin %i / access / spin %i\n", is_write == 1 ? 'W' : 'R',
		me, me * MULT_TIME / 2, MULT_TIME);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		started = 1;

		__sync_add_and_fetch(&sync_enter, 1);
		while (!__sync_bool_compare_and_swap(&sync_enter,
											MAX_THREADS * (i + 1),
											MAX_THREADS * (i + 1)));

		// R spins 5000     |  W spins 10000
		// R reads          |
		// R spins 10000    |  W writes
		//                  |  W spins 10000
		// R commit         |

		if (is_write == 0) {
			if (__TM_begin(TM_buff) != _HTM_TBEGIN_STARTED) {
				errors[index][ABORT]++;
				P8_ERROR_INC(TM_buff, errors[index]);
				started = 0;
			}
		}
		else {
			for (j = 0; j < 100; j++);
		}

		if (started == 1) {

			for (j = 0; j < me * MULT_TIME / 2; j++);

			if (is_write == 1) {
				si->counter++;
			}
			else {
				cpy.counter = si->counter;
			}

			for (j = 0; j < MULT_TIME; j++);

			if (is_write == 0) {
				__TM_end();
				errors[index][SUCCESS]++;
			}
			else {
				for (j = 0; j < 100; j++);
			}
		}

		__sync_add_and_fetch(&sync_exit, 1);
		while (!__sync_bool_compare_and_swap(&sync_exit,
											MAX_THREADS * (i + 1),
											MAX_THREADS * (i + 1)));
	}

	return NULL;
}

int main()
{
	pthread_t thread_w[MAX_THREADS];
	pthread_attr_t attr[THREADS__];
	cpu_set_t cpuset[MAX_THREADS];
	void *res1;
	int i, j;
	int W_ID[MAX_THREADS];
	int cpuid = 0;

	shared_int = (padded_scalar_t*) malloc(sizeof (padded_scalar_t));
	memset(shared_int, 0, sizeof (padded_scalar_t));

	shared_int->counter = 1;

	for (j = 0; j < MAX_THREADS; ++j) {
		W_ID[j] = j + 1;
		CPU_ZERO(&(cpuset[j]));
		CPU_SET((i * 8) % 80, &(cpuset[j]));

		pthread_attr_setaffinity_np(&(attr[i]),
				sizeof (cpu_set_t), &(cpuset[i]));

		pthread_create((pthread_t*) &(thread_w[j]),
				&(attr[i]), T, (void*) &(W_ID[j]));
	}
	for (j = 0; j < MAX_THREADS; ++j) {
		pthread_join(thread_w[j], &res1);
	}

	check_file();
	for (j = 0; j < MAX_THREADS; ++j) {
		fprintf(fp, "%c%i: ", (j % 2) == 1 ? 'W' : 'R', j + 1);
		for (i = 0; i < ERRORS_T_SIZE; ++i) {
			fprintf(fp, "%s=[ %i/ %.3f%%] ", P8_ERROR_INDEX_STR(i),
					errors[j][i], (double) errors[j][i] /
					(double) EXPERIMENT_SAMPLES * 100.0);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);

	printf("final value counter: %i\n", shared_int->counter);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
