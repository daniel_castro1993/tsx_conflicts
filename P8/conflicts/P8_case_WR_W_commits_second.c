#include "P8_tm.h"

#define MAX_THREADS 2
#define MULT_TIME 10000

#define GRANULE_POOL 1000

padded_scalar_t* shared_int;

int sync_enter, sync_exit;
int read_values[MAX_THREADS][EXPERIMENT_SAMPLES];

int errors[MAX_THREADS][ERRORS_T_SIZE];

void * T(void* me_ptr) {
	int i, j;
	padded_scalar_t cpy, *si = shared_int;
	int me = *((int*) me_ptr), index = me - 1;
	int is_write = me % 2;

	printf("%c%i -> spin %i / write / spin %i\n", is_write == 1 ? 'W' : 'R',
			me, me * MULT_TIME / 2, MAX_THREADS * MULT_TIME
			- (index + me) * MULT_TIME / 2);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {

		__sync_add_and_fetch(&sync_enter, 1);
		while (!__sync_bool_compare_and_swap(&sync_enter,
				MAX_THREADS * (i + 1),
				MAX_THREADS * (i + 1)));

		// W spins 5000     |  R spins 10000
		// W writes         |
		// W spins 10000    |  R reads
		//                  |  R spins 5000
		//                  |  R commit
		// W commit         |

		START_TRANSAC(errors[index], true);

		for (j = 0; j < me * MULT_TIME / 2; j++);

		if (is_write == 1) {
			si[0].counter = me;
			si[1].counter = me;
			si[2].counter = me;
			si[3].counter = me;
			si[4].counter = me;
		} else {
			cpy.counter = si[3].counter;
		}

		for (j = 0; j < MAX_THREADS * MULT_TIME
				- (index + me) * MULT_TIME / 2; j++);

		END_TRANSAC(errors[index], true);

		__sync_add_and_fetch(&sync_exit, 1);
		while (!__sync_bool_compare_and_swap(&sync_exit,
				MAX_THREADS * (i + 1),
				MAX_THREADS * (i + 1)));
	}

	return NULL;
}

int main() {
	pthread_t thread_w[MAX_THREADS];
	pthread_attr_t attr[MAX_THREADS];
	cpu_set_t cpuset[MAX_THREADS];
	void *res1;
	int i, j;
	int W_ID[MAX_THREADS];
	int cpuid = 0;

	shared_int = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * GRANULE_POOL);
	memset(shared_int, 0, sizeof (padded_scalar_t));

	for (i = 0; i < GRANULE_POOL; i += CACHE_LINE_SIZE) {
		shared_int->counter = 1;
	}

	for (j = 0; j < MAX_THREADS; ++j) {
		W_ID[j] = j + 1;
		CPU_ZERO(&(cpuset[j]));
		CPU_SET((j * 8) % 80, &(cpuset[j]));
		pthread_attr_init(&(attr[j]));
		pthread_attr_setaffinity_np(&(attr[j]),
				sizeof (cpu_set_t), &(cpuset[j]));

		pthread_create((pthread_t*) &(thread_w[j]),
				&(attr[j]), T, (void*) &(W_ID[j]));
	}
	for (j = 0; j < MAX_THREADS; ++j) {
		pthread_join(thread_w[j], &res1);
	}

	check_file();
	for (j = 0; j < MAX_THREADS; ++j) {
		fprintf(fp, "%c%i: ", (j + 1 % 2) == 1 ? 'W' : 'R', j + 1);
		for (i = 0; i < ERRORS_T_SIZE; ++i) {
			fprintf(fp, "%s=[ %i/ %.3f%%] ", P8_ERROR_INDEX_STR(i),
					errors[j][i], (double) errors[j][i] /
					(double) EXPERIMENT_SAMPLES * 100.0);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);

	printf("final value counter: %i\n", shared_int->counter);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
