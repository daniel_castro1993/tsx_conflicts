#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifndef SAMPLES
#define SAMPLES 1000000
#endif

#ifndef SIZE_CACHE_LINE_L3
#define SIZE_CACHE_LINE_L3 64
#endif

#ifndef WAY_ASSOC_L3
#define WAY_ASSOC_L3 8
#endif

#ifndef NB_SETS_L3
#define NB_SETS_L3 2048
#endif

#define POOL_SIZE NB_SETS_L3 * 100

typedef struct _padded_scalar {
	volatile uint64_t counter;
	uint8_t suffixPadding[SIZE_CACHE_LINE_L3 - sizeof (uint64_t)];
} __attribute__ ((aligned(SIZE_CACHE_LINE_L3))) padded_scalar_t;

padded_scalar_t *pool;

// Given a physical memory address, this hashes the address and
// returns the number of the cache slice that the address maps to.
//
// This assumes a 2-core Sandy Bridge CPU.
//
// "bad_bit" lets us test whether this hash function is correct.  It
// inverts whether the given bit number is included in the set of
// address bits to hash.

int get_cache_slice(uint64_t phys_addr, int bad_bit) {
	// On a 4-core machine, the CPU's hash function produces a 2-bit
	// cache slice number, where the two bits are defined by "h1" and
	// "h2":
	//
	// h1 function:
	//   static const int bits[] = { 18, 19, 21, 23, 25, 27, 29, 30, 31 };
	// h2 function:
	//   static const int bits[] = { 17, 19, 20, 21, 22, 23, 24, 26, 28, 29, 31 };
	//
	// This hash function is described in the paper "Practical Timing
	// Side Channel Attacks Against Kernel Space ASLR".
	//
	// On a 2-core machine, the CPU's hash function produces a 1-bit
	// cache slice number which appears to be the XOR of h1 and h2.

	// XOR of h1 and h2:
	static const int h1[] = { 18, 19, 21, 23, 25, 27, 29, 30, 31 };
	static const int h2[] = { 17, 19, 20, 21, 22, 23, 24, 26, 28, 29, 31 };

	int count = sizeof (bits) / sizeof (bits[0]);
	int hash_bit_1 = 0;
	int hash_bit_2 = 0;
	int hash;
	for (int i = 0; i < count; i++) {
		hash_bit_1 ^= (phys_addr >> h1[i]) & 1;
		hash_bit_2 ^= (phys_addr >> h2[i]) & 2;
	}
	if (bad_bit != -1) {
		hash_bit_1 ^= (phys_addr >> bad_bit) & 1;
		hash_bit_2 ^= (phys_addr >> bad_bit) & 2;
	}
	hash = hash_bit_1 | hash_bit_2;
	return hash;
}

bool in_same_cache_set(uint64_t phys1, uint64_t phys2, int bad_bit) {
	// For Sandy Bridge, the bottom 17 bits determine the cache set
	// within the cache slice (or the location within a cache line).
	uint64_t mask = ((uint64_t) 1 << 17) - 1;
	return ((phys1 & mask) == (phys2 & mask) &&
			get_cache_slice(phys1, bad_bit) == get_cache_slice(phys2, bad_bit));
}

int main () {
	pool = (padded_scalar_t*) malloc(POOL_SIZE * SIZE_CACHE_LINE_L3);





	free(pool);
	return EXIT_SUCCESS;
}
