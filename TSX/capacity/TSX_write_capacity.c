/*

Runs MAX_THREADS transactions concurrently.

Each transactions attempts to write nb_accesses, and each
time it succeeds nb_accesses is incremented and tx restarts.

When some tx has a capacity, it grabs the global lock, aborts
every one else and each thread reports the current nb_accesses.

 */

#include "tm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LARGE_NUMBER  0x2FFFFF
#define PADDING 1024

int sync_enter, sync_exit, sync_enter_capa, other_txs_ended, extra_lock;
unsigned rand_r_fnc_seed, global_lock;

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t *shared_int;

void * W(void * me) {
	register padded_scalar_t *si = shared_int;
	register int status = -1;
	register int i;
	register int j;
	int idx, idx2;
	register int id = *((int*) me);
	bool is_capa = false;
	int nb_accesses;
	bool ended;
	unsigned read_gl, *gl_ptr = &global_lock;

	for (idx = 0; idx < EXPERIMENT_SAMPLES; ++idx) {
		nb_accesses = 0;
		is_capa = false;
		other_txs_ended = 0;
		ended = false;
		global_lock = 0;
		*gl_ptr = 0;
		read_gl = *gl_ptr;

		sync_enter_capa = 0;

		j = 512;
		while (j-- > 0) {
			si[id * PADDING + j].counter++;
		}

		__sync_add_and_fetch(&sync_enter, 1);

		while (sync_enter != MAX_THREADS * (idx + 1));
		// while (!__sync_bool_compare_and_swap(&sync_enter,
		//									MAX_THREADS * (idx + 1),
		//									MAX_THREADS * (idx + 1)));

		while (!is_capa) {

			j = nb_accesses;
			i = 0;

			if (*gl_ptr == 1) {
				is_capa = true;
				continue;
			}

			__sync_add_and_fetch(&sync_enter_capa, 1);
			while (sync_enter_capa != MAX_THREADS * (nb_accesses + 1));

			status = _xbegin();
			read_gl = *gl_ptr;
			if (status != _XBEGIN_STARTED) {

				TSX_ERROR_INC(status, errors[id - 1]);
				if (*gl_ptr == 1) {
					// conflict some guy capacity
					// printf("\nconf. abort!!!\n");
					is_capa = true;
				} else if (status == _XABORT_CAPACITY) {
					// printf("\ncapa. abort!!!\n");
					is_capa = true;
					*gl_ptr = 1; // abort all the others
				}

				continue;
			}

			while (j-- > 0) {
				si[id * PADDING + j].counter++;
			}

			_xend();
			TSX_ERROR_INC(status, errors[id - 1]);
			nb_accesses++;

		}

		INC_NB_ACCESSES(id, nb_accesses);

		__sync_add_and_fetch(&sync_exit, 1);

		while (sync_exit != MAX_THREADS * (idx + 1));
		// while (!__sync_bool_compare_and_swap(&sync_exit,
		//									MAX_THREADS * (idx + 1),
		// 									MAX_THREADS * (idx + 1)));
	}

	while (!__sync_bool_compare_and_swap(&extra_lock, id - 1, id - 1));

	PRINT_STATS(id - 1);

	__sync_add_and_fetch(&extra_lock, 1);

	return NULL;
}

int main(int argc, char **argv) {
	pthread_t thread_w[MAX_THREADS];
	cpu_set_t cpuset[MAX_THREADS];
	void *res1;
	int i, j;
	int W_ID[MAX_THREADS];
	int cpuid = 0;

	shared_int = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);
	memset(shared_int, 0, sizeof (padded_scalar_t) * LARGE_NUMBER);

	printf("CONT WRITE, threads=%i\n", MAX_THREADS);

	for (j = 0; j < MAX_THREADS; ++j) {
		W_ID[j] = j + 1;
		CPU_ZERO(&(cpuset[j]));
		CPU_SET(cpuid, &(cpuset[j]));
		cpuid += 1; // different physical cores I guess
		pthread_setaffinity_np((pthread_t) &(thread_w[j]),
				sizeof (cpu_set_t), &(cpuset[j]));

		pthread_create((pthread_t*) &(thread_w[j]),
				NULL, W, (void*) &(W_ID[j]));
	}
	for (j = 0; j < MAX_THREADS; ++j) {
		pthread_join(thread_w[j], &res1);
	}

	check_file();
	for (j = 0; j < MAX_THREADS; ++j) {
		fprintf(fp, "W%i: ", j + 1);
		for (i = 0; i < ERRORS_T_SIZE; ++i) {
			fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
					errors[j][i], (double) errors[j][i] /
					(double) EXPERIMENT_SAMPLES * 100.0);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);

	printf("\n");

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
