for f in assoc_*.out.plot  ;
do
	grep -Po 'CAPACITY=\[ [0-9]*/ \K([0-9]*\.[0-9]*%)(?=\])' $f | sed ':a;N;$!ba;s/\n/,/g'
	grep -Po 'SUCCESS=\[ [0-9]*/ \K([0-9]*\.[0-9]*%)(?=\])' $f | sed ':a;N;$!ba;s/\n/,/g'
done

