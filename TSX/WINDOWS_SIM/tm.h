#include "rdtsc.h"

#include <immintrin.h>
#include <rtmintrin.h>
#include <thread>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <ctime>

#ifndef EXPERIMENT_SAMPLES
#define EXPERIMENT_SAMPLES 25000
#endif

#define CACHE_LINE_SIZE       64

#ifndef INIT_BUDGET
#define INIT_BUDGET 5
#endif

#define LINE_MASK             0x000000000000003F
#define WAY_MASK              0x00000000000001C0
#define SET_MASK              0x0000000000007E00
#define TAG_MASK              0xFFFFFFFFFFFF8000

/*
#ifndef min
#define min(a,b)\
   ({ __typeof__ (a) _a = (a);\
	   __typeof__ (b) _b = (b);\
	 _a < _b ? _a : _b; })
#endif
#ifndef max
#define max(a,b)\
   ({ __typeof__ (a) _a = (a);\
	   __typeof__ (b) _b = (b);\
	 _a > _b ? _a : _b; })
#endif
 */

typedef struct _padded_scalar {
	volatile uint64_t counter;
	uint8_t suffixPadding[CACHE_LINE_SIZE - 2 * sizeof (uint64_t)];
	volatile uint64_t counter2;
} __attribute__ ((aligned(CACHE_LINE_SIZE))) padded_scalar_t;

typedef struct _padded_scalar_aux {
	volatile int c[CACHE_LINE_SIZE / sizeof (int)];
} __attribute__ ((aligned(CACHE_LINE_SIZE))) padded_scalar_aux_t;

#define NB_SPIN_LOOPS 10

typedef struct _spin {
	int loops[NB_SPIN_LOOPS], a_value;
	int * errors;
} spin_t;

#define MAX_RAND_R_FNC  0x80000000

#define UPDATE_BUDGET(_IS_ABORT, _BUDGET) ({ \
    _BUDGET = _IS_ABORT ? max(_BUDGET - 1, 0) : _BUDGET; \
  })

#define CHECK_GLOBAL_LOCK(lock, in_xact) ({ \
    if(lock->counter) { \
      if(in_xact) { \
        _xabort(30); \
      } else { \
        pthread_mutex_lock(&global_lock); \
        pthread_mutex_unlock(&global_lock); \
      } \
    } \
  })

#define ENTER_FALLBACK(lock, mutex) ({ \
    pthread_mutex_lock(&mutex); \
    lock->counter = true; \
  })

#define EXIT_FALLBACK(lock, mutex) ({ \
    lock->counter = false; \
    pthread_mutex_unlock(&mutex); \
  })

/**
 * _XBEGIN_STARTED    -1
 * _XABORT_EXPLICIT   0x01
 * _XABORT_RETRY      0x02
 * _XABORT_CONFLICT   0x04
 * _XABORT_CAPACITY   0x08
 * _XABORT_DEBUG      0x10
 * _XABORT_NESTED     0x20
 */

typedef enum {
	SUCCESS = 0, ABORT, EXPLICIT, RETRY, CONFLICT,
	CAPACITY, DEBUG, NESTED, OTHER, FALLBACK
} errors_t;

#define ERRORS_T_SIZE 10

#define TSX_ERROR_TO_INDEX(_ERROR) ({ \
  _ERROR == _XBEGIN_STARTED ? SUCCESS: \
  _ERROR & _XABORT_EXPLICIT ? EXPLICIT: \
  _ERROR & _XABORT_CONFLICT ? CONFLICT : \
  _ERROR & _XABORT_RETRY ? RETRY : \
  _ERROR & _XABORT_CAPACITY ? CAPACITY : \
  _ERROR & _XABORT_DEBUG ? DEBUG : \
  _ERROR & _XABORT_NESTED ? NESTED : \
  OTHER; \
})

#define TSX_ERROR_INDEX_STR(_ERROR) ({ \
  _ERROR == SUCCESS ? "SUCCESS": \
  _ERROR == ABORT ? "ABORT": \
  _ERROR == EXPLICIT ? "EXPLICIT": \
  _ERROR == RETRY ? "RETRY" : \
  _ERROR == CONFLICT ? "CONFLICT" : \
  _ERROR == CAPACITY ? "CAPACITY" : \
  _ERROR == DEBUG ? "DEBUG" : \
  _ERROR == FALLBACK ? "FALLBACK" : \
  _ERROR == NESTED ? "NESTED" : "OTHER"; \
})

#define TSX_ERROR_INC(_STATUS, _ERRORS) ({ \
  int __error = _STATUS; \
  if (__error == 0) { \
    _ERRORS[OTHER]++; \
  } else if (__error == _XBEGIN_STARTED) { \
    _ERRORS[SUCCESS]++; \
  } else { \
    int nb_errors = __builtin_popcount(__error); \
    do { \
      int __index = __error & _XABORT_EXPLICIT ? \
        ({__error = __error & ~_XABORT_EXPLICIT; EXPLICIT;}) : \
      __error & _XABORT_RETRY ? \
        ({__error = __error & ~_XABORT_RETRY; RETRY;}) : \
      __error & _XABORT_CONFLICT ? \
        ({__error = __error & ~_XABORT_CONFLICT; CONFLICT;}) : \
      __error & _XABORT_CAPACITY ? \
        ({__error = __error & ~_XABORT_CAPACITY; CAPACITY;}) : \
      __error & _XABORT_DEBUG ? \
        ({__error = __error & ~_XABORT_DEBUG; DEBUG;}) : \
      OTHER; \
      _ERRORS[__index]++; \
      nb_errors--; \
    } while (nb_errors > 0); \
  } \
})

#define START_TRANSAC(error_array)\
	register int status; \
    status = _xbegin(); \
	if (status != _XBEGIN_STARTED) { \
		error_array[ABORT]++;\
		TSX_ERROR_INC(status, error_array); \
	} else {

#define END_TRANSAC(error_array)\
		_xend();\
		error_array[SUCCESS]++;\
	}

#define TM_begin(error_array) \
	do { \
		register int status, budget = INIT_BUDGET; \
		padded_scalar_t * global_lock_held_loc = global_lock_held; \
		while (1) { \
			if (budget > 0) { \
				CHECK_GLOBAL_LOCK(global_lock_held_loc, 0); \
				status = _xbegin(); \
				if (status != _XBEGIN_STARTED) { \
					error_array[ABORT]++; \
					TSX_ERROR_INC(status, error_array); \
					--budget; \
					continue; \
				} \
				CHECK_GLOBAL_LOCK(global_lock_held_loc, 1); \
			} \
			else { \
				ENTER_FALLBACK(global_lock_held, global_lock); \
			}

#define TM_commit(error_array) \
		if (budget > 0) { \
				_xend(); \
				error_array[SUCCESS]++; \
			} \
			else { \
				EXIT_FALLBACK(global_lock_held, global_lock); \
				error_array[FALLBACK]++; \
			} \
			break; \
		} \
	} \
	while (0);


// plot stuff

FILE * fp = NULL;
// todo use C++
// pthread_mutex_t global_lock;
// padded_scalar_t * global_lock_held;

#define INIT_GSL() \
	global_lock_held = (padded_scalar_t*) malloc(sizeof (padded_scalar_t)); \
	global_lock_held->counter = 0; \
	pthread_mutex_init(&global_lock, NULL); 

#define DESTROY_GSL() \
	free(global_lock_held); \
	pthread_mutex_destroy(&global_lock); 

#ifndef PLOT_FILE
#define PLOT_FILE     "rawdata.plot"
#endif

#define check_file() if(fp == NULL) fp = fopen(PLOT_FILE, "a");
