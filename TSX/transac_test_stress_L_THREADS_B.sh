#!/bin/bash
gcc -g -O0 -mrtm ./conflicts/xact_test_vs_sim.c -o xact_test_vs_sim.out -lpthread -lrt -I. -DEXPERIMENT_SAMPLES=100000 -DACCESS_OFFSET=4 -DTHREAD_OFFSET=28
for i in {0..200}
do
  for p in 0.1 0.05
  do
    for d in 256
    do
      for l in 2 3 4 5 6 7 8 9 10 11 12
      do
        for t in 4
        do
          for b in 5
          do
            timeout 1m nice -n 19 ./xact_test_vs_sim.out THREADS $t BUDGET $b D $d WRITE_PROB $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;

  for p in 0.1 0.05
  do
    for d in 512
    do
      for l in 6
      do
        for t in 2 3 4 5 6 7 8 10 12 14 
        do
          for b in 5
          do
            timeout 1m nice -n 19 ./xact_test_vs_sim.out THREADS $t BUDGET $b D $d WRITE_PROB $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;

  for p in 0.1 0.05
  do
    for d in 256
    do
      for l in 6
      do
        for t in 4
        do
          for b in 1 2 3 4 5 6 7
          do
            timeout 1m nice -n 19 ./xact_test_vs_sim.out THREADS $t BUDGET $b D $d WRITE_PROB $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;

  for p in 1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1
  do
    for d in 256
    do
      for l in 6
      do
        for t in 4
        do
          for b in 5
          do
            timeout 1m nice -n 19 ./xact_test_vs_sim.out THREADS $t BUDGET $b D $d WRITE_PROB $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;
  
  for p in 0.1 0.05
  do
    for d in 60 100 140 160 180 200 250 300 400 500 750 1000
    do
      for l in 6
      do
        for t in 4
        do
          for b in 5
          do
            timeout 1m nice -n 19 ./xact_test_vs_sim.out THREADS $t BUDGET $b D $d WRITE_PROB $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;
  
done

