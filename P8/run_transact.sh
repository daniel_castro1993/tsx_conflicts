#!/bin/bash
gcc -g -O0 -mhtm ./conflicts/P8_conf_test.c -o P8_conf_test.out -lpthread -lrt -I.

for a in {1..200}
do
  for p in 0.9 0.5 0.1
  do
    for i in 2 6 8 10
    do
      for d in 256 512 1024 2048 8192
      do
        for t in 2 4 6 7 8
        do
          for b in 3 4 5 7
          do
            timeout 1m nice -n 19 ./P8_conf_test.out THREADS $t BUDGET $b D $d PW $p L $i ;
          done;
        done;
      done;
    done;
  done;
  
  for p in 0.9 0.5 0.1
  do
    for i in 2 6 8 10
    do
      for d in 256 512 1024 2048 8192
      do
        for t in 9 10
        do
          for b in 3 4 5
          do
            timeout 1m nice -n 19 ./P8_conf_test.out THREADS $t BUDGET $b D $d PW $p L $i ;
          done;
        done;
      done;
    done;
  done;
done
