#include "../P8_tm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LARGE_NUMBER  0x2FFFFF
#define OFFSET  0x1FFF

double PROB_READ = 1.0;
int NUM_ACCESSES = 100;
int NUM_THREADS = 100;

#ifdef EXPERIMENT_SAMPLES
#undef EXPERIMENT_SAMPLES
#define EXPERIMENT_SAMPLES 20000
#endif

unsigned rand_r_fnc_seed;
int sync_enter, attempts;

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t* mem_pool;

void * W(void * arg)
{
	int attempt, l;
	register int i;
	register int j;
	register int k;
	int cpy_k;
	int nb_accesses = NUM_ACCESSES;
	int accs[nb_accesses + 1];

	padded_scalar_t * begin_pool, * end_pool, * it;

	begin_pool = (padded_scalar_t*) arg;
	end_pool = arg + OFFSET;

	__sync_add_and_fetch(&sync_enter, 1);
	while (!__sync_bool_compare_and_swap(&sync_enter,
										NUM_THREADS,
										NUM_THREADS));

	for (attempt = 0; attempt < EXPERIMENT_SAMPLES; attempt++) {

		k = rand_r(&rand_r_fnc_seed) % OFFSET;
		j = k;
		for (i = 0; i < nb_accesses + 1; ++i) {
			accs[i] = begin_pool[j].counter = rand_r(&rand_r_fnc_seed) % OFFSET;
			begin_pool[j].counter2 = ((double) rand_r(&rand_r_fnc_seed)
					/ (double) RAND_MAX) > PROB_READ;
			for (l = 0; l < i; ++l) {
				if (accs[i] == accs[l]) {
					begin_pool[j].counter = accs[i - 1];
					--i;
					break;
				}
			}
			j = begin_pool[j].counter;
		}

		register padded_scalar_t* begin = begin_pool;

		cpy_k = k;
		j = nb_accesses;

		while (j > 0) {
			j--;
			i = begin[k].counter;
			begin[k].counter2 = (double) rand_r(&rand_r_fnc_seed)
					/ (double) RAND_MAX > PROB_READ;
			k = i;
		}

		j = nb_accesses;
		k = cpy_k;

		// TODO: Discards the last 10% attempts
		START_TRANSAC(errors_1, (double) attempt < (double) EXPERIMENT_SAMPLES * 0.9)

		while (j > 0) {
			j--;
			i = begin[k].counter; // always reads
			if (begin[k].counter2) {
				begin[k].counter = i; // WRITE
			}
			k = i;
			//			for (i = 0; i < 50; ++i); // more time in the transactions
		}

		END_TRANSAC(errors_1, (double) attempt < (double) EXPERIMENT_SAMPLES * 0.9)

				//		__sync_add_and_fetch(&attempts, (double) attempt < (double) EXPERIMENT_SAMPLES * 0.9);
	}
}

int main(int argc, char **argv)
{
	int i, j, k, l, nb_accesses;
	void * res;

	srand(clock());
	rand_r_fnc_seed = rand();

	for (i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "PROB_READ") == 0) {
			PROB_READ = atof(argv[i + 1]);
			i++;
			continue;
		}
		if (strcmp(argv[i], "NUM_ACCESSES") == 0) {
			NUM_ACCESSES = atoi(argv[i + 1]);
			i++;
			continue;
		}
		if (strcmp(argv[i], "NUM_THREADS") == 0) {
			NUM_THREADS = atoi(argv[i + 1]);
			i++;
			continue;
		}
	}

	printf("USING PROB_READ=%f, NUM_ACCESSES=%i\n", PROB_READ, NUM_ACCESSES);

	pthread_t thread[NUM_THREADS];
	pthread_attr_t attr[NUM_THREADS];
	cpu_set_t cpuset[NUM_THREADS];

	for (i = 0; i < NUM_THREADS; ++i) {
		CPU_ZERO(&(cpuset[i]));
		CPU_SET(i % 80, &(cpuset[i]));
		pthread_attr_init(&(attr[i]));
		pthread_attr_setaffinity_np(&(attr[i]),
									sizeof (cpu_set_t), &(cpuset[i]));
	}

	mem_pool = (padded_scalar_t*)
			malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);

	// create
	for (i = 0; i < NUM_THREADS; ++i) {
		pthread_create(&(thread[i]), &(attr[i]), W, (void *) (mem_pool + i * OFFSET));
		//		pthread_setaffinity_np((pthread_t) & (thread[i]),
		//							sizeof (cpu_set_t), &(cpuset[i]));
	}

	// join
	for (i = 0; i < NUM_THREADS; ++i) {
		pthread_join(thread[i], &res);
	}

	printf("attempts: %i, aborts: %i, capacity: %i\n", attempts,
		   errors_1[ABORT], errors_1[CAPACITY]);

	check_file();
	fseek(fp, 0, SEEK_END);
	if (ftell(fp) < 8) {
		fprintf(fp, "%12s, %12s, %12s, %12s, %12s, %12s\n",
				"NUM_THREADS", "PROB_READ", "NUM_ACCESSES",
				"SUCCESS", "ABORT", "CAPACITY");
	}

	float success = (double) errors_1[SUCCESS] / (double) EXPERIMENT_SAMPLES / 0.9 * 100 / (double) NUM_THREADS;
	float abort = (double) errors_1[ABORT] / (double) EXPERIMENT_SAMPLES / 0.9 * 100 / (double) NUM_THREADS;
	float capacity = (double) errors_1[CAPACITY] / (double) EXPERIMENT_SAMPLES / 0.9 * 100 / (double) NUM_THREADS;

	fprintf(fp, "%12i, %12f, %12i, ", NUM_THREADS, PROB_READ, NUM_ACCESSES);
	fprintf(fp, "%12f, %12f, %12f\n", success, abort, capacity);
	fclose(fp);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
