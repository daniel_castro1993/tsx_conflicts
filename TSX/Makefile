out = ./conflicts/case_RR.out\
	./conflicts/case_WW.out\
	./conflicts/case_RW.out\
	./conflicts/case_htm_lib.out\
	./conflicts/case_WR.out

aliasing = ./conflicts/case_NO_CONF.out

out_more_tests = ./conflicts/cache_test_case1.out\
	./conflicts/cache_test_case2.out\
	./conflicts/cache_test_case3.out\
	./conflicts/cache_test_case3b.out\
	./conflicts/cache_test_case3c.out\
	./conflicts/cache_test_case_conflict.out\
	./conflicts/nontrans_read_t0_starts_nontrans_write.out

assoc-n = 0 1 2 3 4 5 6 7 8 9
assoc-read-n = 0 1 2 3 4 5 6 7 8 9 10 11 12 14 15 16 17 18
assoc-test-max = 1 
assoc-test-max-mix = 1 2 3 4 5 6 10 11
assoc-test-max-offset = 1 2 3 4 5 6 7 8 14 16 18 
num_threads = 1 2 4 8 16
assoc-test-prob-read = 0.9995 0.999 0.998 0.99 0.9 0.5 0.1 0.0
assoc-test-mem = 25 50 75 100 125 150 175 200 225 250 275 300 325 350 375 400 450 500 600 700 800 900 1000 1200 1400 1600 1800 2000 2500 3000 4000 5000
assoc-test-mem-att = $(shell seq 1 10000) # todo samples
stress-cap = $(shell seq 1 100) # todo samples

prob_ws = 1 0.5
nb_items = 512 2048 8192 32768
nb_ops = 2 5 10 20
c_times = 1
n_thrs = 2 4 6 8 10 12 14
budget = 1 2 4 6 8 10

%.out: %.c
	gcc -g -O0 -mrtm $(aliasing) -o $@ -lpthread -lrt -DPLOT_FILE="\"$@.plot\"" -I.
	./$@

build: $(out)
	
build-more: $(out_more_tests)
	
test-rw-conf:
	gcc -g -O0 -mrtm ./conflicts/case_RW.c -o ./conflicts/case_RW.out -lpthread -lrt \
	    -DPLOT_FILE="\"./conflicts/case_RW.out.plot\"" -I. -DNB_ACCESSES=5 -DMAX_THREADS=14
	./conflicts/case_RW.out
	
test-no-conf:
	gcc -g -O0 -mrtm ./conflicts/case_NO_CONF.c -o ./conflicts/case_NO_CONF.out -lpthread -lrt \
	    -DPLOT_FILE="\"./conflicts/case_NO_CONF.out.plot\"" -I. -DNB_ACCESSES=5 -DMAX_THREADS=14
	./conflicts/case_NO_CONF.out
	
test-no-conf2:
	gcc -g -O0 -mrtm ./conflicts/case_NO_CONF2.c -o ./conflicts/case_NO_CONF2.out -lpthread -lrt -DPLOT_FILE="\"./conflicts/case_NO_CONF_2.out.plot\"" -I. -DNB_ACCESSES=10 -DMAX_THREADS=4 -DOFFSET=1
	./conflicts/case_NO_CONF2.out
	
transac:
	gcc -g -O0 -mrtm ./conflicts/xact_test_vs_sim.c -o xact_test_vs_sim.out -lpthread -lrt
	
transac-rand:
	gcc -g -O0 -mrtm ./conflicts/xact_test_vs_sim.c -o xact_test_vs_sim.out -lpthread -lrt -D__RAND_ADDRS__
	
run-transac:
	gcc -g -O0 -mrtm ./conflicts/xact_test_vs_sim.c -o xact_test_vs_sim.out -lpthread -lrt -I.
	$(foreach attempt, $(assoc-test-mem-att),\
	$(foreach prob_w, $(prob_ws), $(foreach nb_item, $(nb_items),\
	$(foreach nb_op, $(nb_ops), $(foreach c_time, $(c_times),\
	$(foreach n_thr, $(n_thrs), $(foreach bud, $(budget),\
	  ./xact_test_vs_sim.out THREADS $(n_thr) BUDGET $(bud) D $(nb_item)\
	WRITE_PROB $(prob_w) C $(c_time) L $(nb_op) ; )))))))

run-tx-test-ro:
	gcc -g -O0 -mrtm ./conflicts/xact_test_vs_sim.c -o xact_test_vs_sim.out -lpthread -lrt -lm -I. -DTEST_RO
	nice -n 19 ./xact_test_vs_sim.out L 100 THREADS 4 BUDGET 50 D 512 WRITE_PROB 1
	
run-tx-test-wo:
	gcc -g -O0 -mrtm ./conflicts/xact_test_vs_sim.c -o xact_test_vs_sim.out -lpthread -lrt -lm -I. -DTEST_WO
	nice -n 19 ./xact_test_vs_sim.out L 14 THREADS 8 BUDGET 50 D 50000 WRITE_PROB 1
	
run-tx-test-rand:
	gcc -g -O0 -mrtm ./conflicts/xact_test_vs_sim.c -o xact_test_vs_sim.out -lpthread -lrt -lm -I.
	nice -n 19 ./xact_test_vs_sim.out L 12 THREADS 14 BUDGET 8 D 1024 WRITE_PROB 0.5
	
run-cap:
	gcc -O0 -mrtm ./capacity/TSX_stress_cap.c -o assoc.out -lpthread -lrt -I. -DPLOT_FILE="\"stress_cap.txt\"" -march=native -DEXPERIMENT_SAMPLES=50000; # -S
	./assoc.out PROB_READ 0.99 NUM_ACCESSES 500
	
assoc-test:
	$(foreach n, $(assoc-n),\
	gcc -g -O0 -mrtm ./capacity/associativity_test.c -o assoc.out -lpthread -lrt\
	  -DNWAY_ACCESSES=$(n) -D__SAME_SET_WAY__\
	  -DPLOT_FILE="\"assoc_$(n).out.plot\""	;\
	./assoc.out ;\
	)
	
assoc-set-test:
	$(foreach n, $(assoc-n),\
	gcc -g -O0 -mrtm ./capacity/associativity_test.c -o assoc.out -lpthread -lrt\
	  -DNWAY_ACCESSES=$(n)\
	  -DPLOT_FILE="\"assoc_$(n).out.plot\""	;\
	./assoc.out ;\
	)
	
assoc-read-test:
	$(foreach n, $(assoc-read-n),\
	gcc -g -O0 -mrtm ./capacity/associativity_test_reads.c -o assoc.out\
	-lpthread -lrt\
	  -DNWAY_ACCESSES=$(n) -D__SAME_SET_WAY__\
	  -DPLOT_FILE="\"assoc_$(n).out.plot\""	;\
	./assoc.out ;\
	)
	
assoc-set-read-test:
	$(foreach n, $(assoc-read-n),\
	gcc -g -O0 -mrtm ./capacity/associativity_test_reads.c -o assoc.out\
	-lpthread -lrt -DNWAY_ACCESSES=$(n)\
	  -DPLOT_FILE="\"assoc_$(n).out.plot\""	;\
	./assoc.out ;\
	)
	
assoc-mixed-max:
	$(foreach n, $(assoc-test-max-mix), $(foreach o,\
	$(assoc-test-max-offset),\
	gcc -O0 -mrtm ./capacity/associativity_test_reads_writes_max.c -o assoc.out\
	  -lpthread -lrt -DWAY_SIZE=$(n) -DWRITE_OFFSET=$(o)\
	  -DPLOT_FILE="\"assoc_writes_mixed.plot\""	;\
	./assoc.out ;\
	))
	
assoc-reads-max:
	$(foreach n, $(assoc-test-max),\
	gcc -O0 -mrtm ./capacity/associativity_test_reads_max.c -o assoc.out -I. \
	  -lpthread -lrt -DWAY_SIZE=$(n)\
	  -DPLOT_FILE="\"assoc_reads_max.plot\""	;\
	nice -n 19 ./assoc.out ;\
	)
	
assoc-writes-max:
	$(foreach n, $(assoc-test-max),\
	gcc -O0 -mrtm ./capacity/associativity_test_writes_max.c -o assoc.out\
	  -lpthread -lrt -DSTRIDE=$(n) -I.\
	  -DPLOT_FILE="\"assoc_writes_max.plot\"" -march=native	;\
	./assoc.out ;\
	)
	
assoc-mixed-rand-max:
	$(foreach n, $(assoc-test-prob-read),\
	gcc -O0 -mrtm ./capacity/associativity_test_reads_writes_mixed_max.c\
	  -o assoc.out -lpthread -lrt -DPROB_READ=$(n)\
	-DPLOT_FILE="\"assoc_writes_max.plot\"" ;\
	./assoc.out ;\
	)
	
assoc-mixed-rand-mem:
	gcc -O0 -mrtm ./capacity/associativity_test_reads_writes_mixed_mem.c\
	  -o assoc.out -lpthread -lrt \
	  -DPLOT_FILE="\"assoc_mixed_mem_1.plot\"" -march=native ;
	$(foreach i, $(assoc-test-mem-att), $(foreach n,\
	$(assoc-test-prob-read), $(foreach m, $(assoc-test-mem),\
	  nice -n 19 ./assoc.out PROB_READ $(n) NUM_ACCESSES $(m) ;\
	)))
    
tsx-read-max-capacity:
	$(foreach n, $(assoc-test-max-mix), $(foreach o,\
	$(assoc-test-max-offset),\
	gcc -O0 -mrtm ./capacity/associativity_test_reads_writes_max.c -o assoc.out\
	  -lpthread -lrt -DWAY_SIZE=$(n) -DWRITE_OFFSET=$(o)\
	  -DPLOT_FILE="\"assoc_writes_mixed.plot\""	;\
	./assoc.out ;\
	))
    
stress-cap:
	gcc -O0 -mrtm ./capacity/TSX_stress_cap.c\
	  -o assoc.out -lpthread -lrt -I. \
	  -DPLOT_FILE="\"cap_per_thr.txt\"" -march=native ;
	$(foreach i, $(num_threads), $(foreach n,\
	$(assoc-test-prob-read), $(foreach m, $(assoc-test-mem),\
	  nice -n 19 ./assoc.out PROB_READ $(n) NUM_ACCESSES $(m) NUM_THREADS $(i) ;\
	)))
	
rand-r-test:
	gcc -g -O0 -mhtm rand_r_test.c -o rand_r_test.out -lpthread -lrt -D PLOT_FILE="\"rand_r_test.plot\""
	./rand_r_test.out
	
clean:
	rm -f *.out
	rm -f *.out.plot
