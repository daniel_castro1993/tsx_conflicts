#include "tm.h"

/**
 *       R1     |      R2
 *     starts   |
 *              |    starts
 *     reads    |          
 *              |    reads 
 *              |    commits
 *     commits  |
 */

#define MAX_THREADS 2
#define MULT_TIME 10000

padded_scalar_t* shared_int;

int sync_enter, sync_exit;
int read_values[MAX_THREADS][EXPERIMENT_SAMPLES];

int errors[MAX_THREADS][ERRORS_T_SIZE];

void * T(void* me_ptr) {
	int i, j;
	padded_scalar_t cpy, *si = shared_int;
	int me = *((int*) me_ptr), index = me - 1;

	printf("R%i -> spin %i / write / spin %i\n", me, me * MULT_TIME / 2,
			MAX_THREADS * MULT_TIME - index * MULT_TIME / 2);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {

		__sync_add_and_fetch(&sync_enter, 1);
		while (!__sync_bool_compare_and_swap(&sync_enter,
				MAX_THREADS * (i + 1),
				MAX_THREADS * (i + 1)));

		// R1 spins 5000     |  R2 spins 10000
		// R1 writes         |
		// R1 spins 15000    |  R2 writes
		//                   |  R2 spins 10000
		// R1 commit         |  R2 commit

		START_TRANSAC(errors[index]);

		for (j = 0; j < me * MULT_TIME / 2; j++);

		cpy.counter = si->counter;

		for (j = 0; j < MAX_THREADS * MULT_TIME - index * MULT_TIME / 2; j++);

		END_TRANSAC(errors[index]);

		__sync_add_and_fetch(&sync_exit, 1);
		while (!__sync_bool_compare_and_swap(&sync_exit,
				MAX_THREADS * (i + 1),
				MAX_THREADS * (i + 1)));
	}

	return NULL;
}

int main() {
	pthread_t thread_w[MAX_THREADS];
	cpu_set_t cpuset[MAX_THREADS];
	void *res1;
	int i, j;
	int W_ID[MAX_THREADS];
	int cpuid;

	shared_int = (padded_scalar_t*) malloc(sizeof (padded_scalar_t));
	memset(shared_int, 0, sizeof (padded_scalar_t));

	shared_int->counter = 1;

	for (j = 0; j < MAX_THREADS; ++j) {
		W_ID[j] = j + 1;
		CPU_ZERO(&(cpuset[j]));
		CPU_SET(cpuid, &(cpuset[j]));
		cpuid += 8; // different physical cores I guess
		pthread_setaffinity_np((pthread_t) &(thread_w[j]),
				sizeof (cpu_set_t), &(cpuset[j]));

		pthread_create((pthread_t*) &(thread_w[j]),
				NULL, T, (void*) &(W_ID[j]));
	}
	for (j = 0; j < MAX_THREADS; ++j) {
		pthread_join(thread_w[j], &res1);
	}

	check_file();
	for (j = 0; j < MAX_THREADS; ++j) {
		fprintf(fp, "R%i: ", j + 1);
		for (i = 0; i < ERRORS_T_SIZE; ++i) {
			fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
					errors[j][i], (double) errors[j][i] /
					(double) EXPERIMENT_SAMPLES * 100.0);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);

	printf("final value counter: %i\n", (int) shared_int->counter);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
