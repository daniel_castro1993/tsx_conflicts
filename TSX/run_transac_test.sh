#!/bin/bash
gcc -g -O0 -mrtm ./conflicts/xact_test_vs_sim.c -o xact_test_vs_sim.out -lpthread -lrt -I. -DEXPERIMENT_SAMPLES=50000 -DACCESS_OFFSET=4 -DTHREAD_OFFSET=28
for i in {0..200}
do
  for p in 1 0.9 0.5 0.1
  do
    for d in 128 256 512 1024 2048 
    do
      for l in 2 4 6 8
      do
        for t in 2 4 6 8 10 12 14
        do
          for b in 3 5 7 9
          do
            timeout 1m nice -n 19 ./xact_test_vs_sim.out THREADS $t BUDGET $b D $d WRITE_PROB $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;
done




