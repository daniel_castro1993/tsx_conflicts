#ifndef __USE_GNU
#define __USE_GNU
#endif
#define _GNU_SOURCE

#include "rdtsc.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sched.h>
#include <time.h>
#include <math.h>

#include <htmxlintrin.h>

#ifndef EXPERIMENT_SAMPLES
#define EXPERIMENT_SAMPLES  100000
#endif

#define CACHE_LINE_SIZE       128

typedef struct padded_scalar
{
	volatile unsigned long counter;
    volatile unsigned long counter2;
	// char suffixPadding[CACHE_LINE_SIZE / sizeof(char) - 2*sizeof(unsigned long)]; // it has more memory???
} __attribute__((aligned(CACHE_LINE_SIZE))) padded_scalar_t;

extern __inline long
__attribute__((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_trans_conflict(void* const TM_buff)
{
	texasr_t texasr = *_TEXASRU_PTR(TM_buff);
	return _TEXASR_TRANSACTION_CONFLICT(texasr);
}

extern __inline long
__attribute__((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_nontrans_conflict(void* const TM_buff)
{
	texasr_t texasr = *_TEXASRU_PTR(TM_buff);
	return _TEXASR_NON_TRANSACTIONAL_CONFLICT(texasr);
}

extern __inline long
__attribute__((__gnu_inline__, __always_inline__, __artificial__))
__TM_is_rot(void* const TM_buff)
{
	texasr_t texasr = *_TEXASRU_PTR(TM_buff);
	return _TEXASR_ROT(texasr);
}

extern __inline long
__attribute__((__gnu_inline__, __always_inline__, __artificial__))
__TM_begin_rot(void* const TM_buff)
{
	*_TEXASRL_PTR(TM_buff) = 0;
	if (__builtin_expect(__builtin_tbegin(1), 1)) {
		return _HTM_TBEGIN_STARTED;
	}
#ifdef __powerpc64__
	*_TEXASR_PTR(TM_buff) = __builtin_get_texasr();
#else
	*_TEXASRU_PTR(TM_buff) = __builtin_get_texasru();
	*_TEXASRL_PTR(TM_buff) = __builtin_get_texasr();
#endif
	*_TFIAR_PTR(TM_buff) = __builtin_get_tfiar();
	return 0;
}

typedef enum
{
	SUCCESS = 0,
	ABORT = 1,
	USER,
	ILLEGAL,
	CAPACITY,
	NESTING_DEPTH,
	NESTED_TOO_DEEP,
	CONFLICT,
	PERSISTENT,
	OTHER
} errors_t;

#ifndef min
#define min(a,b)\
   ({ __typeof__ (a) _a = (a);\
       __typeof__ (b) _b = (b);\
     _a < _b ? _a : _b; })
#endif
#ifndef max
#define max(a,b)\
   ({ __typeof__ (a) _a = (a);\
       __typeof__ (b) _b = (b);\
     _a > _b ? _a : _b; })
#endif

#define ERRORS_T_SIZE 10

#define P8_ERROR_TO_INDEX(tm_buffer) ({\
  errors_t code = OTHER;\
  if(__TM_is_conflict(tm_buffer)) code = CONFLICT;\
  else if(__TM_is_illegal  (tm_buffer)) code = ILLEGAL;\
  else if(__TM_is_footprint_exceeded(tm_buffer)) code = CAPACITY;\
  else if(__TM_nesting_depth(tm_buffer)) code = NESTING_DEPTH;\
  else if(__TM_is_nested_too_deep(tm_buffer)) code = NESTED_TOO_DEEP;\
  else if(__TM_is_user_abort(tm_buffer)) code = USER;\
  else if(__TM_is_failure_persistent(tm_buffer)) code = PERSISTENT;\
  code;\
})

#define P8_ERROR_INDEX_STR(error) ({\
  error == SUCCESS ? "SUCCESS" :\
  error == ABORT ? "ABORT" :\
  error == USER ? "USER" :\
  error == ILLEGAL ? "ILLEGAL" :\
  error == CAPACITY ? "CAPACITY" :\
  error == NESTING_DEPTH ? "NESTING_DEPTH" :\
  error == NESTED_TOO_DEEP ? "NESTED_TOO_DEEP" :\
  error == CONFLICT ? "CONFLICT" :\
  error == PERSISTENT ? "PERSISTENT" : "OTHER";\
})

#define P8_ERROR_INC(_STATUS, _ERRORS) ({ \
  _ERRORS[P8_ERROR_TO_INDEX(_STATUS)] += 1; \
})

#define START_TRANSAC(error_array, store_error) \
	TM_buff_type TM_buff;\
	if (__TM_begin(TM_buff) != _HTM_TBEGIN_STARTED) { \
		if (store_error) { \
			__sync_add_and_fetch(&(error_array[ABORT]), 1); \
			P8_ERROR_INC(TM_buff, error_array); \
		} \
	} else {

#define END_TRANSAC(error_array, store_error)\
		__TM_end();\
		if (store_error) { \
			__sync_add_and_fetch(&(error_array[SUCCESS]), 1); \
		} \
	}

// plot stuff

FILE * fp = NULL;
int errors_1[ERRORS_T_SIZE], errors_2[ERRORS_T_SIZE];

#ifndef PLOT_FILE
#define PLOT_FILE     "rawdata.plot"
#endif

#define check_file() if(fp == NULL) fp = fopen(PLOT_FILE, "a");
