#ifndef __USE_GNU
#define __USE_GNU
#endif
#define _GNU_SOURCE

#include "rdtsc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#include <sched.h>
#include <immintrin.h>
#include <rtmintrin.h>
#include <time.h>
#include <math.h>

#ifndef EXPERIMENT_SAMPLES
#define EXPERIMENT_SAMPLES  50000
#endif

#define CACHE_LINE_SIZE       64

#define ALLOC_FN(size) malloc(size)

#ifndef INIT_BUDGET
#define INIT_BUDGET 5
#endif

#define LINE_MASK             0x000000000000003F
#define WAY_MASK              0x00000000000001C0
#define SET_MASK              0x0000000000007E00
#define TAG_MASK              0xFFFFFFFFFFFF8000

#ifndef min
#define min(a,b)\
   ({ __typeof__ (a) _a = (a);\
       __typeof__ (b) _b = (b);\
     _a < _b ? _a : _b; })
#endif
#ifndef max
#define max(a,b)\
   ({ __typeof__ (a) _a = (a);\
       __typeof__ (b) _b = (b);\
     _a > _b ? _a : _b; })
#endif

#define INC_NB_ACCESSES(id, accesses) ({\
    int i, j;\
    for (i = 0; i < num_accesses[id-1]; ++i) {\
        if (array_accesses[id-1][i] > accesses)\
            break;\
    }\
    for (j = num_accesses[id-1]; j > i; --j) {\
        array_accesses[id-1][j] = array_accesses[id-1][j-1];\
    }\
    array_accesses[id-1][i] = accesses;\
    num_accesses[id-1]++;\
})

#define AVG_NB_ACCESSES(id) ({\
    int i;\
    double avg = 0;\
    for (i = 0; i < num_accesses[id-1]; ++i) {\
        avg += array_accesses[id-1][i];\
    }\
    avg / EXPERIMENT_SAMPLES;\
})

#define STDDEV_NB_ACCESSES(id, avg) ({\
    int i;\
    double stddev = 0;\
    for (i = 0; i < num_accesses[id-1]; ++i) {\
        stddev += pow(avg - array_accesses[id-1][i], 2);\
    }\
    sqrt(stddev / EXPERIMENT_SAMPLES);\
})

#define PRINT_STATS(thread) ({\
    double avg = AVG_NB_ACCESSES(id);\
    double stddev = STDDEV_NB_ACCESSES(id, avg);\
    printf("W%i: min=%i, 25%% percentile=%i, median=%i, 75%% percentile=%i, max=%i\n",\
        id, array_accesses[thread][0], array_accesses[thread][EXPERIMENT_SAMPLES/4],\
        array_accesses[thread][EXPERIMENT_SAMPLES/2],\
        array_accesses[thread][2*EXPERIMENT_SAMPLES/3],\
        array_accesses[thread][EXPERIMENT_SAMPLES-1]);\
    printf("    avg=%f, stddev=%f\n", avg, stddev);\
})

typedef struct _padded_scalar
{
    int counter;
    int counter2;
    // volatile char suffixPadding[CACHE_LINE_SIZE / sizeof (char) - 2 * sizeof (int)];
} __attribute__ ((aligned(CACHE_LINE_SIZE))) padded_scalar_t; // uint8_t

typedef struct _padded_scalar_n
{
    int counter;
    int counter2;
} padded_scalar_n_t; // uint8_t

typedef struct _scalar
{
    volatile uint64_t counter;
} __attribute__ ((aligned(sizeof (uint64_t)))) scalar_t;

typedef struct _padded_scalar_2
{
    volatile uint64_t counter2;
    uint8_t suffixPadding[(CACHE_LINE_SIZE - 2 * sizeof (uint64_t)) / 2];
    volatile uint64_t counter;
    uint8_t suffixPadding2[(CACHE_LINE_SIZE - 2 * sizeof (uint64_t)) / 2];
} __attribute__ ((aligned(CACHE_LINE_SIZE))) padded_scalar2_t;

typedef struct _padded_scalar_aux
{
    volatile int c[CACHE_LINE_SIZE / sizeof (int)];
} __attribute__ ((aligned(CACHE_LINE_SIZE))) padded_scalar_aux_t;

#define NB_SPIN_LOOPS 10

typedef struct _spin
{
    int loops[NB_SPIN_LOOPS], a_value;
    int * errors;
} spin_t;

#define MAX_RAND_R_FNC  0x80000000

// #define RAND_R_FNC(_SEED, _AUX) ({ \
    _AUX = _SEED; \
    _AUX *= 1103515245; \
    _AUX += 12345; \
    _SEED = (_AUX / 65536) % 2048; \
    _AUX *= 1103515245; \
    _AUX += 12345; \
    _SEED <<= 10; \
    _SEED ^= (unsigned long long) (_AUX / 65536) % 2048; \
    _AUX *= 1103515245; \
    _AUX += 12345; \
    _SEED <<= 10; \
    _SEED ^= (unsigned long long) (_AUX / 65536) % 2048; \
    _SEED; \
  })
  
#define RAND_R_FNC(seed) ({ \
	unsigned long long next = seed; \
	unsigned long long result; \
	next *= 1103515245; \
	next += 12345; \
	result = (unsigned long long) (next / 65536) % 2048; \
	next *= 1103515245; \
	next += 12345; \
	result <<= 10; \
	result ^= (unsigned long long) (next / 65536) % 1024; \
	next *= 1103515245; \
	next += 12345; \
	result <<= 10; \
	result ^= (unsigned long long) (next / 65536) % 1024; \
	seed = next; \
	result; \
})

#define IS_WRITE(_WRITE_PROB, _SEED) ({\
	int aux; \
        unsigned rand_value = RAND_R_FNC(_SEED); \
        rand_value = rand_value % 999999; \
    ((double) rand_value / (double) 999999) < _WRITE_PROB; \
  })

#define SHUFFLE_ARRAY(_ARRAY, _LENGTH, _SEED) ({                              \
    if (_LENGTH > 1) {                                                        \
        size_t it_;                                                           \
        for (it_ = 0; it_ < _LENGTH - 1; ++it_) {                             \
          size_t j_ = it_ + rand_r(_SEED) / (RAND_MAX / (_LENGTH - it_) + 1); \
          int t_ = _ARRAY[j_];                                                \
          _ARRAY[j_] = _ARRAY[it_];                                           \
          _ARRAY[it_] = t_;                                                   \
        }                                                                     \
    }                                                                         \
  })

#ifdef __RAND_ADDRS__
#define ITER_FILL_ADDRS(_ADDRS, _SEED, _L, _D, _I) ({                         \
    int __i;                                                                  \
    bool __is_new = true;                                                     \
    do {                                                                      \
      if (!(_L < _D)) {                                                       \
        _ADDRS[_I] = _I;                                                      \
      } else {                                                                \
        _ADDRS[_I] = rand_r(_SEED) % _D;                                      \
        for (__i = 0; __i < _I; ++__i) {                                      \
          if (_ADDRS[__i] == _ADDRS[_I]) {                                    \
            __is_new = false;                                                 \
            break;                                                            \
          }                                                                   \
        }                                                                     \
        if (!__is_new) {                                                      \
          __is_new = true;                                                    \
          continue;                                                           \
        }                                                                     \
      }                                                                       \
      break;                                                                  \
    } while (true);                                                           \
  })

#define FILL_ADDRS(ADDRS_, SEED_, L_, D_) ({                                \
    int it_ = 0;                                                              \
    while (it_ < L_ && it_ < D_) {                                            \
      ITER_FILL_ADDRS(ADDRS_, SEED_, L_, D_, it_);                            \
      ++it_; \
    } \
  })
// SHUFFLE_ARRAY(ADDRS_, L_, SEED_);

#else

#define ITER_FILL_ADDRS(_ADDRS, _SEED, _L, _D, _I) ({ \
    if (_I == 0) { \
      _ADDRS[_I] = rand_r(_SEED) % _D; \
    } else { \
      _ADDRS[_I] = (_ADDRS[_I - 1] + 64*8) % _D; \
    } \
  })

#define FILL_ADDRS(_ADDRS, _SEED, _L, _D) ({ \
    int __i = 0; \
    while (__i < _L && __i < _D ) { \
      ITER_FILL_ADDRS(_ADDRS, _SEED, _L, _D, __i); \
      ++__i; \
    } \
  })

#endif

#define DO_XACT(_POOL, _SEED, _WR_PROB, _L, _D) ({ \
    int __i, __addr[_L]; \
    for (__i = 0; __i < _L; ++__i) { \
      int __j = __i; \
      ITER_FILL_ADDRS(__addr, _SEED, _L, _D, __j); \
      if (IS_WRITE(_WR_PROB, _SEED)) { \
        _POOL[__addr[__i]].counter++; \
      } else { \
        __j = _POOL[__addr[__i]].counter; \
      } \
    } \
  })

#define do_xact(pool, seed, addrs, prob_write, nb_granules) ({ \
    int i, local; \
    for (i = 0; i < nb_granules; ++i) { \
        if (IS_WRITE(prob_write, seed)) { \
            write_granule(pool[addrs[i]]); \
        } \
        else { \
            read_granule(pool[addrs[i]]); \
        } \
    } \
    local; \
})

#define UPDATE_BUDGET(_IS_ABORT, _BUDGET) ({ \
    _BUDGET = _IS_ABORT ? max(_BUDGET - 1, 0) : _BUDGET; \
  })

#define CHECK_GLOBAL_LOCK(lock, in_xact) ({ \
    bool is_locked = (lock)->counter; \
    if(is_locked) { \
      if(in_xact) { \
        _xabort(30); \
      } else { \
        pthread_mutex_lock(&global_lock); \
        pthread_mutex_unlock(&global_lock); \
      } \
    } \
  })

#define ENTER_FALLBACK(lock, mutex) ({ \
    pthread_mutex_lock(&mutex); \
    (lock)->counter = true; \
  })

#define EXIT_FALLBACK(lock, mutex) ({ \
    (lock)->counter = false; \
    pthread_mutex_unlock(&mutex); \
  })

/**
 * _XBEGIN_STARTED    -1
 * _XABORT_EXPLICIT   0x01
 * _XABORT_RETRY      0x02
 * _XABORT_CONFLICT   0x04
 * _XABORT_CAPACITY   0x08
 * _XABORT_DEBUG      0x10
 * _XABORT_NESTED     0x20
 */

typedef enum
{
    SUCCESS = 0, ABORT, EXPLICIT, RETRY, CONFLICT,
    CAPACITY, DEBUG, NESTED, OTHER, FALLBACK
} errors_t;

#define ERRORS_T_SIZE 10

#define TSX_ERROR_TO_INDEX(_ERROR) ({ \
  _ERROR == _XBEGIN_STARTED ? SUCCESS: \
  _ERROR & _XABORT_EXPLICIT ? EXPLICIT: \
  _ERROR & _XABORT_CONFLICT ? CONFLICT : \
  _ERROR & _XABORT_RETRY ? RETRY : \
  _ERROR & _XABORT_CAPACITY ? CAPACITY : \
  _ERROR & _XABORT_DEBUG ? DEBUG : \
  _ERROR & _XABORT_NESTED ? NESTED : \
  OTHER; \
})

#define TSX_ERROR_INDEX_STR(_ERROR) ({ \
  _ERROR == SUCCESS ? "SUCCESS": \
  _ERROR == ABORT ? "ABORT": \
  _ERROR == EXPLICIT ? "EXPLICIT": \
  _ERROR == RETRY ? "RETRY" : \
  _ERROR == CONFLICT ? "CONFLICT" : \
  _ERROR == CAPACITY ? "CAPACITY" : \
  _ERROR == DEBUG ? "DEBUG" : \
  _ERROR == FALLBACK ? "FALLBACK" : \
  _ERROR == NESTED ? "NESTED" : "OTHER"; \
})

#define TSX_ERROR_INC(_STATUS, _ERRORS) ({ \
  int __error = _STATUS; \
  if (__error == 0) { \
    _ERRORS[OTHER] += 1; \
  } else if (__error == _XBEGIN_STARTED) { \
    _ERRORS[SUCCESS] += 1; \
  } else { \
    int nb_errors = __builtin_popcount(__error); \
    do { \
      int __index = __error & _XABORT_EXPLICIT ? \
        ({__error = __error & ~_XABORT_EXPLICIT; EXPLICIT;}) : \
      __error & _XABORT_RETRY ? \
        ({__error = __error & ~_XABORT_RETRY; RETRY;}) : \
      __error & _XABORT_CONFLICT ? \
        ({__error = __error & ~_XABORT_CONFLICT; CONFLICT;}) : \
      __error & _XABORT_CAPACITY ? \
        ({__error = __error & ~_XABORT_CAPACITY; CAPACITY;}) : \
      __error & _XABORT_DEBUG ? \
        ({__error = __error & ~_XABORT_DEBUG; DEBUG;}) : \
      OTHER; \
      _ERRORS[__index] += 1; \
      nb_errors--; \
    } while (nb_errors > 0); \
  } \
})

#define START_TRANSAC(error_array)\
	register int status; \
    status = _xbegin(); \
        if (status != _XBEGIN_STARTED) { \
                error_array[ABORT]++;\
		TSX_ERROR_INC(status, error_array); \
        } else {

#define END_TRANSAC(error_array)\
		_xend();\
		error_array[SUCCESS]++;\
	}

#define TM_begin(error_array) \
        do { \
                register int status, budget = INIT_BUDGET; \
                padded_scalar_t * global_lock_held_loc = global_lock_held; \
                while (1) { \
                        if (budget > 0) { \
                                CHECK_GLOBAL_LOCK(global_lock_held_loc, 0); \
                                status = _xbegin(); \
                                if (status != _XBEGIN_STARTED) { \
                                        error_array[ABORT]++; \
                                        TSX_ERROR_INC(status, error_array); \
                                        --budget; \
                                        continue; \
                                } \
                                CHECK_GLOBAL_LOCK(global_lock_held_loc, 1); \
                        } \
                        else { \
                                ENTER_FALLBACK(global_lock_held, global_lock); \
                        }

#define TM_commit(error_array) \
                if (budget > 0) { \
                                _xend(); \
                                error_array[SUCCESS]++; \
                        } \
                        else { \
                                EXIT_FALLBACK(global_lock_held, global_lock); \
                                error_array[FALLBACK]++; \
                        } \
                        break; \
                } \
        } \
        while (0);


// plot stuff

FILE * fp = NULL;

#define INIT_GSL() \
        pthread_mutex_init(&global_lock, NULL); 

#define DESTROY_GSL() \
        pthread_mutex_destroy(&global_lock); 

#ifndef PLOT_FILE
#define PLOT_FILE     "rawdata.plot"
#endif

#define check_file() if(fp == NULL) fp = fopen(PLOT_FILE, "a");
