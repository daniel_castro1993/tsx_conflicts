#include "tm.h"

#define RAND_R_CALLS 1000

/**
 * Two transaction running rand_r.
 */

unsigned long long total_time = 0;
unsigned long long total_runs = 0;

typedef struct _W_arg_t {
	int seed;
	int* errors;
} W_arg_t;

void * W(void * argptr) {
	int j, status;
	W_arg_t* arg = (W_arg_t*) argptr;
	int seed = arg->seed;

	unsigned long long ts1, ts2;

	ts1 = rdtsc();

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		TSX_ERROR_INC(status, arg->errors);
		pthread_exit(NULL);
	}

	for (j = 0; j < RAND_R_CALLS; ++j) {
		volatile i = rand_r(&seed);
	}

	_xend();

	ts2 = rdtsc();
	__sync_add_and_fetch(&total_time, ts2 - ts1);
	__sync_add_and_fetch(&total_runs, 1);

	TSX_ERROR_INC(status, arg->errors);
	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w1, thread_w2;
	cpu_set_t cpuset_1, cpuset_2;

	int i;
	void* res1;
	void* res2;

	CPU_ZERO(&cpuset_1);
	CPU_SET(1, &cpuset_1);
	pthread_setaffinity_np((pthread_t) & thread_w1, sizeof (cpu_set_t), &cpuset_1);

	CPU_ZERO(&cpuset_2);
	CPU_SET(2, &cpuset_2);
	pthread_setaffinity_np((pthread_t) & thread_w2, sizeof (cpu_set_t), &cpuset_2);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		W_arg_t arg1, arg2;
		arg1.seed = i;
		arg1.errors = errors_1;
		arg2.seed = EXPERIMENT_SAMPLES + i;
		arg2.errors = errors_2;

		pthread_create(&thread_w1, NULL, W, (void *) &arg1);
		pthread_create(&thread_w2, NULL, W, (void *) &arg2);
		pthread_join(thread_w1, &res1);
		pthread_join(thread_w2, &res2);
	}

	check_file();
	fprintf(fp, "THR1: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i] / (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n");
	fprintf(fp, "THR2: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_2[i], (double) errors_2[i] / (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n\n");
	fclose(fp);

	printf("rand_r avg_time x1000: %lf\n",
			(double) total_time / (double) total_runs);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
