#include "tm.h"

#define LARGE_NUMBER  0xFFFFF

// TODO:

/**
 *       W1     |      W2
 *     starts   |
 *              |    starts
 *     write X  |    write same cache line
 *              |    commits
 *     commits  |
 * X and X' map to the same cache line
 */

padded_scalar_t mem_pool[LARGE_NUMBER];
padded_scalar_t * addr_X;
padded_scalar_t * addr_X_[8];

padded_scalar_t * safe_scalar1, * safe_scalar2;

spin_t spin_W1, spin_W2;

void * W(void * arg) {
	int j, status;
	spin_t spin = *((spin_t*) arg);

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		TSX_ERROR_INC(status, spin.errors);
		pthread_exit(NULL);
	}

	while (spin.loops[0]-- > 0);

	addr_X->counter = spin.a_value;

	while (spin.loops[1]-- > 0);

	_xend();

	TSX_ERROR_INC(status, spin.errors);
	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w1, thread_w2;
	cpu_set_t cpuset_1, cpuset_2;

	int rc, i, j;
	void * res;

	padded_scalar_t * begin_pool = (padded_scalar_t*) mem_pool,
			* end_pool = begin_pool + LARGE_NUMBER,
			* it;
	unsigned long mask = SET_MASK | WAY_MASK, addr_X_masked;

	addr_X = begin_pool;
	addr_X->counter = 0xff;

	addr_X_masked = mask & (unsigned long) addr_X;

	spin_W1.loops[0] = 24000;
	spin_W1.loops[1] = 12000;
	spin_W1.a_value = 5;
	spin_W1.errors = errors_1;

	spin_W2.loops[0] = 4000;
	spin_W2.loops[1] = 32000;
	spin_W2.a_value = 7;
	spin_W2.errors = errors_2;

	i = 0;
	for (it = end_pool - 1; it >= begin_pool; --it) {
		if (((unsigned long) it & mask) == addr_X_masked) {
			addr_X_[i] = it;
			addr_X_[i]->counter = 0xff;
			i++;
			if (i == 8) {
				break;
			}
		}
	}

	for (it = end_pool - 1; it >= begin_pool; --it) {
		if (((unsigned long) it & mask) != addr_X_masked) {
			safe_scalar1 = it;
		}
	}

	for (it = begin_pool; it < end_pool; ++it) {
		if (((unsigned long) it & mask) != addr_X_masked
				&& ((unsigned long) it & mask) != ((unsigned long) it & (unsigned long) safe_scalar1)) {
			safe_scalar2 = it;
		}
	}

	check_file();
	fprintf(fp, "addr_X=[%p] ", addr_X);

	CPU_ZERO(&cpuset_1);
	CPU_ZERO(&cpuset_2);

	CPU_SET(1, &cpuset_1);
	CPU_SET(2, &cpuset_2);

	pthread_setaffinity_np((pthread_t) & thread_w1, sizeof (cpuset_1), &cpuset_1);
	pthread_setaffinity_np((pthread_t) & thread_w2, sizeof (cpuset_2), &cpuset_2);

	memset(mem_pool, -1, sizeof (mem_pool));

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		pthread_create(&thread_w2, NULL, W, (void *) &spin_W2);
		pthread_yield();
		pthread_create(&thread_w1, NULL, W, (void *) &spin_W1);

		pthread_join(thread_w2, &res);
		pthread_yield();
		pthread_join(thread_w1, &res);
	}

	check_file();
	fprintf(fp, "\nW1: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i] / (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\nW2: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_2[i], (double) errors_2[i] / (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n");
	fclose(fp);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
