#include "rdtsc.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

int main () {
	unsigned long i, ts0, ts1, sum;
	clock_t t0;

	for ( i = 0; i < 10; ++i) {
		ts0 = rdtsc();
		t0 = clock();
		while ((clock() - t0) / CLOCKS_PER_SEC < 1);
		ts1 = rdtsc();
		sum += ts1 - ts0;
	}

	printf("cycles in 1s: %f\n", (double) sum / 10.0);

	return EXIT_SUCCESS;
}
