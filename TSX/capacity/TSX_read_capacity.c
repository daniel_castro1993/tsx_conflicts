#include "tm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LARGE_NUMBER  0x2FFFFF

#ifndef PADDING
#define PADDING 2048
#endif

#ifndef STEP
#define STEP 2048
#endif

#define CALC_SLICE(addr) ({ \
    int h1[] = {18, 19, 21, 23, 25, 27, 29, 30, 31}; \
	int h2[] = {17, 19, 20, 21, 22, 23, 24, 26, 28, 29, 31}; \
	int h1_size = 9, h2_size = 11; \
	int i, bit1 = (addr >> h1[0]) & 1, bit2 = (addr >> h2[0]) & 1; \
	for (i = 1; i < h1_size; ++i) { \
		bit1 ^= (addr >> h1[i]) & 1; \
	} \
	for (i = 1; i < h2_size; ++i) { \
		bit2 ^= (addr >> h2[i]) & 1; \
	} \
    (bit2 << 1) | (bit1); \
})

int sync_enter, sync_exit, extra_lock;
unsigned rand_r_fnc_seed;

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t *shared_int;

void * W(void * me) {
	register padded_scalar_t *si = shared_int;
	register int status = -1;
	register int i;
	register int j;
	int idx;
	register int id = *((int*) me);
	int nb_capa;
	int nb_accesses;

	for (idx = 0; idx < EXPERIMENT_SAMPLES; ++idx) {
		nb_accesses = 0;
		nb_capa = 0;

		__sync_add_and_fetch(&sync_enter, 1);
		while (!__sync_bool_compare_and_swap(&sync_enter,
				MAX_THREADS * (idx + 1),
				MAX_THREADS * (idx + 1)));

		while (nb_capa < 10) {
			j = nb_accesses;
			i = 0;

			status = _xbegin();
			if (status != _XBEGIN_STARTED) {
				TSX_ERROR_INC(status, errors[id - 1]);
				if (status == _XABORT_CAPACITY) {
					// printf("capa. abort!!!\n");
					nb_capa++;
				} else {
					// printf("other abort!!!\n");
					nb_capa = 0;
				}
				continue;
			}

			while (i < j) {
				int k;
				i++;
				k = si[id * PADDING + i * STEP].counter;
				k ^= k;
			}

			_xend();
			TSX_ERROR_INC(status, errors[id - 1]);
			nb_accesses++;
		}

		INC_NB_ACCESSES(id, nb_accesses);

		__sync_add_and_fetch(&sync_exit, 1);
		while (!__sync_bool_compare_and_swap(&sync_exit,
				MAX_THREADS * (idx + 1),
				MAX_THREADS * (idx + 1)));
	}

	while (!__sync_bool_compare_and_swap(&extra_lock, id - 1, id - 1));

	PRINT_STATS(id - 1);

	__sync_add_and_fetch(&extra_lock, 1);

	return NULL;
}

int main(int argc, char **argv) {
	pthread_t thread_w[MAX_THREADS];
	cpu_set_t cpuset[MAX_THREADS];
	void *res1;
	int i, j;
	int W_ID[MAX_THREADS];
	int cpuid = 0;

	shared_int = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);
	memset(shared_int, 0, sizeof (padded_scalar_t) * LARGE_NUMBER);

	printf("CONT READ, threads=%i\n", MAX_THREADS);

	for (j = 0; j < MAX_THREADS; ++j) {
		W_ID[j] = j + 1;
		CPU_ZERO(&(cpuset[j]));
		CPU_SET(cpuid, &(cpuset[j]));
		cpuid += 1; // different physical cores I guess
		pthread_setaffinity_np((pthread_t) &(thread_w[j]),
				sizeof (cpu_set_t), &(cpuset[j]));

		pthread_create((pthread_t*) &(thread_w[j]),
				NULL, W, (void*) &(W_ID[j]));
	}
	for (j = 0; j < MAX_THREADS; ++j) {
		pthread_join(thread_w[j], &res1);
	}

	check_file();
	for (j = 0; j < MAX_THREADS; ++j) {
		fprintf(fp, "W%i: ", j + 1);
		for (i = 0; i < ERRORS_T_SIZE; ++i) {
			fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
					errors[j][i], (double) errors[j][i] /
					(double) EXPERIMENT_SAMPLES * 100.0);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);

	printf("\n");

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
