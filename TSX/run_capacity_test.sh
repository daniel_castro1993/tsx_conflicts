#!/bin/bash

gcc -O0 -mrtm ./capacity/TSX_stress_cap.c -o assoc.out -lpthread -lrt -I. -DPLOT_FILE="\"stress_cap.txt\"" -march=native -DEXPERIMENT_SAMPLES=50000; # -S
for p in 0.0 0.99999 0.9999 0.9995 0.999 0.998 0.99 0.9 0.5 0.1 0.0
do
  for m in 25 50 75 100 125 150 175 200 225 250 275 300 325 350 375 400 450 500
  do
    ./assoc.out PROB_READ $p NUM_ACCESSES $m ; wait ;
  done ;
  wait ;
done

for p in 0.99999 0.9999 0.9995 0.999 0.998 0.99 0.9
do
  for m in 600 700 800 900 1000 1200
  do
    ./assoc.out PROB_READ $p NUM_ACCESSES $m ; wait ;
  done ;
  wait ;
done

gcc -O0 -mrtm ./capacity/TSX_stress_cap.c -o assoc.out -lpthread -lrt -I. -DPLOT_FILE="\"stress_cap.txt\"" -march=native -DEXPERIMENT_SAMPLES=25000; # -S
for p in 0.99999 0.9999 0.9995 0.999 
do
  for m in 1400 1600 1800 2000 2500 3000 4000 5000 6000 7000 8000 9000 10000
  do
    ./assoc.out PROB_READ $p NUM_ACCESSES $m ; wait ;
  done ;
  wait ;
done

