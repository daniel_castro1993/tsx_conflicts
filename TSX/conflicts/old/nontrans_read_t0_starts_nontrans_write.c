#include "tm.h"

/**
 *       W      |      R
 *              |    reads
 *              |    starts
 *     writes   |           
 *              |    commits
 */

padded_scalar_t shared_int, shared_int2, shared_int3;

void *R() {
	int j;

	// reads before all
	shared_int2.counter = shared_int.counter;

	int status;
	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		check_file();
		fprintf(fp, "R=Failed[%i] ", status);
		pthread_exit(NULL);
	}

	shared_int3.counter = shared_int2.counter;
	shared_int2.counter = 4;

	for (j = 0; j < 8000; j++);

#ifdef ABORT_EXPLICIT
	_xabort(0x00);
#endif
	_xend();

	check_file();
	fprintf(fp, "R=Succeeded[%i] ", status);
	pthread_exit(NULL);
}

void *nontrans_W() {
	int j;

	for (j = 0; j < 2000; j++);

	shared_int.counter = 5;

	fprintf(fp, "W=Succeeded[%i] ", -1);
	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w;
	pthread_t thread_r;
	int rc;
	void** res;
	check_file();

	memset(&shared_int, 0, sizeof (padded_scalar_t));
	memset(&shared_int2, 0, sizeof (padded_scalar_t));
	memset(&shared_int3, 0, sizeof (padded_scalar_t));

	int i;
	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		fprintf(fp, "\n");
		pthread_create(&thread_r, NULL, R, (void *) 2);

		int j;
		for (j = 0; j < 2000; j++);

		pthread_create(&thread_w, NULL, nontrans_W, (void *) 1);
		pthread_join(thread_w, res);
		pthread_join(thread_r, res);
	}

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}

