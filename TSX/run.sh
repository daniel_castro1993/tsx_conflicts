#!/bin/sh
# 
# File:   run.sh
# Author: daniel
#
# Created on 5/nov/2015, 18:42:15
#

make clean
make

for file in *.out
do
	echo "\n<<<<<\n$file\n>>>>>" && ./$file
done

echo "" >> experiment.txt
date >> experiment.txt
echo "" >> experiment.txt
echo "###########################" >> experiment.txt
echo "############### WITH COMMIT" >> experiment.txt
echo "###########################" >> experiment.txt
for file in *.out.plot
do    
	echo "\n<<<<<\n$file\n>>>>>" && python ./plotRW.py $file
	echo "\n<<<<<\n$file\n>>>>>" >> experiment.txt && python ./plotRW.py $file >> experiment.txt
done

make clean
make explicit="-D ABORT_EXPLICIT=\"1\""

echo "" >> experiment.txt
echo "" >> experiment.txt
echo "###########################" >> experiment.txt
echo "################ WITH ABORT" >> experiment.txt
echo "###########################" >> experiment.txt
for file in *.out
do
	echo "\n<<<<<\n$file\n>>>>>" && ./$file
done

for file in *.out.plot
do
	echo "\n<<<<<\n$file\n>>>>>" && python ./plotRW.py $file
	echo "\n<<<<<\n$file\n>>>>>" >> experiment.txt && python ./plotRW.py $file >> experiment.txt
done
