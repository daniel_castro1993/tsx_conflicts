#!/bin/bash

gcc -O0 -mrtm ./capacity/TSX_stress_cap_seq.c -o assoc.out -lpthread -lrt -I. -DPLOT_FILE="\"stress_cap.txt\"" -march=native -DEXPERIMENT_SAMPLES=50000; # -S
for i in {1..1000}
do
  for p in 0.0
  do
    for m in 410 420 430 440 450 460 470 480 490 500 510 520
    do
      ./assoc.out PROB_READ $p NUM_ACCESSES $m ; wait ;
    done ;
    wait ;
  done
done
