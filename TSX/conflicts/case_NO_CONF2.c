#include <signal.h>

#include "tm.h"

#ifndef MAX_THREADS
#define MAX_THREADS 2
#endif

#ifndef POOL_SIZE
#define POOL_SIZE 100000
#endif

#ifndef NB_ACCESSES
#define NB_ACCESSES 10
#endif

#ifndef OFFSET
#define OFFSET 10
#endif

#define MULT_TIME 100

padded_scalar_t* shared_int;

int sync_enter, sync_exit;
int read_values[MAX_THREADS][EXPERIMENT_SAMPLES];

int errors[MAX_THREADS][ERRORS_T_SIZE];

void * T(void* me_ptr)
{
    int i;
    padded_scalar_t cpy, *si = shared_int;
    register int me = *((int*) me_ptr), index = me - 1;
    register int j, k;

    for (i = 0; i < POOL_SIZE; i += 64) {
        si[i].counter = 1;
    }

    for (i = 0; i < EXPERIMENT_SAMPLES; i++) {

        __sync_add_and_fetch(&sync_enter, 1);
        while (!__sync_bool_compare_and_swap(&sync_enter,
                                             MAX_THREADS * (i + 1),
                                             MAX_THREADS * (i + 1)));

        START_TRANSAC(errors[index]);

        for (j = 0; j < me * MULT_TIME; j++);

        for (k = 0; k < NB_ACCESSES; ++k) {
            si[index * OFFSET + (k * MAX_THREADS * OFFSET)].counter = me; // index + (k * MAX_THREADS)
            for (j = 0; j < MULT_TIME; j++);
        }

        END_TRANSAC(errors[index]);

        __sync_add_and_fetch(&sync_exit, 1);
        while (!__sync_bool_compare_and_swap(&sync_exit,
                                             MAX_THREADS * (i + 1),
                                             MAX_THREADS * (i + 1)));
    }

    return NULL;
}

int main()
{
    pthread_t thread_w[MAX_THREADS];
    cpu_set_t cpuset[MAX_THREADS];
    pthread_attr_t attrs[MAX_THREADS];
    int W_ID[MAX_THREADS];
    void *res1;
    int i, j;
    int cpuid = 0;

    shared_int = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * POOL_SIZE);
    //    memset(shared_int, 0, sizeof (padded_scalar_t));

    for (j = 0; j < MAX_THREADS; ++j) {
        W_ID[j] = j + 1;
        CPU_ZERO(&(cpuset[j]));
        CPU_SET(cpuid, &(cpuset[j]));
        cpuid++; // different physical cores I guess
        pthread_attr_init(&(attrs[j]));
        pthread_attr_setaffinity_np(&(attrs[j]),
                                    sizeof (cpu_set_t), &(cpuset[j]));

        pthread_create((pthread_t*) &(thread_w[j]),
                       &(attrs[j]), T, (void*) &(W_ID[j]));
    }
    for (j = 0; j < MAX_THREADS; ++j) {
        pthread_join(thread_w[j], &res1);
    }

    printf("BEGIN_ADDR: %p\n", shared_int);

    check_file();
    for (j = 0; j < MAX_THREADS; ++j) {
        fprintf(fp, "W%i: ", j + 1);
        for (i = 0; i < ERRORS_T_SIZE; ++i) {
            fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
                    errors[j][i], (double) errors[j][i] /
                    (double) EXPERIMENT_SAMPLES * 100.0);
        }
        fprintf(fp, "\n");
    }
    fclose(fp);

    FILE *conf_fp = fopen("NO_CONF2.txt", "a");
    fseek(conf_fp, 0, SEEK_END);
    if (ftell(conf_fp) < 8) {
        fprintf(conf_fp, "%12s, %12s, %12s, %12s, ",
                "NUM_THREADS", "NUM_ACCESSES", "BEGIN_ADDR", "POOL_SIZE");
        for (j = 0; j < MAX_THREADS; ++j) {
            fprintf(conf_fp, "%12s[THR%i] (%%)", "CONFLICT", j);
            if (j != MAX_THREADS - 1) {
                fprintf(conf_fp, ",");
            }
        }
        fprintf(conf_fp, "\n");
    }
    fprintf(conf_fp, "%10i, %10i, %12p, %12i, ", MAX_THREADS, NB_ACCESSES,
            shared_int, POOL_SIZE);
    for (j = 0; j < MAX_THREADS; ++j) {
        fprintf(conf_fp, "%12f", (double) errors[j][CONFLICT] /
                (double) EXPERIMENT_SAMPLES * 100.0);
        if (j != MAX_THREADS - 1) {
            fprintf(conf_fp, ", ");
        }
    }
    fprintf(conf_fp, "\n");

    for (j = 0; j < MAX_THREADS; ++j) {
        fprintf(stdout, "W%i: ", j + 1);
        for (i = 0; i < ERRORS_T_SIZE; ++i) {
            fprintf(stdout, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
                    errors[j][i], (double) errors[j][i] /
                    (double) EXPERIMENT_SAMPLES * 100.0);
        }
        fprintf(stdout, "\n");
    }

    //	printf("final value counter: %i\n", (int) shared_int->counter);

    /* Last thing that main() should do */
    pthread_exit(NULL);

    return 0;
}
