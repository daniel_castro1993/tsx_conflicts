import re
import sys

file_name = sys.argv[1]
file_input = open(file_name, "r")
file_lines = file_input.readlines()

data = []
resumed = {}

for line in file_lines:
  status = re.match("(?P<process1>\w+)=(Failed|Succeeded)\[(?P<status1>[+-]?\d+)\] (?P<process2>\w+)=(Failed|Succeeded)\[(?P<status2>[+-]?\d+)\]", line)
  if status != None:
    res = status.group('process1') + '[' + status.group('status1') + '] ' + status.group('process2') + '[' + status.group('status2') + ']'
    data.append(res)

for result in data:
  if result not in resumed:
    resumed[result] = 1
  else:
    resumed[result] += 1

for result in resumed:
  print result + ": " + str(resumed[result]) + " times"
