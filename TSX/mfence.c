#include "tm.h"

#ifndef READ_FROM_POOL
#define READ_FROM_POOL 10
#endif

#ifndef LOOPING
#define LOOPING 1000
#endif

int *pool;

int main () {
	int status;
	int i, j, k;

	pool = (int*) malloc(sizeof (int) * 1000);

	// writes somewhere in the heap
	for (i = 0; i < 40; ++i) {
		pool[i] = 10;
	}
	pool[20] = 50;

#ifndef DISABLE_CLFLUSH
	for (i = 0; i < 40; ++i) {
		asm volatile ("clflush (%0)" ::"r"(&(pool[i]))); // flushes and invalidate
	}
#endif

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		printf("MFENCE not allowed!\n");
		return EXIT_FAILURE; // if abort and execute after this -> SIGSEGV 
	}

	for (i = 0; i < LOOPING; ++i) ;

	i = pool[READ_FROM_POOL]; // aborts a lot (machine on node35) if READ_FROM_POOL > 11 and DISABLE_CLFLUSH off (independent on DISABLE_MFENCE on or off)
	j = 20;
	k = 30;

	pool[0] = i;
	pool[1] = j;
	pool[2] = k;

#ifndef DISABLE_MFENCE
	asm volatile ("mfence" :: : "memory");
	// __sync_synchronize(); // same as before?
#endif

	_xend();

	printf("end 0=%i, 1=%i, 2=%i\n", pool[0], pool[1], pool[2]);

	free(pool);

	return EXIT_SUCCESS;
}
