#include "tm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LARGE_NUMBER  0xFFFF

#ifndef NWAY_ACCESSES
#define NWAY_ACCESSES 9
#endif

#define SET_WAY_OFFSET(_ADDR, _TIMES) (                                       \
    ((padded_scalar_t*) _ADDR) + (_TIMES * 512)                               \
  )

#define WAY_OFFSET(_ADDR, _TIMES) (                                           \
    _TIMES < 8 ? (((padded_scalar_t*) _ADDR) + (_TIMES * 64))                 \
               : (((padded_scalar_t*) _ADDR) + ((_TIMES - 7) * 512))          \
  )

#ifdef __SAME_SET_WAY__
#define OFFSET(_ADDR, _TIMES) SET_WAY_OFFSET(_ADDR, _TIMES)
#else
#define OFFSET(_ADDR, _TIMES) WAY_OFFSET(_ADDR, _TIMES)
#endif

#define WRITE_ADDRS_0(_POOL)                                                  \
  ((padded_scalar_t*) OFFSET(_POOL, 0))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 0))->counter2++;

#define WRITE_ADDRS_1(_POOL)                                                  \
  WRITE_ADDRS_0(_POOL)                                                        \
  ((padded_scalar_t*) OFFSET(_POOL, 1))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 1))->counter2++;

#define WRITE_ADDRS_2(_POOL)                                                  \
  WRITE_ADDRS_1(_POOL)                                                        \
  ((padded_scalar_t*) OFFSET(_POOL, 2))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 2))->counter2++;

#define WRITE_ADDRS_3(_POOL)                                                  \
  WRITE_ADDRS_2(_POOL)                                                        \
  ((padded_scalar_t*) OFFSET(_POOL, 3))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 3))->counter2++;

#define WRITE_ADDRS_4(_POOL)                                                  \
  WRITE_ADDRS_3(_POOL)                                                        \
  ((padded_scalar_t*) OFFSET(_POOL, 4))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 4))->counter2++;

#define WRITE_ADDRS_5(_POOL)                                                  \
  WRITE_ADDRS_4(_POOL);                                                       \
  ((padded_scalar_t*) OFFSET(_POOL, 5))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 5))->counter2++;

#define WRITE_ADDRS_6(_POOL)                                                  \
  WRITE_ADDRS_5(_POOL);                                                       \
  ((padded_scalar_t*) OFFSET(_POOL, 6))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 6))->counter2++;

#define WRITE_ADDRS_7(_POOL)                                                  \
  WRITE_ADDRS_6(_POOL);                                                       \
  ((padded_scalar_t*) OFFSET(_POOL, 7))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 7))->counter2++;

#define WRITE_ADDRS_8(_POOL)                                                  \
  WRITE_ADDRS_7(_POOL);                                                       \
  ((padded_scalar_t*) OFFSET(_POOL, 8))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 8))->counter2++;

#define WRITE_ADDRS_9(_POOL)                                                  \
  WRITE_ADDRS_8(_POOL);                                                       \
  ((padded_scalar_t*) OFFSET(_POOL, 9))->counter++;                           \
  ((padded_scalar_t*) OFFSET(_POOL, 9))->counter2++;

#if defined NWAY_ACCESSES && NWAY_ACCESSES == 0
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_0(_POOL) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 1
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_1(_POOL) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 2
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_2(_POOL) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 3
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_3(_POOL) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 4
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_4(_POOL) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 5
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_5(_POOL) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 6
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_6(_POOL) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 7
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_7(_POOL) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 8
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_8(_POOL) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 9
#define WRITE_ADDRS(_POOL) WRITE_ADDRS_9(_POOL) 
#endif

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t* mem_pool;
padded_scalar_t* addr_X;
padded_scalar_t* addr_X_[NWAY_ACCESSES];

void * W(void * arg) {
	register padded_scalar_t* begin = (padded_scalar_t*) arg;
	int status;

	WRITE_ADDRS(begin); // Loads into cache
	WRITE_ADDRS(begin);
	WRITE_ADDRS(begin);

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		TSX_ERROR_INC(status, errors_1);
		pthread_exit(NULL);
	}

	WRITE_ADDRS(begin);

	_xend();

	TSX_ERROR_INC(status, errors_1);
	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w;
	cpu_set_t cpuset;

	int i;
	void * res;

	padded_scalar_t * begin_pool, * end_pool, * it;

	printf("USING ASSOC=%i\n\n", NWAY_ACCESSES);

	mem_pool = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);
	begin_pool = (padded_scalar_t*) mem_pool;
	end_pool = begin_pool + LARGE_NUMBER;

	printf("START ADDR=%p, END ADDR=%p\n", begin_pool, end_pool);

	for (i = 0; i < NWAY_ACCESSES + 1; ++i) {
		padded_scalar_t* addr = OFFSET(begin_pool, i);
		printf(" %i) ADDR=%p\n", i, addr);
	}

	memset(begin_pool, 0, sizeof (padded_scalar_t) * LARGE_NUMBER);

	printf("\n\n");

	CPU_ZERO(&cpuset);
	CPU_SET(0, &cpuset);

	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpu_set_t), &cpuset);

	CPU_ZERO(&cpuset);
	CPU_SET(1, &cpuset);
	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpuset), &cpuset);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		pthread_create(&thread_w, NULL, W, (void *) begin_pool);
		pthread_join(thread_w, &res);
	}

	check_file();
	fprintf(fp, "W1: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i]
				/ (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n\n");
	fclose(fp);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
