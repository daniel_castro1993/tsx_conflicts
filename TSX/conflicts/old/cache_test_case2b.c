#include "tm.h"

#define LARGE_NUMBER  0xFFFF

/**
 *        W
 *    starts
 *    write X
 *    write X'
 *    commits
 *
 * X and X' have the same Set and Way bits,
 * but different Tag bits
 */

padded_scalar_t mem_pool[LARGE_NUMBER];
padded_scalar_t * addr_X;
padded_scalar_t * addr_X_[8];

void * W(void * arg) {
	int j, status;

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		TSX_ERROR_INC(status, errors_1);
		pthread_exit(NULL);
	}

	for (j = 0; j < 2000; j++);

	addr_X->counter = 7;
	memset(addr_X, -1, sizeof (padded_scalar_t));
	addr_X->counter2 = 9;

	for (j = 0; j < 2000; j++);

	_xend();

	TSX_ERROR_INC(status, errors_1);
	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w;
	cpu_set_t cpuset;

	int i, rc;
	void * res;

	padded_scalar_t * begin_pool = (padded_scalar_t*) mem_pool,
			* end_pool = begin_pool + LARGE_NUMBER,
			* it;
	unsigned long mask = SET_MASK | WAY_MASK, addr_X_masked;

	addr_X = begin_pool;
	addr_X->counter = 0xff;

	addr_X_masked = mask & (unsigned long) addr_X;

	i = 0;
	for (it = end_pool - 1; it >= begin_pool; --it) {
		if (((unsigned long) it & mask) == addr_X_masked) {
			addr_X_[i] = it;
			addr_X_[i]->counter = 0xff;
			i++;
			if (i == 8) {
				break;
			}
		}
	}

	check_file();

	fprintf(fp, "addr_X=[%p] ", addr_X);
	for (i = 0; i < 8; ++i) {
		fprintf(fp, "addr_X%i=[%p] ", i + 2, addr_X_[i]);
	}
	fprintf(fp, "\n");

	CPU_ZERO(&cpuset);
	CPU_SET(0, &cpuset);

	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpu_set_t), &cpuset);

	memset(mem_pool, 0, sizeof (mem_pool));

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		pthread_create(&thread_w, NULL, W, (void *) 1);
		pthread_join(thread_w, &res);
	}

	check_file();
	fprintf(fp, "\nW1: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i] / (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n");
	fclose(fp);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
