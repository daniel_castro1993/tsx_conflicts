#include "tm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LARGE_NUMBER  0x2FFFFF

#ifndef PROB_READ
#define PROB_READ 1.00
#endif

unsigned rand_r_fnc_seed;

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t* mem_pool;

int max = 0;
int sum_i = 0;

void * W(void * arg) {
	register padded_scalar_t* begin = (padded_scalar_t*) arg;
	register int status;
	register int i = 0;
	register int j = 0;
	register int k = 0;
	int l = 0, m = 0;
	int nb_accesses = 0, nb_capa_in_row = 0, nb_other_in_row = 0;

	i = begin[0].counter;

	status = -1;
	while (nb_capa_in_row < 3 && nb_other_in_row < 10) {

		nb_accesses++;

		int accs[nb_accesses + 1];

		k = rand_r(&rand_r_fnc_seed) % LARGE_NUMBER;
		j = k;
		for (i = 0; i < nb_accesses + 1; ++i) {
			accs[i] = begin[j].counter = rand_r(&rand_r_fnc_seed) % LARGE_NUMBER;
			begin[j].counter2 = (double) rand_r(&rand_r_fnc_seed)
					/ (double) RAND_MAX > PROB_READ;
			for (l = 0; l < i; ++l) {
				if (accs[i] == accs[l]) {
					begin[j].counter = accs[i - 1];
					--i;
					break;
				}
			}
			j = begin[j].counter;
		}

		j = nb_accesses;

		status = _xbegin();
		if (status != _XBEGIN_STARTED) {
			if (status == _XABORT_CAPACITY) {
				nb_capa_in_row++;
				nb_accesses--;
			} else {
				nb_other_in_row++;
			}
			TSX_ERROR_INC(status, errors_1);
			continue;
		}

		while (j > 0) {
			j--;
			i = begin[k].counter; // always reads
			if (begin[k].counter2) {
				begin[k].counter2 = i; // WRITE
			}
			k = i;
		}

		_xend();
		nb_other_in_row = 0;
		nb_capa_in_row = 0;
		TSX_ERROR_INC(status, errors_1);
	}

	//  if (!(nb_other_in_row < 10) && nb_capa_in_row < 3) {
	//    printf("{[WARN|OTHERS]: nb_accesses=%i}", nb_accesses);
	//  }

	if ((nb_accesses - 1) > max) {
		max = (nb_accesses - 1);
	}

	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w;
	cpu_set_t cpuset;

	int i;
	void * res;

	padded_scalar_t * begin_pool, * end_pool, * it;

	srand(time(NULL));
	rand_r_fnc_seed = rand();

	printf("USING PROB_READ=%f\n\n", PROB_READ);

	CPU_ZERO(&cpuset);
	CPU_SET(0, &cpuset);

	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpu_set_t), &cpuset);

	CPU_ZERO(&cpuset);
	CPU_SET(1, &cpuset);
	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpuset), &cpuset);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		mem_pool = (padded_scalar_t*)
				malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);
		begin_pool = (padded_scalar_t*) mem_pool;
		end_pool = begin_pool + LARGE_NUMBER;
		pthread_create(&thread_w, NULL, W, (void *) begin_pool);
		pthread_join(thread_w, &res);
		free(mem_pool);

		if (i % 100 == 0) {
			printf(".s%i(max=%i).", i, max);
			fflush(stdout);
		}
	}

	check_file();
	fprintf(fp, "W (PROB_READ=%4f): ", PROB_READ);
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i]
				/ (double) EXPERIMENT_SAMPLES);
	}
	fprintf(fp, "\n");
	fprintf(fp, "max reads: %i\n", max);
	fprintf(fp, "\n\n");
	fclose(fp);

	printf("\nmax nb_accesses = %i\n\n", max);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
