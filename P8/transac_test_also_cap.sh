#!/bin/bash
gcc -g -O0 -mhtm ./conflicts/P8_conf_test.c -o P8_conf_test.out -lpthread -lrt -I.
for i in {0..200}
do
  for p in 0.9 0.1 0.05
  do
    for d in 10000
    do
      for l in 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 60 62 64 
      do
        for t in 4
        do
          for b in 5
          do
            timeout 1m nice -n 19 ./P8_conf_test.out THREADS $t BUDGET $b D $d PW $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;

  # for p in 0.9 0.1 0.05
  # do
    # for d in 500 750 1000 1500 2000 4000 5000 6000 7000 8000
    # do
      # for l in 20 30 40
      # do
        # for t in 4
        # do
          # for b in 5
          # do
            # timeout 1m nice -n 19 ./P8_conf_test.out THREADS $t BUDGET $b D $d PW $p L $l ;
          # done;
        # done;
      # done;
    # done;
  # done;
  # wait ;
done

