#include "P8_tm.h"

#define MAX_NUMB_THREADS 64

#define MAX_RAND_R_FNC  0x80000000

//#define RAND_R_FNC(_SEED, _AUX) ({ \
//    _AUX = _SEED; \
//    _AUX *= 1103515245; \
//    _AUX += 12345; \
//    _SEED = (_AUX / 65536) % 2048; \
//    _AUX *= 1103515245; \
//    _AUX += 12345; \
//    _SEED <<= 10; \
//    _SEED ^= (unsigned long long) (_AUX / 65536) % 2048; \
//    _AUX *= 1103515245; \
//    _AUX += 12345; \
//    _SEED <<= 10; \
//    _SEED ^= (unsigned long long) (_AUX / 65536) % 2048; \
//    _SEED; \
//  })

#define RAND_R_FNC(seed) ({ \
	unsigned long long next = *(seed); \
	unsigned long long result; \
	next *= 1103515245; \
	next += 12345; \
	result = (unsigned long long) (next / 65536) % 2048; \
	next *= 1103515245; \
	next += 12345; \
	result <<= 10; \
	result ^= (unsigned long long) (next / 65536) % 1024; \
	next *= 1103515245; \
	next += 12345; \
	result <<= 10; \
	result ^= (unsigned long long) (next / 65536) % 1024; \
	*(seed) = next; \
	result; \
})

// *_SEED ^= aux;

#define IS_WRITE(_WRITE_PROB, _SEED) ({ \
	unsigned long long rand_val = RAND_R_FNC(_SEED); \
	rand_val = rand_val % 999999; \
    ((double) rand_val / (double) 999999) < _WRITE_PROB; \
  })

#define read_granule(g) ({ \
	int res; \
    res = (g)->counter + (g)->counter2; \
	res; \
})

#define write_granule(g) ({ \
    (g)->counter++; \
})

#define UPDATE_BUDGET(_IS_ABORT, _BUDGET) ({ \
    _BUDGET = _IS_ABORT ? max(_BUDGET - 1, 0) : _BUDGET; \
  })

// __TM_abort();

#define CHECK_GLOBAL_LOCK(lock, in_xact) ({ \
    if(lock->counter) { \
      if(in_xact) { \
        __TM_abort(); \
      } else { \
        pthread_mutex_lock(&global_lock); \
        pthread_mutex_unlock(&global_lock); \
      }\
    }\
  })

#define ENTER_FALLBACK(lock, mutex) ({\
    pthread_mutex_lock(&mutex);\
    lock->counter = true;\
  })

#define EXIT_FALLBACK(lock, mutex) ({\
    lock->counter = false;\
    pthread_mutex_unlock(&mutex);\
  })

padded_scalar_t * mem_pool;

errors_t ** errors_a;
errors_t ** errors_a_snp;
int * count_aborts, * count_commits;
int * count_aborts_snp, * count_commits_snp;
int * count_serls;
int * count_serls_snp;
double * count_time;
double * count_time_snp;

int * tids;
int * per_tid;
unsigned int * seeds;

int number_of_completed_threads = 0;

int THREADS__ = 4;
int BUDGET__ = 5;
int D__ = 5000;
int L__ = 10;
double WRITE_PROB__ = 1.0;

double avgC = 0.0;

int sync_enter = 0;

pthread_mutex_t global_lock;
padded_scalar_t * global_lock_held;

unsigned long long time_xact[MAX_NUMB_THREADS];
unsigned long long numb_runs[MAX_NUMB_THREADS];
unsigned long long time_serl[MAX_NUMB_THREADS];
unsigned long long numb_serl[MAX_NUMB_THREADS];
unsigned long long time_total[MAX_NUMB_THREADS];
unsigned long long numb_total[MAX_NUMB_THREADS];
unsigned long long time_aborts[MAX_NUMB_THREADS];
unsigned long long numb_aborts[MAX_NUMB_THREADS];
unsigned long long time_notx[MAX_NUMB_THREADS];
unsigned long long numb_notx[MAX_NUMB_THREADS];
unsigned long long time_commit[MAX_NUMB_THREADS];
unsigned long long numb_commit[MAX_NUMB_THREADS];

unsigned long long time_xact_snp[MAX_NUMB_THREADS];
unsigned long long numb_runs_snp[MAX_NUMB_THREADS];
unsigned long long time_serl_snp[MAX_NUMB_THREADS];
unsigned long long numb_serl_snp[MAX_NUMB_THREADS];
unsigned long long time_total_snp[MAX_NUMB_THREADS];
unsigned long long numb_total_snp[MAX_NUMB_THREADS];
unsigned long long time_aborts_snp[MAX_NUMB_THREADS];
unsigned long long numb_aborts_snp[MAX_NUMB_THREADS];
unsigned long long time_notx_snp[MAX_NUMB_THREADS];
unsigned long long numb_notx_snp[MAX_NUMB_THREADS];
unsigned long long time_commit_snp[MAX_NUMB_THREADS];
unsigned long long numb_commit_snp[MAX_NUMB_THREADS];

unsigned long long last_ts[MAX_NUMB_THREADS];

void iter_fill_addrs_f(unsigned * addrs, int * seed, int l, int d, int i)
{
	int j;
	bool is_new = true;
	// *seed ^= value_from_mem;
	do {
		if (!(l < d)) {
			addrs[i] = i;
		}
		else {
			addrs[i] = RAND_R_FNC(seed); // rand_r(seed)
			addrs[i] = addrs[i] % d;
			for (j = 0; j < i; ++j) {
				if (addrs[j] == addrs[i]) {
					is_new = false;
					break;
				}
			}
			if (!is_new) {
				is_new = true;
				continue;
			}
		}
		break;
	}
	while (true);
}

#define do_xact_macro(pool, seed, prob_w, l, d) ({ \
    register int i; \
    register unsigned long long j; \
    for (i = 0; i < l; ++i) { \
        j = RAND_R_FNC(seed); \
        j = j % d; \
        if (IS_WRITE(prob_w, seed)) { \
            write_granule(&(pool[j])); \
        } \
        else { \
            read_granule(&(pool[j])); \
        } \
    } \
})

void do_xact_f(int tid, int thrs, padded_scalar_t * pool, int * seed,
			   double prob_w, int l, int d)
{

	int i;
	unsigned long long j;
	int addrs[l];
#ifdef TEST_RO
	for (i = 0; i < l; ++i) {
		read_granule(&(pool[0]));
		for (j = 0; j < 2000 / l; ++j);
	}
#elif TEST_WO
	for (i = 0; i < l; ++i) {
		unsigned long long r_val = RAND_R_FNC(seed); // rand_r(seed)
		r_val = r_val % d;
		// *seed ^= addrs[i];
		for (j = 0; j < 10; ++j);
		write_granule(&(pool[(tid)+(i * thrs)]));
		// printf(" [%2i]%4lu ", tid, r_val);
	}
#else
	for (i = 0; i < l; ++i) {
		iter_fill_addrs_f(addrs, seed, l, d, i);
		// for (j = 0; j < 100 / l; ++j);
		if (IS_WRITE(prob_w, seed)) {
			write_granule(&(pool[addrs[i]]));
		}
		else {
			read_granule(&(pool[addrs[i]]));
		}
	}
#endif /* TEST */
}

// (padded_scalar_t * pool, int * seed, double prob_w, int l, int d)

void xact(int tid, unsigned int * seed)
{
	int i, status, addrs[L__];
	int budget = BUDGET__;
	int thrs = THREADS__;
	int local_variable;
	int cp_seed = *seed;
	register padded_scalar_t * mem_pool_loc = mem_pool;
	padded_scalar_t * global_lock_held_loc = global_lock_held;
	register double write_prob = WRITE_PROB__;
	register int l = L__;
	register int d = D__;
	padded_scalar_t lock;

	unsigned long long ts0, ts1, ts2, ts3, ts_commit;
	unsigned long long discount_init = 0;

	ts0 = rdtsc();

	while (budget > 0) {

		//lock = *global_lock_held_lock;
		CHECK_GLOBAL_LOCK(global_lock_held_loc, false);

		// printf("[%4i] CHECKED LOCK!\n", tid);
		// fflush(stdout);
		// sleep(1);

		ts1 = rdtsc();
		TM_buff_type TM_buff;
		//lock = *global_lock_held_lock;

		if (__TM_begin(TM_buff) != _HTM_TBEGIN_STARTED) {
			// we aborted

			// printf("[%4i] ABORTED!\n", tid);
			// fflush(stdout);
			// sleep(1);

			if (__TM_is_conflict(TM_buff)) {
				UPDATE_BUDGET(true, budget);
			}
			count_aborts[tid]++;
			errors_a[tid][ABORT]++;
			P8_ERROR_INC(TM_buff, errors_a[tid]);

			ts2 = rdtsc();

			time_aborts[tid] += ts2 - ts1;
			numb_aborts[tid] += 1;

			if (__TM_is_footprint_exceeded(TM_buff)) {
				budget = 0;
			}

		}
		else {
			// we started
			
			lock.counter = global_lock_held_loc->counter;

			// reads the lock, aborts on someone locking
			CHECK_GLOBAL_LOCK((&lock), true);

			// do xact
			// do_xact_f(tid, thrs, mem_pool_loc, &cp_seed, write_prob, l, d);
			do_xact_macro(mem_pool_loc, &cp_seed, write_prob, l, d);

			ts_commit = rdtsc();
			__TM_end();
			ts2 = rdtsc();

			time_commit[tid] += ts2 - ts_commit;
			numb_commit[tid] += 1;

			time_xact[tid] += ts2 - ts1;
			numb_runs[tid] += 1;

			// completed successfully
			count_commits[tid]++;
			break;
		}
	}

	if (!(budget > 0)) {
		ts1 = rdtsc();
		//lock = *global_lock_held_lock;
		ENTER_FALLBACK(global_lock_held_loc, global_lock);
		do_xact_macro(mem_pool_loc, &cp_seed, write_prob, l, d);
		EXIT_FALLBACK(global_lock_held_loc, global_lock);
		count_serls[tid]++;
		ts2 = rdtsc();

		time_serl[tid] += ts2 - ts1;
		numb_serl[tid] += 1;
	}

	ts3 = rdtsc();

	if (last_ts[tid] == 0) {
		last_ts[tid] = ts3;
	}
	else {
		time_notx[tid] += ts0 - last_ts[tid];
		numb_notx[tid] += 1;
		last_ts[tid] = ts3;
	}

	time_total[tid] += ts3 - ts0;
	numb_total[tid] += 1;
}

void * run_xacts(void * args)
{
	int i, tid = *((int*) args);
	//  struct timespec start, finish, elapsed;
	unsigned long long start, finish, elapsed;
	unsigned int seed = seeds[tid];

	//	printf("[THR%i] Started\n", tid);

	__sync_add_and_fetch(&sync_enter, 1);
	while (!__sync_bool_compare_and_swap(&sync_enter, THREADS__, THREADS__));

	// ignore some initial data
	for (i = 0; i < 50; ++i) {
		xact(tid, &seed);
	}

	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		errors_a[tid][i] = 0;
	}

	count_aborts[tid] = 0;
	count_commits[tid] = 0;
	count_serls[tid] = 0;

    time_xact[tid] = 0;
    numb_runs[tid] = 0;
    time_serl[tid] = 0;
    numb_serl[tid] = 0;
    time_total[tid] = 0;
    numb_total[tid] = 0;
    time_aborts[tid] = 0;
    numb_aborts[tid] = 0;
    time_notx[tid] = 0;
    numb_notx[tid] = 0;
    time_commit[tid] = 0;
    numb_commit[tid] = 0;

	//	printf("[THR%i] Start collecting data\n", tid);

	start = rdtsc();
	for (i = 0; i < EXPERIMENT_SAMPLES; ++i) {
		int j;
		// seed ^= j;
		unsigned long long r_val = RAND_R_FNC(&seed);
		r_val = r_val % 100;
		for (j = 0; j < r_val; ++j); // not allow transactions to run serially
		xact(tid, &seed);
	}
	finish = rdtsc();

	//	printf("[THR%i] Ended collecting data\n", tid);

	__sync_add_and_fetch(&number_of_completed_threads, 1);

	elapsed = finish - start;

	count_time[tid] = (double) elapsed;

	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		errors_a_snp[tid][i] = errors_a[tid][i];
	}
    count_aborts_snp[tid] = count_aborts[tid];
    count_commits_snp[tid] = count_commits[tid];
    count_serls_snp[tid] = count_serls[tid];
    count_time_snp[tid] = count_time[tid];

    time_xact_snp[tid] = time_xact[tid];
    numb_runs_snp[tid] = numb_runs[tid];
    time_serl_snp[tid] = time_serl[tid];
    numb_serl_snp[tid] = numb_serl[tid];
    time_total_snp[tid] = time_total[tid];
    numb_total_snp[tid] = numb_total[tid];
    time_aborts_snp[tid] = time_aborts[tid];
    numb_aborts_snp[tid] = numb_aborts[tid];
    time_commit_snp[tid] = time_commit[tid];
    numb_commit_snp[tid] = numb_commit[tid];
    time_notx_snp[tid] = time_notx[tid];
    numb_notx_snp[tid] = numb_notx[tid];

	// keep running to get the other results
	while (number_of_completed_threads < THREADS__) {
		xact(tid, &seed);
	}

	//	printf("[THR%i] Exit\n", tid);

	return NULL;
}

int main(int argc, char ** argv)
{
	int i = 1, j, k;
	while (i < argc) {

		if (strcmp(argv[i], "L") == 0) {
			L__ = atoi(argv[i + 1]);
		}
		else if (strcmp(argv[i], "D") == 0) {
			D__ = atoi(argv[i + 1]);
		}
		else if (strcmp(argv[i], "BUDGET") == 0) {
			BUDGET__ = atoi(argv[i + 1]);
		}
		else if (strcmp(argv[i], "THREADS") == 0) {
			THREADS__ = atoi(argv[i + 1]);
		}
		else if (strcmp(argv[i], "PW") == 0) {
			WRITE_PROB__ = atof(argv[i + 1]);
		}
		i += 2;
	}

	// TEST RAND
	//	for (k = 0; k < 100; ++k) {
	//		printf("%c", IS_WRITE(0.5, &i) ? 'W' : 'R');
	//	}
	//	printf("\n");

	pthread_t threads[THREADS__];
	pthread_attr_t attr[THREADS__];
	cpu_set_t cpuset[THREADS__];
	void * res;
	FILE * data_file;
	double time_el = 0, aborts = 0, commits = 0, serls = 0, tot, ends,
            prob_confl, prob_capac, prob_abort, prob_other,
            prob_commit, resp_time, throughput, aborts_avg, non_transac_avg,
            t_commit_avg, conflicts = 0, capacities = 0, other = 0,
            avg_r_trans, avg_r_serl, avg_r_xact;

    double time_xact_snpt = 0, numb_runs_snpt = 0, numb_serl_snpt = 0,
            time_total_snpt = 0, numb_total_snpt = 0, time_aborts_snpt = 0,
            numb_aborts_snpt = 0, time_notx_snpt = 0, numb_notx_snpt = 0,
            time_commit_snpt = 0, numb_commit_snpt = 0, time_serl_snpt = 0;
	int explicit = 0;

	srand(time(NULL));

	mem_pool = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * (D__ + 1));
	global_lock_held = &(mem_pool[0]);
	mem_pool += 1;

	errors_a = (errors_t**) malloc(THREADS__ * sizeof (errors_t*));
	errors_a_snp = (errors_t**) malloc(THREADS__ * sizeof (errors_t*));
	seeds = (unsigned int*) malloc(THREADS__ * sizeof (unsigned int));
	for (i = 0; i < THREADS__; ++i) {
		errors_a[i] = (errors_t*) calloc(ERRORS_T_SIZE, sizeof (errors_t));
		errors_a_snp[i] = (errors_t*) calloc(ERRORS_T_SIZE, sizeof (errors_t));
		seeds[i] = rand();
	}
	count_aborts = (int*) calloc(THREADS__, sizeof (int));
	count_commits = (int*) calloc(THREADS__, sizeof (int));
	count_serls = (int*) calloc(THREADS__, sizeof (int));
	count_time = (double*) calloc(THREADS__, sizeof (double));
	count_aborts_snp = (int*) malloc(THREADS__ * sizeof (int));
	count_commits_snp = (int*) malloc(THREADS__ * sizeof (int));
	count_serls_snp = (int*) malloc(THREADS__ * sizeof (int));
	count_time_snp = (double*) malloc(THREADS__ * sizeof (double));
	tids = (int*) malloc(THREADS__ * sizeof (int));
	per_tid = (int*) malloc(THREADS__ * sizeof (int));

	printf("====================== Running P8 test ====================== \n");
	printf("PROB_WRITE=%.2f, THRS=%d, BUDG=%d, D=%d, L=%d\n",
		WRITE_PROB__, THREADS__, BUDGET__, D__, L__);

	data_file = fopen("transac_avg.txt", "a");
	fseek(data_file, 0, SEEK_END);
	if (ftell(data_file) < 8) {
		// not created yet
		fprintf(data_file, "%12s, %12s, %12s, %12s, %12s, %12s, %12s, "
				"%12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, "
				"%12s\n", "PROB_READ", "D", "L", "THREADS", "BUDGET",
				"ABORTS", "COMMITS", "SERLS", "CONFLICTS", "CAPACITY", "OTHER",
				"AVG_C_TRANS", "AVG_R_ABORTS", "AVG_R_SERL", "AVG_R_XACT",
				"NON_TRANSAC", "COMMIT_OP_LAT", "TOTAL_XACTS");
	}

	pthread_mutex_init(&global_lock, NULL);

	global_lock_held->counter = false;
	for (i = 0; i < D__; i += CACHE_LINE_SIZE) {
		mem_pool[i].counter = -1;
		mem_pool[i].counter2 = -1;
	}

	for (i = 0; i < THREADS__; ++i) {
		CPU_ZERO(cpuset + i);
		CPU_SET((i * 8) % 80, cpuset + i);
		pthread_attr_init(&(attr[i]));
		pthread_attr_setaffinity_np(&(attr[i]),
									sizeof (cpu_set_t), &(cpuset[i]));
		tids[i] = i;

		pthread_create(threads + i, &(attr[i]), run_xacts, (void *) (tids + i));
	}

	for (i = 0; i < THREADS__; ++i) {
		pthread_join(threads[i], &res);
	}

	for (i = 0; i < THREADS__; ++i) {
		time_el += count_time_snp[i];
        aborts += count_aborts_snp[i];
        commits += count_commits_snp[i];
        serls += count_serls_snp[i];
        conflicts += (double) errors_a_snp[i][CONFLICT];
        capacities += (double) errors_a_snp[i][CAPACITY];
        other += (double) errors_a_snp[i][OTHER];
        explicit += errors_a_snp[i][USER];

        time_xact_snpt += time_xact_snp[i];
        numb_runs_snpt += numb_runs_snp[i];
        time_serl_snpt += time_serl_snp[i];
        numb_serl_snpt += numb_serl_snp[i];
        time_total_snpt += time_total_snp[i];
        numb_total_snpt += numb_total_snp[i];
        time_aborts_snpt += time_aborts_snp[i];
        numb_aborts_snpt += numb_aborts_snp[i];
        time_notx_snpt += time_notx_snp[i];
        numb_notx_snpt += numb_notx_snp[i];
        time_commit_snpt += time_commit_snp[i];
        numb_commit_snpt += numb_commit_snp[i];
	}

	//    time_el /= (double) THREADS__;
	//    aborts /= (double) THREADS__;
	//    commits /= (double) THREADS__;
	//    serls /= (double) THREADS__;

	tot = aborts + commits + serls;
    ends = commits + serls;
    prob_commit = ends / tot;
    prob_confl = conflicts / (aborts);
    prob_capac = capacities / (aborts);
	prob_abort = aborts / (aborts + commits);
    prob_other = other / aborts;
    resp_time = time_el / ends;
    throughput = (double) THREADS__ / resp_time;
    //    throughput = ends / (time_el / (double) THREADS__);

    non_transac_avg = time_notx_snpt / numb_notx_snpt;
    t_commit_avg = time_commit_snpt / numb_commit_snpt;
    aborts_avg = time_aborts_snpt / numb_aborts_snpt;
    avg_r_trans = time_xact_snpt / numb_runs_snpt;
    avg_r_serl = time_serl_snpt / numb_serl_snpt;
    avg_r_xact = time_total_snpt / numb_total_snpt;

	fprintf(data_file, "%12.2f, %12d, %12d, %12d, %12d, %12e, %12e, "
			"%12e, %12e, %12e, %12e, %12e, %12e, %12e, %12e, %12e, %12e, "
			"%12e\n", 1.0 - WRITE_PROB__, D__, L__, THREADS__, BUDGET__,
			aborts, commits, serls, conflicts,
			capacities, other, avg_r_trans, aborts_avg, avg_r_serl,
			avg_r_xact, non_transac_avg, t_commit_avg, throughput);

	fclose(data_file);

	printf("Avg tran-commit time: %lf\n", avg_r_trans);
	printf("Avg serl-commit time: %lf\n", avg_r_serl);
	printf("Avg abort time      : %lf\n", aborts_avg);
	printf("Avg non-transac time: %lf\n", non_transac_avg);
	printf("Avg R complete xact : %lf\n", avg_r_xact);
	printf("Avg _xend() time    : %lf\n", t_commit_avg);
	printf("throughput          : %lf\n", throughput);
	printf("prob_commit         : %lf\n", prob_commit);
	printf("prob_abort          : %lf\n", prob_abort);
	printf("prob_confl          : %lf\n", prob_confl);
	printf("prob_capac          : %lf\n", prob_capac);
	printf("prob_serl           : %lf\n", numb_serl_snpt / ends);
	printf("prob_other          : %lf\n", prob_other);
	printf("EXPLICIT aborts     : %i\n", explicit);

	printf("======================== TESTS  ENDED ========================\n\n");

	pthread_mutex_destroy(&global_lock);


	return 0;
}
