#!/bin/bash

rm *.txt
#
#for p in 0.9 0.5 0.1
#do
#	cd ..
#	make clean && make SAMPLES=200000 PROFILE=1 USE_P8=1 FILE=./bench_L_pw$p.txt
#	cd results
#	for i in {1..30}
#	do
#	  ../stress_conflicts.out THREADS 6 BUDGET 4 D 8192 L $i PW $p
#	  wait ; wait ;
#	  sleep 0.1 ;
#	done
#done
#
#for i in 8 10 12
#do
#	cd ..
#	make clean && make SAMPLES=200000 PROFILE=1 USE_P8=1 FILE=./bench_pw_L$i.txt
#	cd results
#	for p in 1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 0.0
#	do
#	  ../stress_conflicts.out THREADS 6 BUDGET 4 D 1024 L $i PW $p
#	  wait ; wait ;
#	  sleep 0.1 ;
#	done
#done
for p in 1.0 0.9 0.1 0.05
do
    for d in 9999999 # 2000 8000 
    do
        for b in 6 # 60
        do
            for t in 1 2 # 60
            do
                cd ..
                make clean && make SAMPLES=200000 PROFILE=1 USE_P8=1 FILE=./bench_L_cap_pw$p.d$d.b$b.THRS$t.txt
                cd results
                for i in {1..64}
                do
                  ../stress_conflicts.out THREADS $t BUDGET $b D $d L $i PW $p
                  wait ; wait ;
                  sleep 0.1 ;
                done
            done
        done
    done
done
#
#for p in 0.2 0.1 0.05
#do
#	cd ..
#	make clean && make SAMPLES=200000 PROFILE=1 USE_P8=1 FILE=./bench_L_cap_pw$p.txt
#	cd results
#	for i in {1..64}
#	do
#	  ../stress_conflicts.out THREADS 1 BUDGET 4 D 2048 L $i PW $p
#	  wait ; wait ;
#	  sleep 0.1 ;
#	done
#done
#
#for p in 0.9 0.5 0.1
#do
#	cd ..
#	make clean && make SAMPLES=200000 PROFILE=1 USE_P8=1 FILE=./bench_B_pw$p.txt
#	cd results
#	for i in {1..8}
#	do
#	  ../stress_conflicts.out THREADS 4 BUDGET $i D 1024 L 12 PW $p
#	  wait ; wait ;
#	  sleep 0.1 ;
#	done
#done
#
#for p in 0.9 0.5 0.1 0.05
#do
#	cd ..
#	make clean && make SAMPLES=200000 PROFILE=1 USE_P8=1 FILE=./bench_THRS_pw$p.txt
#	cd results
#	for i in {1..10}
#	do
#	  ../stress_conflicts.out THREADS $i BUDGET 4 D 1024 L 8 PW $p
#	  wait ; wait ;
#	  sleep 0.1 ;
#	done
#done
#
#for p in 0.9 0.5 0.1 0.05
#do
#	cd ..
#	make clean && make SAMPLES=200000 PROFILE=1 USE_P8=1 FILE=./bench_D_pw$p.txt
#	cd results
#	for ((i=100;i<=2000;i += 50)); 
#	do
#	  ../stress_conflicts.out THREADS 6 BUDGET 4 D $i L 10 PW $p
#	  wait ; wait ;
#	  sleep 0.1 ;
#	done
#done
