#include "tm.h"

#include <cstdlib>
#include <mutex>
#include <pthread.h>
#include <thread>

#define RND_ACCESSES_SIZE 1000000

using namespace std;

int *HTM_sgl_is_locked_global;
thread_local int *HTM_sgl_is_locked;
static mutex mtx;
static int count_serl_txs;
static int nb_threads, budget, *is_record;

static int **errors;

thread_local int *rnd_accesses;
thread_local int rnd_accesses_ptr;

void TM_thr_init_rnd_array(int D) {
    int i = 0;
    unsigned seed = (unsigned) rdtsc();
    rnd_accesses = (int*) malloc(RND_ACCESSES_SIZE * sizeof (int));
    rnd_accesses_ptr = 0;
    for (i = 0; i < RND_ACCESSES_SIZE; ++i) {
        rnd_accesses[i] = ((unsigned) RAND_R_FNC(seed)) % D;
    }
}

int TM_get_rand() {
    rnd_accesses_ptr++;
    if (rnd_accesses_ptr >= RND_ACCESSES_SIZE) {
        rnd_accesses_ptr = rnd_accesses_ptr % RND_ACCESSES_SIZE;
    }
    return rnd_accesses[rnd_accesses_ptr];
}

void *(*func_ptr) (void *);
int D;

void *wait_func(void *arg) {

    HTM_sgl_is_locked = HTM_sgl_is_locked_global;

#ifdef INIT_RAND
    TM_thr_init_rnd_array(D);
#endif /* INIT_RAND */

    return func_ptr(arg);
}

int TM_exec(int _nb_threads, int _budget, int _D,
    void *(*thread_routine) (void *)) {

    int i;
    pthread_t threads[_nb_threads];
    pthread_attr_t attr[_nb_threads];
    cpu_set_t cpuset[_nb_threads];
    int *tids = (int*) malloc(_nb_threads * sizeof (int));
    void *res;

    D = _D;
    func_ptr = thread_routine;

    nb_threads = _nb_threads;
    budget = _budget;

    HTM_sgl_is_locked_global = (int*) malloc(sizeof (int));
    errors = (int**) malloc(nb_threads * sizeof (int*));
    is_record = (int*) malloc(nb_threads * sizeof (int));
    for (i = 0; i < nb_threads; ++i) {
        errors[i] = (int*) malloc(SIZEOF_ERRORS_T * sizeof (int));
    }

    *HTM_sgl_is_locked_global = false; // no one in the fallback at the beginning

    for (i = 0; i < nb_threads; ++i) {
        CPU_ZERO(cpuset + i);
        CPU_SET(i * THREAD_SPACING + THREAD_OFFSET, cpuset + i);
        pthread_attr_init(&(attr[i]));
        pthread_attr_setaffinity_np(&(attr[i]), sizeof (cpu_set_t), &(cpuset[i]));
        tids[i] = i;

        pthread_create(threads + i, &(attr[i]), wait_func, (void *) (tids + i));
    }

    for (i = 0; i < nb_threads; ++i) {
        pthread_join(threads[i], &res);
    }

    free(tids);
}

int TM_get_budget() {
    return budget;
}

void TM_set_budget(int _budget) {
    budget = _budget;
}

void TM_enter_fallback(int tid) {
    mtx.lock();
    if (is_record[tid]) {
        errors[tid][FALLBACK]++;
    }
    *HTM_sgl_is_locked = true;
}

void TM_exit_fallback(int) {
    *HTM_sgl_is_locked = false;
    mtx.unlock();
}

void TM_block() {
    mtx.lock();
    mtx.unlock();
}

int TM_read_lock() {
    return *HTM_sgl_is_locked;
}

int TM_update_budget(int tid, int budget, TM_STATUS_TYPE error) {
    int res = 0;

    if (is_record[tid]) {
        ERROR_INC(error, errors[tid]);
    }

    if (budget > 0) {
        res = budget - 1;
    }

    return res;
}

void TM_set_record_errors(int tid, int _is_record) {
    is_record[tid] = _is_record;
}

int TM_get_error_count(int error) {
    int i, res = 0;
    for (i = 0; i < nb_threads; ++i) {
        res += errors[i][error];
    }
    return res;
}
