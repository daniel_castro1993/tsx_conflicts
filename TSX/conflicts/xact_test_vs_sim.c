#include "tm.h"

#define MAX_NUMB_THREADS 64

#define read_granule(g) ({ \
	int res; \
    res = (g)->counter + (g)->counter2; \
	res; \
})

#define write_granule(g) ({ \
    (g)->counter++; \
	(g)->counter2++; \
})

#ifndef ACCESS_OFFSET
#define ACCESS_OFFSET 32
#endif

#ifndef SEED
#define SEED 2344562
#endif

#ifndef THREAD_OFFSET
#define THREAD_OFFSET 0
#endif

padded_scalar_t * mem_pool;

#ifdef DEBUG_TXS
padded_scalar_t * deb_pool;
#endif /* DEBUG_TXS */

errors_t ** errors_a;
errors_t ** errors_a_snp;
int * count_aborts, * count_commits;
int * count_aborts_snp, * count_commits_snp;
int * count_serls;
int * count_serls_snp;
double * count_time;
double * count_time_snp;

int * tids;
int * per_tid;
unsigned int * seeds;

int number_of_completed_threads = 0;

int THREADS__ = 4;
int BUDGET__ = 5;
int D__ = 5000;
int L__ = 10;
double WRITE_PROB__ = 1.0;

double avgC = 0.0;

int sync_enter = 0;

pthread_mutex_t global_lock;
padded_scalar_t * global_lock_held;

unsigned long long time_xact[MAX_NUMB_THREADS];
unsigned long long numb_runs[MAX_NUMB_THREADS];
unsigned long long time_serl[MAX_NUMB_THREADS];
unsigned long long numb_serl[MAX_NUMB_THREADS];
unsigned long long time_total[MAX_NUMB_THREADS];
unsigned long long numb_total[MAX_NUMB_THREADS];
unsigned long long time_aborts[MAX_NUMB_THREADS];
unsigned long long numb_aborts[MAX_NUMB_THREADS];
unsigned long long time_notx[MAX_NUMB_THREADS];
unsigned long long numb_notx[MAX_NUMB_THREADS];
unsigned long long time_commit[MAX_NUMB_THREADS];
unsigned long long numb_commit[MAX_NUMB_THREADS];

unsigned long long time_xact_snp[MAX_NUMB_THREADS];
unsigned long long numb_runs_snp[MAX_NUMB_THREADS];
unsigned long long time_serl_snp[MAX_NUMB_THREADS];
unsigned long long numb_serl_snp[MAX_NUMB_THREADS];
unsigned long long time_total_snp[MAX_NUMB_THREADS];
unsigned long long numb_total_snp[MAX_NUMB_THREADS];
unsigned long long time_aborts_snp[MAX_NUMB_THREADS];
unsigned long long numb_aborts_snp[MAX_NUMB_THREADS];
unsigned long long time_notx_snp[MAX_NUMB_THREADS];
unsigned long long numb_notx_snp[MAX_NUMB_THREADS];
unsigned long long time_commit_snp[MAX_NUMB_THREADS];
unsigned long long numb_commit_snp[MAX_NUMB_THREADS];

unsigned long long last_ts[MAX_NUMB_THREADS];

void iter_fill_addrs_f(unsigned * addrs, unsigned long long * seed,
                       int l, int d, int i)
{
    register int j;
    register bool is_new = true;
    // *seed ^= value_from_mem;

    do {
        if (!(l < d)) {
            addrs[i] = i;
        }
        else {
            addrs[i] = RAND_R_FNC(seed); // rand_r(seed)
            addrs[i] = addrs[i] % d;
            addrs[i] *= ACCESS_OFFSET; // (seems to avoid some spurious conflicts)
            for (j = 0; j < i; ++j) {
                if (addrs[j] == addrs[i]) {
                    is_new = false;
                    break;
                }
            }
            if (!is_new) {
                is_new = true;
                continue;
            }
        }
        break;
    }
    while (true);
}

//        if (IS_WRITE(prob_w, seed)) { \
            write_granule(&(pool[addrs[i]])); \
        } \
        else { \
            read_granule(&(pool[addrs[i]])); \
        } 
\

//  *seed ^= j;

#if TEST_WO
#define do_xact_macro(tid, thrs, pool, seed, prob_w, l, d) ({ \
    int i; \
    for (i = 0; i < l; ++i) { \
        write_granule(&(pool[(tid)+(i * thrs)])); \
    } \
})
#elif defined(DEBUG_TXS)
#define do_xact_macro(tid, pool, seed, prob_w, l, d) ({ \
    register int i; \
    register unsigned long long j; \
    for (i = 0; i < l; ++i) { \
        j = RAND_R_FNC(seed); \
        j = j % d; \
        j *= ACCESS_OFFSET; \
        if (IS_WRITE(prob_w, seed)) { \
            write_granule(&(pool[j])); \
			mem_pool[(D__ + tid) * ACCESS_OFFSET].counter++; \
        } \
        else { \
            read_granule(&(pool[j])); \
			mem_pool[(D__ + tid) * ACCESS_OFFSET].counter2++; \
        } \
    } \
})


		// j = RAND_R_FNC(seed) % (100 / l); \
		while (j-- > 0); \

#else
/* #define do_xact_macro(tid, thrs, pool, seed, prob_w, l, d) ({ \
    register int i; \
    register unsigned long long j; \
    int addrs[l]; \
    for (i = 0; i < l; ++i) { \
        j = addrs[i]; \
        iter_fill_addrs_f(addrs, seed, l, d, i); \
        if (IS_WRITE(prob_w, seed)) { \
            write_granule(&(pool[addrs[i]])); \
        } \
        else { \
            read_granule(&(pool[addrs[i]])); \
        } \
    } \
}) */
#define do_xact_macro(pool, seed, prob_w, l, d) ({ \
    register int i; \
    register unsigned long long j; \
    for (i = 0; i < l; ++i) { \
        j = RAND_R_FNC(seed); \
        j = j % d; \
        j *= ACCESS_OFFSET; \
        if (IS_WRITE(prob_w, seed)) { \
            write_granule(&(pool[j])); \
        } \
        else { \
            read_granule(&(pool[j])); \
        } \
    } \
})
#endif

void do_xact_f(int tid, int thrs, padded_scalar_t * pool, unsigned long long * seed,
               double prob_w, int l, int d)
{
    register int i;
    register unsigned long long j;
    int addrs[l];
#ifdef TEST_RO
    for (i = 0; i < l; ++i) {
        unsigned long long r_val = RAND_R_FNC(seed); // rand_r(seed)
        r_val = r_val % d;
        // *seed ^= addrs[i];
        read_granule(&(pool[r_val]));
        // for (j = 0; j < 250; ++j);
    }
#elif TEST_WO
    for (i = 0; i < l; ++i) {
        // unsigned long long r_val = RAND_R_FNC(seed); // rand_r(seed)
        // r_val = r_val % d;
        // *seed ^= addrs[i];
        for (j = 0; j < 10; ++j);
        write_granule(&(pool[(tid)+(i * thrs)]));
        // printf(" [%2i]%4lu ", tid, r_val);
    }
#else
    for (i = 0; i < l; ++i) {
        j = addrs[i]; // random value
        /*addrs[i] = (*seed + i) % d;*/
        iter_fill_addrs_f(addrs, seed, l, d, i);
        // *seed ^= j;
        if (IS_WRITE(prob_w, seed)) {
            write_granule(&(pool[addrs[i]]));
        }
        else {
            read_granule(&(pool[addrs[i]]));
        }
        // for (j = 0; j < 100 / l; ++j);
    }
#endif /* TEST */
}

void prepare_xact_f(padded_scalar_t * pool, int *addrs,
                    unsigned long long * seed, double prob_w, int l, int d)
{
    int i;
    for (i = 0; i < l; ++i) {
        int j = i;
        iter_fill_addrs_f(addrs, seed, l, d, i);
        write_granule(&(pool[addrs[i]]));
    }
}

void xact(int tid, unsigned long long * seed)
{
    int i, status = -1;
    int budget = BUDGET__;
    unsigned long long cp_seed = *seed;
    register padded_scalar_t * mem_pool_loc = mem_pool;
    register padded_scalar_t * sgl_ptr = global_lock_held;
    register double write_prob = WRITE_PROB__;
    register int l = L__;
    register int d = D__;
    int thrs = THREADS__;

    unsigned long long ts0, ts1, ts2, ts3, ts_commit;

    ts0 = rdtsc();

    while (budget > 0) {
        CHECK_GLOBAL_LOCK(sgl_ptr, false);

        ts1 = rdtsc();
        status = _xbegin();
        //lock = *global_lock_held_lock;

        if (status != _XBEGIN_STARTED) {
            // we aborted

            count_aborts[tid]++;
            //if (status & _XABORT_CONFLICT) {
            UPDATE_BUDGET(true, budget);
            //}
            TSX_ERROR_INC(status, errors_a[tid]);

            ts2 = rdtsc();

            time_aborts[tid] += ts2 - ts1;
            numb_aborts[tid] += 1;

            if (status == _XABORT_CAPACITY) {
                budget = 0;
            }
        }
        else {
            // we started
            CHECK_GLOBAL_LOCK(sgl_ptr, true); // reads the lock, aborts on someone locking
#ifdef DEBUG_TXS
			do_xact_macro(tid, mem_pool_loc, &cp_seed, write_prob, l, d);
#else
            do_xact_macro(mem_pool_loc, &cp_seed, write_prob, l, d);
#endif
            ts_commit = rdtsc();
            _xend();
            ts2 = rdtsc();

            time_commit[tid] += ts2 - ts_commit;
            numb_commit[tid] += 1;

            time_xact[tid] += ts2 - ts1;
            numb_runs[tid] += 1;

            // completed successfully
            count_commits[tid]++;
            break;
        }
    }

    if (!(budget > 0)) {

        ts1 = rdtsc();
        //lock = *global_lock_held_lock;
        ENTER_FALLBACK(sgl_ptr, global_lock);
#ifdef DEBUG_TXS
			do_xact_macro(tid, mem_pool_loc, &cp_seed, write_prob, l, d);
#else
            do_xact_macro(mem_pool_loc, &cp_seed, write_prob, l, d);
#endif
        EXIT_FALLBACK(sgl_ptr, global_lock);
        count_serls[tid]++;
        ts2 = rdtsc();

        time_serl[tid] += ts2 - ts1;
        numb_serl[tid] += 1;
    }

    ts3 = rdtsc();

    if (last_ts[tid] == 0) {
        last_ts[tid] = ts3;
    }
    else {
        time_notx[tid] += ts0 - last_ts[tid];
        numb_notx[tid] += 1;
        last_ts[tid] = ts3;
    }

    time_total[tid] += ts3 - ts0;
    numb_total[tid] += 1;
}

void * run_xacts(void * args)
{
    int i, tid = *((int*) args);
    //  struct timespec start, finish, elapsed;
    unsigned long long start, finish, elapsed;
    unsigned long long seed = seeds[tid];

    //    printf("[THR%i] Started\n", tid);

    __sync_add_and_fetch(&sync_enter, 1);
    while (!__sync_bool_compare_and_swap(&sync_enter, THREADS__, THREADS__));

    // ignore some initial data
    for (i = 0; i < 50; ++i) {
        xact(tid, &seed);
    }

    for (i = 0; i < ERRORS_T_SIZE; ++i) {
        errors_a[tid][i] = 0;
    }

    count_aborts[tid] = 0;
    count_commits[tid] = 0;
    count_serls[tid] = 0;

    time_xact[tid] = 0;
    numb_runs[tid] = 0;
    time_serl[tid] = 0;
    numb_serl[tid] = 0;
    time_total[tid] = 0;
    numb_total[tid] = 0;
    time_aborts[tid] = 0;
    numb_aborts[tid] = 0;
    time_notx[tid] = 0;
    numb_notx[tid] = 0;
    time_commit[tid] = 0;
    numb_commit[tid] = 0;

    //    printf("[THR%i] Start collecting data\n", tid);

    start = rdtsc();
    for (i = 0; i < EXPERIMENT_SAMPLES; ++i) {
        unsigned long long r_val;
        r_val = RAND_R_FNC(&seed) % 10; 
		while (r_val-- > 0); // not allow transactions to run serially
        xact(tid, &seed);
    }
    finish = rdtsc();

    //    printf("[THR%i] Ended collecting data\n", tid);

    __sync_add_and_fetch(&number_of_completed_threads, 1);

    elapsed = finish - start;

    count_time[tid] = (double) elapsed;

    for (i = 0; i < ERRORS_T_SIZE; ++i) {
        errors_a_snp[tid][i] = errors_a[tid][i];
    }
    count_aborts_snp[tid] = count_aborts[tid];
    count_commits_snp[tid] = count_commits[tid];
    count_serls_snp[tid] = count_serls[tid];
    count_time_snp[tid] = count_time[tid];

    time_xact_snp[tid] = time_xact[tid];
    numb_runs_snp[tid] = numb_runs[tid];
    time_serl_snp[tid] = time_serl[tid];
    numb_serl_snp[tid] = numb_serl[tid];
    time_total_snp[tid] = time_total[tid];
    numb_total_snp[tid] = numb_total[tid];
    time_aborts_snp[tid] = time_aborts[tid];
    numb_aborts_snp[tid] = numb_aborts[tid];
    time_commit_snp[tid] = time_commit[tid];
    numb_commit_snp[tid] = numb_commit[tid];
    time_notx_snp[tid] = time_notx[tid];
    numb_notx_snp[tid] = numb_notx[tid];

    // keep running to get the other results
    while (number_of_completed_threads < THREADS__) {
        xact(tid, &seed);
    }

    //    printf("[THR%i] Exit\n", tid);

    return NULL;
}

int main(int argc, char ** argv)
{
    int i = 1, j, k;
    while (i < argc) {

        if (strcmp(argv[i], "L") == 0) {
            L__ = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "D") == 0) {
            D__ = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "BUDGET") == 0) {
            BUDGET__ = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "THREADS") == 0) {
            THREADS__ = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "WRITE_PROB") == 0) {
            WRITE_PROB__ = atof(argv[i + 1]);
        }
        i += 2;
    }

    // TEST RAND
    //	for (k = 0; k < 100; ++k) {
    //		printf("%c", IS_WRITE(0.5, &i) ? 'W' : 'R');
    //	}
    //	printf("\n");

    pthread_t threads[THREADS__];
    pthread_attr_t attr[THREADS__];
    cpu_set_t cpuset[THREADS__];
    void * res;
    FILE * data_file;
    double time_el = 0, aborts = 0, commits = 0, serls = 0, tot, ends,
            prob_confl, prob_other,
            prob_commit, resp_time, throughput, aborts_avg, non_transac_avg,
            t_commit_avg, conflicts = 0, capacities = 0, other = 0,
            avg_r_trans, avg_r_serl, avg_r_xact;

    double time_xact_snpt = 0, numb_runs_snpt = 0, numb_serl_snpt = 0,
            time_total_snpt = 0, numb_total_snpt = 0, time_aborts_snpt = 0,
            numb_aborts_snpt = 0, time_notx_snpt = 0, numb_notx_snpt = 0,
            time_commit_snpt = 0, numb_commit_snpt = 0, time_serl_snpt = 0;

#ifdef DEBUG_TXS
	double tot_reads = 0, tot_writes = 0;
#endif /* DEBUG_TXS */
			
    int explicit = 0;

    srand(time(NULL));

    mem_pool = (padded_scalar_t*) ALLOC_FN(sizeof (padded_scalar_t) 
		* (D__ + 1
#ifdef DEBUG_TXS
		+ THREADS__
#endif /* DEBUG_TXS */
		) * ACCESS_OFFSET);
		
    global_lock_held = (padded_scalar_t*) &(mem_pool[0]);
    mem_pool = mem_pool + ACCESS_OFFSET; // the previous value is global lock
#ifdef DEBUG_TXS
	deb_pool = (padded_scalar_t*) &(mem_pool[D__ * ACCESS_OFFSET]);	
#endif /* DEBUG_TXS */
	
    errors_a = (errors_t**) ALLOC_FN(THREADS__ * sizeof (errors_t*));
    errors_a_snp = (errors_t**) ALLOC_FN(THREADS__ * sizeof (errors_t*));
    seeds = (unsigned int*) ALLOC_FN(THREADS__ * sizeof (unsigned int));
    for (i = 0; i < THREADS__; ++i) {
        errors_a[i] = (errors_t*) calloc(ERRORS_T_SIZE, sizeof (errors_t)); // aligned_alloc?
        errors_a_snp[i] = (errors_t*) calloc(ERRORS_T_SIZE, sizeof (errors_t));
        seeds[i] = SEED + i * 911;
#ifdef DEBUG_TXS
		mem_pool[(D__ + i) * ACCESS_OFFSET].counter = 0;
		mem_pool[(D__ + i) * ACCESS_OFFSET].counter2 = 0;
#endif /* DEBUG_TXS */
    }
    count_aborts = (int*) calloc(THREADS__, sizeof (int));
    count_commits = (int*) calloc(THREADS__, sizeof (int));
    count_serls = (int*) calloc(THREADS__, sizeof (int));
    count_time = (double*) calloc(THREADS__, sizeof (double));
    count_aborts_snp = (int*) malloc(THREADS__ * sizeof (int));
    count_commits_snp = (int*) malloc(THREADS__ * sizeof (int));
    count_serls_snp = (int*) malloc(THREADS__ * sizeof (int));
    count_time_snp = (double*) malloc(THREADS__ * sizeof (double));
    tids = (int*) malloc(THREADS__ * sizeof (int));
    per_tid = (int*) malloc(THREADS__ * sizeof (int));

#ifdef TEST_RAND_R_FNC
    unsigned long long fn_seed = rdtsc();
    unsigned long long aux_fn, count_writes = 0;
    int count_gen[D__], min_g = 9999999, max_g = -1;
    double stddev = 0, swp = 0, avg = 9999999.0 / (double) D__;
    for (i = 0; i < D__; ++i) {
        count_gen[i] = 0;
    }
    for (i = 0; i < 9999999; ++i) {
        /* if (i > 0) {
            fn_seed ^= seed_test_fn[(i - 1) % 9999];
        }*/
        if (IS_WRITE(0.5, (&fn_seed))) {
            count_writes++;
        }
        int rand_value = RAND_R_FNC(&fn_seed); // rand_r(&seed_test_fn);//
        count_gen[rand_value % D__]++;
    }
    for (i = 0; i < D__; ++i) {
        stddev += (count_gen[i] - avg) * (count_gen[i] - avg);
    }
    for (i = 0; i < D__; ++i) {
        for (j = i; j < D__; ++j) {
            if (count_gen[i] > count_gen[j]) {
                swp = count_gen[i];
                count_gen[i] = count_gen[j];
                count_gen[j] = swp;
            }
        }
    }
    stddev /= D__;
    printf("RANDOM values: max=%i, min=%i, stddev=%f, writes=%lu(?5000000)\n",
           count_gen[(int) (0.99 * D__)], count_gen[(int) (0.01 * D__)], sqrt(stddev), count_writes);
#endif /* TEST_RAND_R_FNC */

#ifdef TEST_ITER_FILL
    int fill_addrs[L__];
    unsigned long long if_seed1 = rdtsc(), if_seed2 = RAND_R_FNC(&if_seed1), if_seed3 = RAND_R_FNC(&if_seed1);
    printf("TEST_GEN_ADDRS= \n1[");
    for (i = 0; i < L__; ++i) {
        iter_fill_addrs_f(fill_addrs, &if_seed1, L__, D__, i);
        printf("%i,", fill_addrs[i]);
    }
    printf("]\n2[");
    for (i = 0; i < L__; ++i) {
        iter_fill_addrs_f(fill_addrs, &if_seed2, L__, D__, i);
        printf("%i,", fill_addrs[i]);
    }
    printf("]\n3[");
    for (i = 0; i < L__; ++i) {
        iter_fill_addrs_f(fill_addrs, &if_seed3, L__, D__, i);
        printf("%i,", fill_addrs[i]);
    }
    printf("]again\n\n1[");

    for (i = 0; i < L__; ++i) {
        iter_fill_addrs_f(fill_addrs, &if_seed1, L__, D__, i);
        printf("%i,", fill_addrs[i]);
    }
    printf("]\n2[");
    for (i = 0; i < L__; ++i) {
        iter_fill_addrs_f(fill_addrs, &if_seed2, L__, D__, i);
        printf("%i,", fill_addrs[i]);
    }
    printf("]\n3[");
    for (i = 0; i < L__; ++i) {
        iter_fill_addrs_f(fill_addrs, &if_seed3, L__, D__, i);
        printf("%i,", fill_addrs[i]);
    }
    printf("]");
#endif

    printf("====================== Running TSX test ====================== \n");
    printf("PROB_WRITE=%.2f, THRS=%d, BUDG=%d, D=%d, L=%d\n",
           WRITE_PROB__, THREADS__, BUDGET__, D__, L__);

    pthread_mutex_init(&global_lock, NULL);

    global_lock_held->counter = false;

    for (i = 0; i < D__*ACCESS_OFFSET; i += ACCESS_OFFSET) {
        mem_pool[i].counter = -1;
        mem_pool[i].counter2 = -1;
    }

    for (i = 0; i < THREADS__; ++i) {
        CPU_ZERO(cpuset + i);
        CPU_SET(i + THREAD_OFFSET, cpuset + i); // use the second CPU
        pthread_attr_init(&(attr[i]));
        pthread_attr_setaffinity_np(&(attr[i]),
                                    sizeof (cpu_set_t), &(cpuset[i]));
        tids[i] = i;

        pthread_create(threads + i, &(attr[i]),
                       run_xacts, (void *) (tids + i));
    }

    for (i = 0; i < THREADS__; ++i) {
        pthread_join(threads[i], &res);
    }

    for (i = 0; i < THREADS__; ++i) {
        time_el += count_time_snp[i];
        aborts += count_aborts_snp[i];
        commits += count_commits_snp[i];
        serls += count_serls_snp[i];
        conflicts += (double) errors_a_snp[i][CONFLICT];
        capacities += (double) errors_a_snp[i][CAPACITY];
        other += (double) errors_a_snp[i][OTHER];
        explicit += errors_a_snp[i][EXPLICIT];

        time_xact_snpt += time_xact_snp[i];
        numb_runs_snpt += numb_runs_snp[i];
        time_serl_snpt += time_serl_snp[i];
        numb_serl_snpt += numb_serl_snp[i];
        time_total_snpt += time_total_snp[i];
        numb_total_snpt += numb_total_snp[i];
        time_aborts_snpt += time_aborts_snp[i];
        numb_aborts_snpt += numb_aborts_snp[i];
        time_notx_snpt += time_notx_snp[i];
        numb_notx_snpt += numb_notx_snp[i];
        time_commit_snpt += time_commit_snp[i];
        numb_commit_snpt += numb_commit_snp[i];
		
#ifdef DEBUG_TXS
		tot_writes += mem_pool[(D__ + i) * ACCESS_OFFSET].counter;
		tot_reads += mem_pool[(D__ + i) * ACCESS_OFFSET].counter2;
#endif /* DEBUG_TXS */
    }

    //    time_el /= (double) THREADS__;
    //    aborts /= (double) THREADS__;
    //    commits /= (double) THREADS__;
    //    serls /= (double) THREADS__;

    tot = aborts + commits + serls;
    ends = commits + serls;
    prob_commit = ends / tot;
    prob_confl = conflicts / (conflicts + commits);
    prob_other = other / aborts;
    resp_time = time_el / ends;
    throughput = (double) THREADS__ / resp_time;
    //    throughput = ends / (time_el / (double) THREADS__);

    non_transac_avg = time_notx_snpt / numb_notx_snpt;
    t_commit_avg = time_commit_snpt / numb_commit_snpt;
    aborts_avg = time_aborts_snpt / numb_aborts_snpt;
    avg_r_trans = time_xact_snpt / numb_runs_snpt;
    avg_r_serl = time_serl_snpt / numb_serl_snpt;
    avg_r_xact = time_total_snpt / numb_total_snpt;

    data_file = fopen("transac_avg.txt", "a");
    fseek(data_file, 0, SEEK_END);
    if (ftell(data_file) < 8) {
        // not created yet
        fprintf(data_file, "%12s, %12s, %12s, %12s, %12s, %12s, %12s, "
                "%12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, "
                "%12s\n", "PROB_READ", "D", "L", "THREADS", "BUDGET",
                "ABORTS", "COMMITS", "SERLS", "CONFLICTS", "CAPACITY", "OTHER",
                "AVG_C_TRANS", "AVG_R_ABORTS", "AVG_R_SERL", "AVG_R_XACT",
                "NON_TRANSAC", "COMMIT_OP_LAT", "THROUGHPUT");
    }

    fprintf(data_file, "%12.2f, %12d, %12d, %12d, %12d, %12e, %12e, "
            "%12e, %12e, %12e, %12e, %12e, %12e, %12e, %12e, %12e, %12e, "
            "%12e\n", 1.0 - WRITE_PROB__, D__, L__, THREADS__, BUDGET__,
            aborts, commits, serls, conflicts,
            capacities, other, avg_r_trans, aborts_avg, avg_r_serl,
            avg_r_xact, non_transac_avg, t_commit_avg, throughput);

    fclose(data_file);

    printf("Avg tran-commit time: %lf\n", avg_r_trans);
    printf("Avg serl-commit time: %lf\n", avg_r_serl);
    printf("Avg xact-aborts time: %lf\n", aborts_avg);
    printf("Avg non-transac time: %lf\n", non_transac_avg);
    printf("Avg R complete xact : %lf\n", avg_r_xact);
    printf("Avg _xend() time    : %lf\n", t_commit_avg);
    printf("throughput          : %lf\n", throughput);
    printf("prob_conflict       : %lf\n", prob_confl);
    printf("prob_commit         : %lf\n", prob_commit);
    printf("prob_other          : %lf\n", prob_other);
    printf("EXPLICIT aborts     : %i\n", explicit);

#ifdef DEBUG_TXS
	printf("\nDEBUG\n");
	printf("reads               : %f.0 (%f%%)\n", tot_reads, tot_reads / (tot_reads + tot_writes) * 100.0);
	printf("writes              : %f.0 (%f%%)\n", tot_writes, tot_writes / (tot_reads + tot_writes) * 100.0);
	printf("DEBUG\n\n");
#endif /* DEBUG_TXS */
	
    printf("======================== TESTS  ENDED ========================\n\n");

    pthread_mutex_destroy(&global_lock);

    printf("Size padded_scalar_t %i\n", sizeof (padded_scalar_t));

    return 0;
}
