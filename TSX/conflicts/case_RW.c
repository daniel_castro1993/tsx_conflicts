#include "tm.h"

#ifndef MAX_THREADS
#define MAX_THREADS 2
#endif

#ifndef MULT_TIME
#define MULT_TIME 1000
#endif

#ifndef WRITE_THREAD
#define WRITE_THREAD 1
#endif

padded_scalar_t* shared_int;

int sync_enter, sync_exit;
int read_values[MAX_THREADS][EXPERIMENT_SAMPLES];

int errors[MAX_THREADS][ERRORS_T_SIZE];

void * T(void* me_ptr) {
	int i, j;
	padded_scalar_t cpy, *si = shared_int;
	int me = *((int*) me_ptr), index = me - 1;
	int is_write = index == WRITE_THREAD; // last thread writes

	printf("%c%i -> spin %i / write / spin %i\n", is_write == 1 ? 'W' : 'R',
			me, me * MULT_TIME / MAX_THREADS, 2 * MULT_TIME);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {

		__sync_add_and_fetch(&sync_enter, 1);
		while (!__sync_bool_compare_and_swap(&sync_enter,
				MAX_THREADS * (i + 1),
				MAX_THREADS * (i + 1)));

		// R spins 5000     |  W spins 10000
		// R reads          |
		// R spins 10000    |  W writes
		//                  |  W spins 10000
		// R commit         |
		//                  |  W commit

		START_TRANSAC(errors[index]);

		for (j = 0; j < me * MULT_TIME / MAX_THREADS; j++);

		if (is_write == 1) {
			si->counter = me;
		} else {
			cpy.counter = si->counter;
		}

		for (j = 0; j < 2 * MULT_TIME; j++);

		END_TRANSAC(errors[index]);

		__sync_add_and_fetch(&sync_exit, 1);
		while (!__sync_bool_compare_and_swap(&sync_exit,
				MAX_THREADS * (i + 1),
				MAX_THREADS * (i + 1)));
	}

	return NULL;
}

int main() {
	pthread_t thread_w[MAX_THREADS];
	cpu_set_t cpuset[MAX_THREADS];
	void *res1;
	int i, j;
	int W_ID[MAX_THREADS];
	int cpuid = 0;

	shared_int = (padded_scalar_t*) malloc(sizeof (padded_scalar_t));
	memset(shared_int, 0, sizeof (padded_scalar_t));

	shared_int->counter = 1;

	for (j = 0; j < MAX_THREADS; ++j) {
		W_ID[j] = j + 1;
		CPU_ZERO(&(cpuset[j]));
		CPU_SET(cpuid, &(cpuset[j]));
		cpuid += 8; // different physical cores I guess
		pthread_setaffinity_np((pthread_t) &(thread_w[j]),
				sizeof (cpu_set_t), &(cpuset[j]));

		pthread_create((pthread_t*) &(thread_w[j]),
				NULL, T, (void*) &(W_ID[j]));
	}
	for (j = 0; j < MAX_THREADS; ++j) {
		pthread_join(thread_w[j], &res1);
	}

	check_file();
	for (j = 0; j < MAX_THREADS; ++j) {
		fprintf(fp, "%c%i: ", (j % 2) == 1 ? 'W' : 'R', j + 1);
		for (i = 0; i < ERRORS_T_SIZE; ++i) {
			fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
					errors[j][i], (double) errors[j][i] /
					(double) EXPERIMENT_SAMPLES * 100.0);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);

	for (j = 0; j < MAX_THREADS; ++j) {
		fprintf(stdout, "%c%i: ", j == WRITE_THREAD ? 'W' : 'R', j + 1);
		for (i = 0; i < ERRORS_T_SIZE; ++i) {
			fprintf(stdout, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
					errors[j][i], (double) errors[j][i] /
					(double) EXPERIMENT_SAMPLES * 100.0);
		}
		fprintf(stdout, "\n");
	}

	/* Last thing that main() should do */
	// pthread_exit(NULL);

	return 0;
}
