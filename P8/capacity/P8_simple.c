#include "P8_tm.h"

/**
 * Simple test R/R.
 */

padded_scalar_t shared_int;
void *T1()
{
	int j;

	START_TRANSAC(errors_1);

	j = shared_int.counter;

	END_TRANSAC(errors_1);
}

void *T2()
{
	int j;

	START_TRANSAC(errors_2);

	j = shared_int.counter;

	END_TRANSAC(errors_2);
}

int main()
{
	pthread_t thread_w, thread_r;
	cpu_set_t cpuset_1, cpuset_2;
	void* res1;
	void* res2;
	int i, j;

	memset(&shared_int, 0, sizeof (padded_scalar_t));

	CPU_ZERO(&cpuset_1);
	CPU_SET(1, &cpuset_1);
	pthread_setaffinity_np((pthread_t) & thread_r, sizeof (cpu_set_t), &cpuset_1);

	CPU_ZERO(&cpuset_2);
	CPU_SET(2, &cpuset_2);
	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpu_set_t), &cpuset_2);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		pthread_create(&thread_w, NULL, T1, (void *) 1);

		for (j = 0; j < 1000; j++);

		pthread_create(&thread_r, NULL, T2, (void *) 2);
		pthread_join(thread_w, &res1);
		pthread_join(thread_r, &res2);
	}

	check_file();
	fprintf(fp, "R: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i] / (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n");
	fprintf(fp, "W: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_2[i], (double) errors_2[i] / (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n\n");
	fclose(fp);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
