#include "tm.h"

/**
 *       R1     |      R2
 *     starts   |
 *              |    starts
 *     reads    |          
 *              |    reads 
 *              |    commits
 *     commits  |
 */

#define MAX_THREADS 2
#define MULT_TIME 10000

#ifndef NB_GRANULES 
#define NB_GRANULES 1000
#endif

#ifndef NB_ACCESSES
#define NB_ACCESSES 1
#endif

padded_scalar_t* shared_int;

padded_scalar_t * global_lock_held;
pthread_mutex_t global_lock;

int sync_enter, sync_exit;
int read_values[MAX_THREADS][EXPERIMENT_SAMPLES];

int errors[MAX_THREADS][ERRORS_T_SIZE];

void __attribute__ ((noinline)) increment(padded_scalar_t *si) {
	si[0].counter++;
}

void * T(void* me_ptr) {
	int i, j, k;
	padded_scalar_t cpy, *si = shared_int;
	int me = *((int*) me_ptr), index = me - 1;
	int seed;

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {

		__sync_add_and_fetch(&sync_enter, 1);
		while (!__sync_bool_compare_and_swap(&sync_enter,
				MAX_THREADS * (i + 1),
				MAX_THREADS * (i + 1)));

		// ---
		TM_begin(errors[index]);
		// ---

		for (j = 0; j < NB_ACCESSES; j++) {
			increment(si);
		}

		// ---
		TM_commit(errors[index]);
		// ---

		__sync_add_and_fetch(&sync_exit, 1);
		while (!__sync_bool_compare_and_swap(&sync_exit,
				MAX_THREADS * (i + 1),
				MAX_THREADS * (i + 1)));

	}

	return NULL;
}

int main() {
	pthread_t thread_w[MAX_THREADS];
	cpu_set_t cpuset[MAX_THREADS];
	void *res1;
	int i, j;
	int W_ID[MAX_THREADS];
	int cpuid;

	shared_int = (padded_scalar_t*)
			malloc(sizeof (padded_scalar_t) * NB_GRANULES);
	memset(shared_int, 0, sizeof (padded_scalar_t) * NB_GRANULES);

	INIT_GSL();

	for (j = 0; j < MAX_THREADS; ++j) {
		W_ID[j] = j + 1;
		CPU_ZERO(&(cpuset[j]));
		CPU_SET(cpuid, &(cpuset[j]));
		cpuid += 8; // different physical cores I guess
		pthread_setaffinity_np((pthread_t) &(thread_w[j]),
				sizeof (cpu_set_t), &(cpuset[j]));

		pthread_create((pthread_t*) &(thread_w[j]),
				NULL, T, (void*) &(W_ID[j]));
	}
	for (j = 0; j < MAX_THREADS; ++j) {
		pthread_join(thread_w[j], &res1);
	}

	check_file();
	for (j = 0; j < MAX_THREADS; ++j) {
		fprintf(fp, "T%i: ", j + 1);
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(SUCCESS),
				errors[j][SUCCESS], (double) errors[j][SUCCESS] /
				(double) EXPERIMENT_SAMPLES * 100.0);
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(FALLBACK),
				errors[j][FALLBACK], (double) errors[j][FALLBACK] /
				(double) EXPERIMENT_SAMPLES * 100.0);
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(ABORT),
				errors[j][ABORT], (double) errors[j][ABORT] /
				(double) EXPERIMENT_SAMPLES * 100.0);
		fprintf(fp, "\n");
	}
	fclose(fp);

	printf("final value counter: %i\n", (int) shared_int->counter);

	DESTROY_GSL();

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
