#include "tm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LARGE_NUMBER  0x2FFFFF

int errors_1[ERRORS_T_SIZE];

#ifndef NWAY_ACCESSES
#define NWAY_ACCESSES 9
#endif

#ifndef SET_SIZE 
#define SET_SIZE 64
#endif

#ifndef WAY_SIZE 
#define WAY_SIZE 8
#endif

#define SET_WAY_OFFSET(_ADDR, _TIMES) (                                       \
    ((padded_scalar_t*) _ADDR) + (_TIMES * WAY_SIZE * SET_SIZE)               \
  )

#define WAY_OFFSET(_ADDR, _TIMES) (                                           \
    _TIMES < WAY_SIZE ? (((padded_scalar_t*) _ADDR) + (_TIMES * SET_SIZE))    \
               : (((padded_scalar_t*) _ADDR) + ((_TIMES - (WAY_SIZE - 1) * WAY_SIZE * SET_SIZE)))   \
  )

#ifdef __SAME_SET_WAY__
#define OFFSET(_ADDR, _TIMES) SET_WAY_OFFSET(_ADDR, _TIMES)
#else
#define OFFSET(_ADDR, _TIMES) WAY_OFFSET(_ADDR, _TIMES)
#endif

#define READ_ADDRS_0(_POOL, _REG)                                             \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 0))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 0))->counter2;

#define READ_ADDRS_1(_POOL, _REG)                                             \
  READ_ADDRS_0(_POOL, _REG)                                                   \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 1))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 1))->counter2;

#define READ_ADDRS_2(_POOL, _REG)                                             \
  READ_ADDRS_1(_POOL, _REG)                                                   \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 2))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 2))->counter2;

#define READ_ADDRS_3(_POOL, _REG)                                             \
  READ_ADDRS_2(_POOL, _REG)                                                   \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 3))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 3))->counter2;

#define READ_ADDRS_4(_POOL, _REG)                                             \
  READ_ADDRS_3(_POOL, _REG)                                                   \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 4))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 4))->counter2;

#define READ_ADDRS_5(_POOL, _REG)                                             \
  READ_ADDRS_4(_POOL, _REG);                                                  \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 5))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 5))->counter2;

#define READ_ADDRS_6(_POOL, _REG)                                             \
  READ_ADDRS_5(_POOL, _REG);                                                  \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 6))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 6))->counter2;

#define READ_ADDRS_7(_POOL, _REG)                                             \
  READ_ADDRS_6(_POOL, _REG);                                                  \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 7))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 7))->counter2;

#define READ_ADDRS_8(_POOL, _REG)                                             \
  READ_ADDRS_7(_POOL, _REG);                                                  \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 8))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 8))->counter2;

#define READ_ADDRS_9(_POOL, _REG)                                             \
  READ_ADDRS_8(_POOL, _REG);                                                  \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 9))->counter;                     \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 9))->counter2;

#define READ_ADDRS_10(_POOL, _REG)                                            \
  READ_ADDRS_9(_POOL, _REG);                                                  \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 10))->counter;                    \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 10))->counter2;

#define READ_ADDRS_11(_POOL, _REG)                                            \
  READ_ADDRS_10(_POOL, _REG);                                                 \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 11))->counter;                    \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 11))->counter2;

#define READ_ADDRS_12(_POOL, _REG)                                            \
  READ_ADDRS_11(_POOL, _REG);                                                 \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 12))->counter;                    \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 12))->counter2;

#define READ_ADDRS_13(_POOL, _REG)                                            \
  READ_ADDRS_12(_POOL, _REG);                                                 \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 13))->counter;                    \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 13))->counter2;

#define READ_ADDRS_14(_POOL, _REG)                                            \
  READ_ADDRS_13(_POOL, _REG);                                                 \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 14))->counter;                    \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 14))->counter2;

#define READ_ADDRS_15(_POOL, _REG)                                            \
  READ_ADDRS_14(_POOL, _REG);                                                 \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 15))->counter;                    \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 15))->counter2;

#define READ_ADDRS_16(_POOL, _REG)                                            \
  READ_ADDRS_15(_POOL, _REG);                                                 \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 16))->counter;                    \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 16))->counter2;

#define READ_ADDRS_17(_POOL, _REG)                                            \
  READ_ADDRS_16(_POOL, _REG);                                                 \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 17))->counter;                    \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 17))->counter2;

#define READ_ADDRS_18(_POOL, _REG)                                            \
  READ_ADDRS_17(_POOL, _REG);                                                 \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 18))->counter;                    \
  _REG += ((padded_scalar_t*) OFFSET(_POOL, 18))->counter2;

#if defined NWAY_ACCESSES && NWAY_ACCESSES == 0
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_0(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 1
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_1(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 2
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_2(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 3
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_3(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 4
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_4(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 5
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_5(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 6
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_6(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 7
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_7(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 8
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_8(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 9
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_9(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 10
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_10(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 11
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_11(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 12
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_12(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 13
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_13(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 14
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_14(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 15
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_15(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 16
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_16(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 17
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_17(_POOL, _REG) 
#elif defined NWAY_ACCESSES && NWAY_ACCESSES == 18
#define READ_ADDRS(_POOL, _REG) READ_ADDRS_18(_POOL, _REG) 
#endif

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t* mem_pool;
padded_scalar_t* addr_X;
padded_scalar_t* addr_X_[NWAY_ACCESSES];

int max = 0;
int sum_i = 0;

//void * W(void * arg) {
//  register padded_scalar_t* begin = (padded_scalar_t*) arg;
//  int status;
//  register int i = 0;
//
//  READ_ADDRS(begin, i); // Loads into cache
//  READ_ADDRS(begin, i);
//  READ_ADDRS(begin, i);
//
//  i = 0;
//
//  status = _xbegin();
//  if (status != _XBEGIN_STARTED) {
//    TSX_ERROR_INC(status, errors_1);
//    sum_i += i;
//    pthread_exit(NULL);
//  }
//
//  READ_ADDRS(begin, i);
//
//  _xend();
//
//  TSX_ERROR_INC(status, errors_1);
//  pthread_exit(NULL);
//}

void * W(void * arg) {
	register padded_scalar_t* begin = (padded_scalar_t*) arg;
	int status;
	register int i = 0;
	register int j = 0;
	int nb_accesses = 0, attempts = 0, nb_other_in_row = 0;

	i = 1;

	status = -1;
	while ((status != _XABORT_CAPACITY || attempts < 10)
			&& nb_other_in_row < 10) {

		nb_accesses++;
		j = nb_accesses * WAY_SIZE;
		status = _xbegin();
		if (status != _XBEGIN_STARTED) {
			if (status == _XABORT_CAPACITY) {
				attempts++;
				nb_accesses--;
			} else {
				nb_other_in_row++;
			}
			TSX_ERROR_INC(status, errors_1);
			continue;
		}

		while (j > 0) {
			j -= WAY_SIZE;
			i += begin[j].counter;
		}

		_xend();
		nb_other_in_row = 0;
		attempts = 0;
		TSX_ERROR_INC(status, errors_1);
	}

	if (nb_accesses > max) {
		max = nb_accesses;
	}

	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w;
	cpu_set_t cpuset;

	int i;
	void * res;

	padded_scalar_t * begin_pool, * end_pool, * it;

	printf("USING ASSOC=%i\n\n", NWAY_ACCESSES);

	mem_pool = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);
	begin_pool = (padded_scalar_t*) mem_pool;
	end_pool = begin_pool + LARGE_NUMBER;

	printf("START ADDR=%p, END ADDR=%p\n", begin_pool, end_pool);

	for (i = 0; i < NWAY_ACCESSES + 1; ++i) {
		padded_scalar_t* addr = OFFSET(begin_pool, i);
		printf(" %i) ADDR=%p\n", i, addr);
	}

	memset(begin_pool, 1, sizeof (padded_scalar_t) * LARGE_NUMBER);

	printf("\n\n");

	CPU_ZERO(&cpuset);
	CPU_SET(0, &cpuset);

	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpu_set_t), &cpuset);

	CPU_ZERO(&cpuset);
	CPU_SET(1, &cpuset);
	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpuset), &cpuset);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		pthread_create(&thread_w, NULL, W, (void *) begin_pool);
		pthread_join(thread_w, &res);
	}

	check_file();
	fprintf(fp, "W1: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i]
				/ (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n\n");
	fclose(fp);

	printf("max nb_accesses = %i\n", max);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
