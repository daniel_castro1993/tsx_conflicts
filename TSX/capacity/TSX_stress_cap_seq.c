#include "tm.h"

#include <unistd.h>
#include <assert.h>

// 4x L3
#ifndef LARGE_NUMBER
#define LARGE_NUMBER  65536
#endif

// TODO: for many threads use smaller OFFSET
#ifndef OFFSET
#define OFFSET  32768
#endif

#define MAX_THREADS 100

double PROB_READ = 1.0;
int NUM_ACCESSES = 100;
int NUM_THREADS = 1;

#ifdef EXPERIMENT_SAMPLES
#undef EXPERIMENT_SAMPLES
#define EXPERIMENT_SAMPLES 50000
#endif

unsigned rand_r_fnc_seed;

clock_t begin_ts;

padded_scalar_aux_t aux;

int sync_enter;

unsigned long count_ts_abort[MAX_THREADS],
count_ts_commit[MAX_THREADS],
sum_ts_abort[MAX_THREADS],
sum_ts_commit[MAX_THREADS],
count_ts_cap[MAX_THREADS],
sum_ts_cap[MAX_THREADS];

int errors_1[MAX_THREADS][ERRORS_T_SIZE];

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t* mem_pool;

int tids = 0;

void * W(void * arg) {
    int attempt, l;
    unsigned *seed = &rand_r_fnc_seed;
    int i, j, k;
    register int status;
    int cpy_k;
    int nb_accesses = NUM_ACCESSES;
    unsigned long long ts0, ts1;
    int tid = __sync_fetch_and_add(&tids, 1);

    __sync_add_and_fetch(&sync_enter, 1);
    while (!__sync_bool_compare_and_swap(&sync_enter,
        NUM_THREADS,
        NUM_THREADS));

    for (attempt = 0; attempt < EXPERIMENT_SAMPLES; attempt++) {

        // to much time
        //		if ((clock() - begin_ts) / CLOCKS_PER_SEC > 1000) {
        //			break;
        //		}

        register padded_scalar_t *begin = mem_pool;
		padded_scalar_t *mem = mem_pool;

		// free(mem_pool);
		mem = (padded_scalar_t*)
			malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);
		
		mem_pool = mem;
		
		begin = mem_pool;
		
        k = rand_r(seed) % OFFSET;
        j = k;
		cpy_k = k;
        for (i = 0; i < nb_accesses + 1; ++i) { // next access is seq
            begin[j].counter = ((j + 1) % OFFSET);
            begin[j].counter2 = ((double) rand_r(seed)
                / (double) RAND_MAX) > PROB_READ;
			j += 1;
        }
        
        j = nb_accesses;
		k = cpy_k;

		// printf("access: ");
        while (j > 0) {
			// printf(" %i ", k);
            j--;
            i = begin[k].counter;
            k = i;
        }
		// printf("\n");

        k = cpy_k;

        j = nb_accesses;
        //		k = rand_r(seed) % LARGE_NUMBER;

        aux.c[0] = k;
        aux.c[1] = j;
        aux.c[2] = i;

        register int counter = j;
        register int idx = k;
        register int aux;

        ts0 = rdtsc();
        status = _xbegin();
        if (status != _XBEGIN_STARTED) {
            ts1 = rdtsc();

            if ((double) attempt < (double) EXPERIMENT_SAMPLES * 0.9) {
				
                errors_1[tid][ABORT] += 1;
                TSX_ERROR_INC(status, errors_1[tid]);

                if (status == _XABORT_CAPACITY) {
                    // __sync_add_and_fetch(&(errors_1[CAPACITY]), 1);
                    count_ts_cap[tid] += 1;
                    sum_ts_cap[tid] += ts1 - ts0;
                } else {
                    count_ts_abort[tid] += 1;
                    sum_ts_abort[tid] += ts1 - ts0;
                }
				free(mem_pool);
            }
        } else {
            // READ ONLY
            //			while (counter > 0) {
            //				counter--;
            //				idx = begin[idx].counter;

            while (counter > 0) {
                counter--;
                aux = begin[idx].counter2; // always reads
                if (aux) {
                    begin[idx].counter2++; // WRITE
                }
                idx = begin[idx].counter;
            }
            _xend();
            ts1 = rdtsc();
            if ((double) attempt < (double) EXPERIMENT_SAMPLES * 0.9) {
                errors_1[tid][SUCCESS] += 1;
                count_ts_commit[tid] += 1;
                sum_ts_commit[tid] += ts1 - ts0;
            }
			free(mem_pool);
        }
    }

    return NULL;
}

int main(int argc, char **argv) {
    int i, j, k, l, nb_accesses;
    void * res;

    srand(clock());
    rand_r_fnc_seed = rand();

    begin_ts = clock();

    for (i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "PROB_READ") == 0) {
            PROB_READ = atof(argv[i + 1]);
            i++;
            continue;
        }
        if (strcmp(argv[i], "NUM_ACCESSES") == 0) {
            NUM_ACCESSES = atoi(argv[i + 1]);
            i++;
            continue;
        }
        if (strcmp(argv[i], "NUM_THREADS") == 0) {
            NUM_THREADS = atoi(argv[i + 1]);
            i++;
            continue;
        }
    }

    // printf("USING PROB_READ=%f, NUM_ACCESSES=%i\n", PROB_READ, NUM_ACCESSES);

    /*
    pthread_t thread[NUM_THREADS];
    pthread_attr_t attr[NUM_THREADS];
    cpu_set_t cpuset[NUM_THREADS];

    for (i = 0; i < NUM_THREADS; ++i) {
            CPU_ZERO(&(cpuset[i]));
            CPU_SET(i % 56, &(cpuset[i]));
            pthread_attr_init(&(attr[i]));
            pthread_attr_setaffinity_np(&(attr[i]), sizeof (cpu_set_t), &(cpuset[i]));
    } */

    /* mem_pool = (padded_scalar_t*)
        malloc(sizeof (padded_scalar_t) * LARGE_NUMBER); */

    // create
    /* for (i = 0; i < NUM_THREADS; ++i) {
            pthread_create(&(thread[i]), &(attr[i]), W, (void *) (mem_pool + i * OFFSET));
            //		pthread_setaffinity_np((pthread_t) & (thread[i]),
            //							sizeof (cpu_set_t), &(cpuset[i]));
    }

    // join
    for (i = 0; i < NUM_THREADS; ++i) {
            pthread_join(thread[i], &res);
    } */

    W(mem_pool);

    check_file();
    fseek(fp, 0, SEEK_END);
    if (ftell(fp) < 8) {
        fprintf(fp, "%12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s\n",
            "NUM_THREADS", "PROB_READ", "POOL_SIZE", "NUM_ACCESSES",
            "SUCCESS", "CAPACITY", "CONFLICT", "OTHER");
    }

    float success = (double) errors_1[0][SUCCESS] / (double) EXPERIMENT_SAMPLES / 0.9 * 100 / (double) NUM_THREADS;
    float abort = (double) errors_1[0][OTHER] / (double) EXPERIMENT_SAMPLES / 0.9 * 100 / (double) NUM_THREADS;
    float conflict = (double) errors_1[0][CONFLICT] / (double) EXPERIMENT_SAMPLES / 0.9 * 100 / (double) NUM_THREADS;
    float capacity = (double) errors_1[0][CAPACITY] / (double) EXPERIMENT_SAMPLES / 0.9 * 100 / (double) NUM_THREADS;

    fprintf(fp, "%12i, %12f, %12i, %12i, ", NUM_THREADS, PROB_READ,
        LARGE_NUMBER, NUM_ACCESSES);
    fprintf(fp, "%12f, %12f, %12f, %12f\n", success, capacity, conflict, abort);
    fclose(fp);

    //-- stdout
    fprintf(stdout, "%12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s\n",
        "NUM_THREADS", "PROB_READ", "POOL_SIZE", "NUM_ACCESSES",
        "SUCCESS", "CAPACITY", "CONFLICT", "OTHER");
    fprintf(stdout, "%12i, %12f, %12i, %12i, ", NUM_THREADS, PROB_READ,
        LARGE_NUMBER, NUM_ACCESSES);
    fprintf(stdout, "%12f, %12f, %12f, %12f\n", success, capacity,
        conflict, abort);
    fclose(stdout);

    /* Last thing that main() should do */
    // pthread_exit(NULL);

    return 0;
}
