#include "../tm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LARGE_NUMBER  0x2FFFFF

double PROB_READ = 1.0;
int NUM_ACCESSES = 100;

#ifdef EXPERIMENT_SAMPLES
#undef EXPERIMENT_SAMPLES
#define EXPERIMENT_SAMPLES 10
#endif

unsigned rand_r_fnc_seed;

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t* mem_pool;

void * W(void * arg) {
	register padded_scalar_t* begin = mem_pool;
	register int status;
	register int i;
	register int j;
	register int k = *((int*) arg);
	int nb_accesses = NUM_ACCESSES;

	j = nb_accesses;

	while (j > 0) {
		j--;
		i = begin[k].counter;
		k = i;
	}

	j = nb_accesses;
	k = *((int*) arg);

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		TSX_ERROR_INC(status, errors_1);
		errors_1[ABORT]++;
		pthread_exit(NULL);
	}

	while (j > 0) {
		j--;
		i = begin[k].counter; // always reads
		if (begin[k].counter2) {
			begin[k].counter = i; // WRITE
		}
		k = i;
	}

	_xend();
	TSX_ERROR_INC(status, errors_1);

	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	pthread_t thread_w;
	cpu_set_t cpuset;

	int i, j, k, l, nb_accesses;
	void * res;
	int reads = 0, writes = 0;

	padded_scalar_t * begin_pool, * end_pool, * it;

	srand(time(NULL));
	rand_r_fnc_seed = rand();

	for (i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "PROB_READ") == 0) {
			PROB_READ = atof(argv[i + 1]);
			i++;
			continue;
		}
		if (strcmp(argv[i], "NUM_ACCESSES") == 0) {
			NUM_ACCESSES = atoi(argv[i + 1]);
			i++;
			continue;
		}
	}

	printf("USING PROB_READ=%f, NUM_ACCESSES=%i\n", PROB_READ, NUM_ACCESSES);

	CPU_ZERO(&cpuset);
	CPU_SET(0, &cpuset);

	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpu_set_t), &cpuset);

	CPU_ZERO(&cpuset);
	CPU_SET(1, &cpuset);
	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpuset), &cpuset);

	mem_pool = (padded_scalar_t*)
			malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);
	begin_pool = (padded_scalar_t*) mem_pool;
	end_pool = begin_pool + LARGE_NUMBER;

	nb_accesses = NUM_ACCESSES;

	int accs[nb_accesses + 1];

	k = rand_r(&rand_r_fnc_seed) % LARGE_NUMBER;
	j = k;
	for (i = 0; i < nb_accesses + 1; ++i) {
		accs[i] = begin_pool[j].counter = rand_r(&rand_r_fnc_seed) % LARGE_NUMBER;
		begin_pool[j].counter2 = ((double) rand_r(&rand_r_fnc_seed)
				/ (double) RAND_MAX) > PROB_READ;
		if (begin_pool[j].counter2) {
			writes++;
		} else {
			reads++;
		}
		for (l = 0; l < i; ++l) {
			if (accs[i] == accs[l]) {
				begin_pool[j].counter = accs[i - 1];
				--i;
				break;
			}
		}
		j = begin_pool[j].counter;
	}
	printf("READS: %i, WRITES: %i\n\n", reads, writes);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		pthread_create(&thread_w, NULL, W, (void *) &k);
		pthread_join(thread_w, &res);
	}

	printf("\n\n");

	check_file();
	fseek(fp, 0, SEEK_END);
	if (ftell(fp) < 8) {
		fprintf(fp,
				"%12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s\n",
				"PROB_READ", "NUM_ACCESSES", "SUCCESS", "ABORT", "EXPLICIT", "RETRY",
				"CONFLICT", "CAPACITY", "DEBUG", "NESTED", "OTHER");
	}
	fprintf(fp, "%12f, %12i, ", PROB_READ, NUM_ACCESSES);
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%11f%%, ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i]
				/ (double) EXPERIMENT_SAMPLES * 100);
	}
	fprintf(fp, "\n");
	fclose(fp);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
