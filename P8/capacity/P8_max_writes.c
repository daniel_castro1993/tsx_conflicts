#include "P8_tm.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define LARGE_NUMBER  0x2FFFFF
#define OFFSET 128

int EXPECTED_MAXIMUM = 63;
int MAX_THREADS = 80;

#ifndef STRIDE 
#define STRIDE 1
#endif

padded_scalar_t* mem_pool;

int sync_enter;

unsigned long long time_success, time_abort;
unsigned long long count_success, count_abort;

int max = 0;
int sum_i = 0;

int nb_capacity = 0;
int nb_successes = 0;

void * W(void * arg)
{
	register padded_scalar_t* begin = (padded_scalar_t*) arg;
	TM_buff_type TM_buff;
	register int i = 0;
	register int j = 0;
	int nb_accesses = 0, nb_capa_in_row = 0, nb_other_in_row = 0;
	padded_scalar_t cpy;
	cpy.counter = 0;

	unsigned long long ts0, ts1;

	i = 1;

	nb_accesses = EXPECTED_MAXIMUM;
	j = nb_accesses;

	while (j > 0) {
		j--;
		begin[j].counter++;
	}

	j = nb_accesses;

	__sync_add_and_fetch(&sync_enter, 1);
	while (!__sync_bool_compare_and_swap(&sync_enter,
										MAX_THREADS, MAX_THREADS));

	ts0 = rdtsc();

	if (__TM_begin(TM_buff) != _HTM_TBEGIN_STARTED) {
		if (P8_ERROR_TO_INDEX(TM_buff) == CAPACITY) {
			__sync_add_and_fetch(&nb_capacity, 1);
		}
		ts1 = rdtsc();
		__sync_add_and_fetch(&time_abort, ts1 - ts0);
		__sync_add_and_fetch(&count_abort, 1);
		return NULL;
	}

	while (j > 0) {
		j -= STRIDE;
		begin[j].counter++;
		for (i = 0; i < 50; ++i);
	}

	__TM_end();
	ts1 = rdtsc();
	__sync_add_and_fetch(&nb_successes, 1);

	__sync_add_and_fetch(&time_success, ts1 - ts0);
	__sync_add_and_fetch(&count_success, 1);
	return NULL;
}

int main(int argc, char ** argv)
{
	MAX_THREADS = atoi(argv[1]);
	EXPECTED_MAXIMUM = atoi(argv[2]);

	pthread_t thread[MAX_THREADS];
	cpu_set_t cpuset[MAX_THREADS];

	int i;
	void * res;
	unsigned long long ts0, ts1;

	padded_scalar_t * begin_pool, * end_pool, * it;

	//		printf("USING MAX_THREADS=%i EXPECTED_MAXIMUM=%i\n", MAX_THREADS,
	//			EXPECTED_MAXIMUM);

	mem_pool = (padded_scalar_t*)
			malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);
	begin_pool = (padded_scalar_t*) mem_pool;
	end_pool = begin_pool + LARGE_NUMBER;

	//		printf("START ADDR=%p, END ADDR=%p\n\n", begin_pool, end_pool);

	for (i = 0; i < MAX_THREADS; i++) {
		CPU_ZERO(&(cpuset[i]));
		CPU_SET(i, &(cpuset[i]));
		pthread_setaffinity_np((pthread_t) & thread[i], sizeof (cpu_set_t),
							&(cpuset[i]));

		pthread_create(&(thread[i]), NULL, W,
					(void *) (begin_pool + (i * OFFSET)));
	}

	ts0 = rdtsc();

	for (i = 0; i < MAX_THREADS; i++) {
		pthread_join(thread[i], &res);
	}

	ts1 = rdtsc();

	//		printf("SUCCESS=%i, CAPACITY=%i\n", nb_successes, nb_capacity);
	//		printf("JOIN_TIME=%fms\n", (double) (ts1 - ts0) / 3.425e9 * 1000.0);

	FILE *data_file = fopen("cap_multiple_thrs.txt", "a");
	fseek(data_file, 0, SEEK_END);
	if (ftell(data_file) < 8) {
		// not created yet
		fprintf(data_file, "%s,%s,%s,%s,%s,%s,%s\n", "THREADS", "GRANULES",
				"SUCCESS", "CAPACITY", "AVG_SUCC_TIME (ms)",
				"AVG_ABORT_TIME (ms)", "JOIN_TIME (ms)");
	}

	fprintf(data_file, "%d,%d,%d,%d,%f,%f,%f\n", MAX_THREADS,
			EXPECTED_MAXIMUM, nb_successes, nb_capacity,
			(double) time_success / (double) count_success / 3.425e9 * 1000.0,
			(double) time_abort / (double) count_abort / 3.425e9 * 1000.0,
			(double) (ts1 - ts0) / 3.425e9 * 1000.0);

	fclose(data_file);

	return 0;
}
