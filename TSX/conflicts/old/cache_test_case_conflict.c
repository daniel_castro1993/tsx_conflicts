#include "tm.h"

#define LARGE_NUMBER  0xFFFF

/**
 *       W1     |      W2
 *     starts   |
 *              |    starts
 *     write X  |    write X'
 *              |    commits
 *     commits  |
 * X and X' have the same Set and Way bits,
 * but different Tag bits
 */

padded_scalar_t mem_pool[LARGE_NUMBER];
padded_scalar_t * addr_X;
padded_scalar_t * addr_X_[8];

int conflict_int;

void * W1(void * arg) {
	int j, status;

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		TSX_ERROR_INC(status, errors_1);
		pthread_exit(NULL);
	}

	for (j = 0; j < 4000; j++);

	addr_X->counter = 7;

	for (j = 0; j < 4000; j++);

	_xend();

	TSX_ERROR_INC(status, errors_1);
	pthread_exit(NULL);
}

void * W2(void * arg) {
	int j, status;

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		TSX_ERROR_INC(status, errors_2);
		pthread_exit(NULL);
	}

	for (j = 0; j < 2000; j++);

	addr_X->counter = 5;

	for (j = 0; j < 6000; j++);

	_xend();

	TSX_ERROR_INC(status, errors_2);
	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w1, thread_w2;
	cpu_set_t cpuset_1, cpuset_2;

	int rc, i;
	void * res;

	padded_scalar_t * begin_pool = (padded_scalar_t*) mem_pool,
			* end_pool = begin_pool + LARGE_NUMBER,
			* it;
	unsigned long mask = SET_MASK | WAY_MASK, addr_X_masked;

	addr_X = begin_pool;
	addr_X->counter = 0xff;

	addr_X_masked = mask & (unsigned long) addr_X;

	i = 0;
	for (it = end_pool - 1; it >= begin_pool; --it) {
		if (((unsigned long) it & mask) == addr_X_masked) {
			addr_X_[i] = it;
			addr_X_[i]->counter = 0xff;
			i++;
			if (i == 8) {
				break;
			}
		}
	}

	conflict_int = -1;

	check_file();
	fprintf(fp, "addr_X=[%p] addr_X2=[%p] ", addr_X, addr_X_[0]);

	CPU_ZERO(&cpuset_1);
	CPU_ZERO(&cpuset_2);

	CPU_SET(2, &cpuset_1);
	CPU_SET(1, &cpuset_2);

	pthread_setaffinity_np((pthread_t) & thread_w1, sizeof (cpuset_1), &cpuset_1);
	pthread_setaffinity_np((pthread_t) & thread_w2, sizeof (cpuset_2), &cpuset_2);

	memset(mem_pool, 0, sizeof (mem_pool));

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		pthread_create(&thread_w1, NULL, W1, (void *) 1);
		pthread_create(&thread_w2, NULL, W2, (void *) 2);

		pthread_join(thread_w1, &res);
		pthread_join(thread_w2, &res);
	}

	check_file();
	fprintf(fp, "\nW1: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i] / (double) EXPERIMENT_SAMPLES * 100);
	}
	fprintf(fp, "\nW2: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_2[i], (double) errors_2[i] / (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n\n");
	fclose(fp);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
