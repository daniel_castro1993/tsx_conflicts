#!/bin/bash

rm *.txt

for p in 0.9 0.5 0.1
do
	cd ..
	make clean && make USE_POSIX=1 SAMPLES=100000 PROFILE=1 FILE=./bench_L_pw$p.txt
	cd results
	for i in {1..30}
	do
	  ../stress_conflicts.out THREADS 4 BUDGET 6 D 4096 L $i PW $p
      wait ; wait ;
	  sleep 0.1 ;
	done
done

for i in 2 6 10
do
	cd ..
	make clean && make USE_POSIX=1 SAMPLES=100000 PROFILE=1 FILE=./bench_pw_L$i.txt
	cd results
	for p in 1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 0.0
	do
	  ../stress_conflicts.out THREADS 4 BUDGET 6 D 512 L $i PW $p
      wait ; wait ;
	  sleep 0.1 ;
	done
done

for p in 0.9 0.5 0.1
do
	cd ..
	make clean && make USE_POSIX=1 SAMPLES=100000 PROFILE=1 FILE=./bench_B_pw$p.txt
	cd results
	for i in {1..8}
	do
	  ../stress_conflicts.out THREADS 4 BUDGET $i D 64 L 6 PW $p
      wait ; wait ;
	  sleep 0.1 ;
	done
done

for p in 0.9 0.5 0.1
do
	cd ..
	make clean && make USE_POSIX=1 SAMPLES=100000 PROFILE=1 FILE=./bench_D_pw$p.txt
	cd results
	for ((i=20;i<=1000;i += 20)); 
	do
	  ../stress_conflicts.out THREADS 4 BUDGET 6 D $i L 6 PW $p
      wait ; wait ;
	  sleep 0.1 ;
	done
done

for p in 0.9 0.5 0.1
do
	cd ..
	make clean && make SAMPLES=100000 PROFILE=1 FILE=./bench_THRS_pw$p.txt
	cd results
	for i in {1..14}
	do
	  ../stress_conflicts.out THREADS $i BUDGET 4 D 128 L 6 PW $p
      wait ; wait ;
	  sleep 0.1 ;
	done
done
