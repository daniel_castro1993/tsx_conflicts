#!/bin/bash
gcc -g -O0 -mhtm ./conflicts/P8_conf_test.c -o P8_conf_test.out -lpthread -lrt -I.
for i in {0..200}
do
  for p in 0.1 0.05
  do
    for d in 512
    do
      for l in 2 4 6 8 10 12 14 16 18 20
      do
        for t in 4
        do
          for b in 5
          do
            timeout 1m nice -n 19 ./P8_conf_test.out THREADS $t BUDGET $b D $d PW $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;

  for p in 0.1 0.05
  do
    for d in 512
    do
      for l in 12
      do
        for t in 2 3 4 5 6 7 8 9 10
        do
          for b in 5
          do
            timeout 1m nice -n 19 ./P8_conf_test.out THREADS $t BUDGET $b D $d PW $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;

  for p in 0.1 0.05
  do
    for d in 512
    do
      for l in 12
      do
        for t in 4
        do
          for b in 1 2 3 4 5 6 8
          do
            timeout 1m nice -n 19 ./P8_conf_test.out THREADS $t BUDGET $b D $d PW $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;

  # for p in 1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1
  # do
    # for d in 512
    # do
      # for l in 12
      # do
        # for t in 4
        # do
          # for b in 5
          # do
            # timeout 1m nice -n 19 ./P8_conf_test.out THREADS $t BUDGET $b D $d PW $p L $l ;
          # done;
        # done;
      # done;
    # done;
  # done;
  # wait ;
done

