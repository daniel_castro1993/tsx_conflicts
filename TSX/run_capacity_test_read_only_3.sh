#!/bin/bash
gcc -O0 -mrtm ./capacity/TSX_stress_cap.c -o assoc.out -lpthread -lrt -I.  \
  -DPLOT_FILE="\"stress_cap.txt\"" -march=native -DEXPERIMENT_SAMPLES=5000;
for i in {0..5}
do
  for m in 1000 8000 17500 50000 55000
  do
    for n in 1.00 
    do
      ~/sde-external-7.49.0-2016-07-07-lin/sde64 -hsw -rtm-mode full -- ./assoc.out PROB_READ $n NUM_ACCESSES $m &
    done
  done
done
