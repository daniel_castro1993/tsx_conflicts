#include "tm.h"

#include <cstring>

#define LARGE_NUMBER  0x2FFFFF

using namespace std;

double PROB_READ = 1.0;
int NUM_ACCESSES = 100;

unsigned rand_r_fnc_seed;

padded_scalar_aux_t aux;

unsigned long count_ts_abort, count_ts_commit, sum_ts_abort,
sum_ts_commit, count_ts_cap, sum_ts_cap;

int errors_1[ERRORS_T_SIZE];

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t* mem_pool;

void W()
{
	int attempt, l;
	unsigned *seed = &rand_r_fnc_seed;
	int i, j, k;
	register int status;
	int cpy_k;
	int nb_accesses = NUM_ACCESSES;
	int accs[nb_accesses + 1];
	unsigned long long ts0, ts1;

	padded_scalar_t * begin_pool, * end_pool, * it;

	begin_pool = (padded_scalar_t*) mem_pool;
	end_pool = begin_pool + LARGE_NUMBER;

	for (attempt = 0; attempt < EXPERIMENT_SAMPLES; attempt++) {
		register padded_scalar_t *begin = mem_pool;

		k = rand() % LARGE_NUMBER;
		/*
		j = k;
		for (i = 0; i < nb_accesses + 1; ++i) {
			accs[i] = begin_pool[j].counter = rand() % LARGE_NUMBER;
			begin_pool[j].counter2 = ((double) rand()
					/ (double) RAND_MAX) > PROB_READ;
			for (l = 0; l < i; ++l) {
				if (accs[i] == accs[l]) {
					begin_pool[j].counter = accs[i - 1];
					--i;
					break;
				}
			}
			j = begin_pool[j].counter;
		}


		cpy_k = k;
		j = nb_accesses;

		while (j > 0) {
			j--;
			i = begin[k].counter;
			k = i;
		}

		k = cpy_k;*/
		j = nb_accesses;

		aux.c[0] = k;
		aux.c[1] = j;
		aux.c[2] = i;

		ts0 = rdtsc();
		status = _xbegin();
		if (status != _XBEGIN_STARTED) {
			ts1 = rdtsc();

			if (status == _XABORT_CAPACITY) {
				count_ts_cap++;
				sum_ts_cap += ts1 - ts0;
			}
			else {
				count_ts_abort++;
				sum_ts_abort += ts1 - ts0;
			}

			errors_1[ABORT]++;
			TSX_ERROR_INC(status, errors_1);
		}
		else {
			while (aux.c[1] > 0) {
				aux.c[1]--;
				aux.c[2] = begin[aux.c[0]].counter; // always reads
				if (((double) rand() / (double) RAND_MAX) > PROB_READ) {
					// if (begin[aux.c[0]].counter2) {
					begin[aux.c[0]].counter = aux.c[2]; // WRITE
				}
				// aux.c[0] = aux.c[2];
				aux.c[0] = rand() % LARGE_NUMBER;
			}
			_xend();
			ts1 = rdtsc();
			errors_1[SUCCESS]++;
			count_ts_commit++;
			sum_ts_commit += ts1 - ts0;
		}
		ts1 = rdtsc();
	}
}

int main(int argc, char **argv)
{
	int i, j, k, l, nb_accesses;
	void * res;

	srand(clock());
	rand_r_fnc_seed = rand();

	for (i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "PROB_READ") == 0) {
			PROB_READ = atof(argv[i + 1]);
			i++;
			continue;
		}
		if (strcmp(argv[i], "NUM_ACCESSES") == 0) {
			NUM_ACCESSES = atoi(argv[i + 1]);
			i++;
			continue;
		}
	}

	// printf("USING PROB_READ=%f, NUM_ACCESSES=%i\n", PROB_READ, NUM_ACCESSES);

	mem_pool = (padded_scalar_t*)
			malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);

	thread thr(W);
	thr.join();

	check_file();
	fseek(fp, 0, SEEK_END);
	if (ftell(fp) < 8) {
		fprintf(fp,
				"%12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s\n",
				"PROB_READ", "NUM_ACCESSES", "SUCCESS", "ABORT", "CAPACITY",
				"AVG_T_COMMIT", "AVG_T_ABORT", "AVG_T_CAP");
	}

	float success = (double) errors_1[SUCCESS] / (double) EXPERIMENT_SAMPLES * 100;
	float abort = (double) errors_1[ABORT] / (double) EXPERIMENT_SAMPLES * 100;
	float capacity = (double) errors_1[CAPACITY] / (double) EXPERIMENT_SAMPLES * 100;

	fprintf(fp, "%12f, %12i, ", PROB_READ, NUM_ACCESSES);
	fprintf(fp, "%12f, %12f, %12f, ", success, abort, capacity);
	fprintf(fp, "%12f, %12f, ", (double) sum_ts_commit / (double) count_ts_commit,
			(double) sum_ts_abort / (double) count_ts_abort);
	fprintf(fp, "%12f\n", (double) sum_ts_cap / (double) count_ts_cap);
	fclose(fp);

	return 0;
}
