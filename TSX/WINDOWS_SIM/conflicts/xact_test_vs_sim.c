#include "tm.h"

#define MAX_NUMB_THREADS 16

padded_scalar_t * mem_pool;

errors_t ** errors_a;
errors_t ** errors_a_snp;
int * count_aborts, * count_commits;
int * count_aborts_snp, * count_commits_snp;
int * count_serls;
int * count_serls_snp;
double * count_time;
double * count_time_snp;

int * tids;
int * per_tid;
unsigned int * seeds;

int number_of_completed_threads = 0;

int THREADS__ = 4;
int BUDGET__ = 5;
int D__ = 5000;
int L__ = 10;
double WRITE_PROB__ = 1.0;

double avgC = 0.0;

int sync_enter = 0;

unsigned long long time_fill;
unsigned long long numb_fill;

unsigned long long time_xact;
unsigned long long numb_runs;
unsigned long long time_serl;
unsigned long long numb_serl;
unsigned long long time_total;
unsigned long long numb_total;
unsigned long long time_aborts;
unsigned long long numb_aborts;
unsigned long long time_notx;
unsigned long long numb_notx;
unsigned long long time_commit;
unsigned long long numb_commit;

unsigned long long time_fill_snp;
unsigned long long numb_fill_snp;

unsigned long long time_xact_snp;
unsigned long long numb_runs_snp;
unsigned long long numb_serl_snp;
unsigned long long time_total_snp;
unsigned long long numb_total_snp;
unsigned long long time_aborts_snp;
unsigned long long numb_aborts_snp;
unsigned long long time_notx_snp;
unsigned long long numb_notx_snp;
unsigned long long time_commit_snp;
unsigned long long numb_commit_snp;

unsigned long long last_ts[MAX_NUMB_THREADS];

//(int * addrs, int * seed, int l, int d, int i)
#define iter_fill_addrs_f(addrs, seed, l, d, i) ({ \
	int j;\
	bool is_new = true;\
	do {\
		if (!(l < d)) {\
			addrs[i] = i;\
		}\
		else {\
			addrs[i] = rand_r(seed) /* RAND_R_FNC(*seed, j)*/ % d;\
			for (j = 0; j < i; ++j) {\
				if (addrs[j] == addrs[i]) {\
					is_new = false;\
					break;\
				}\
			}\
			if (!is_new) {\
				is_new = true;\
				continue;\
			}\
		}\
		break;\
	}\
	while (true);\
})

// (padded_scalar_t * pool, int * seed, double prob_w, int l, int d)

/* (padded_scalar_t * pool,
					  int * seed,
					  double prob_w,
					  int l,
					  int d)*/
void do_xact_f(padded_scalar_t * pool,
		int * seed,
		double prob_w,
		int l,
		int d) {
	int i, addrs[l];
	for (i = 0; i < l; ++i) {
		int j = i;
		iter_fill_addrs_f(addrs, seed, l, d, i);
		/*addrs[i] = (*seed + i) % d;*/
		if (IS_WRITE(prob_w, seed)) {
			write_granule(j, pool[addrs[i]]);
		} else {
			read_granule(j, pool[addrs[i]]);
		}
	}
}

void xact(int tid, unsigned int * seed) {
	int i, status, addrs[L__];
	int budget = BUDGET__;
	int local_variable;
	int cp_seed = *seed;
	padded_scalar_t * mem_pool_loc = mem_pool;
	padded_scalar_t * global_lock_held_loc = global_lock_held;
	double write_prob = WRITE_PROB__;
	int l = L__;
	int d = D__;
	padded_scalar_t lock;

	unsigned long long ts0, ts1, ts2, ts3, ts_commit;
	unsigned long long discount_init = 0;

	ts0 = rdtsc();

	while (budget > 0) {

		//lock = *global_lock_held_lock;
		CHECK_GLOBAL_LOCK(global_lock_held, false);

		ts1 = rdtsc();
		status = _xbegin();
		//lock = *global_lock_held_lock;

		if (status != _XBEGIN_STARTED) {
			// we aborted
			count_aborts[tid]++;
			UPDATE_BUDGET(true, budget);
			TSX_ERROR_INC(status, errors_a[tid]);

			ts2 = rdtsc();

			__sync_add_and_fetch(&time_aborts, ts2 - ts1);
			__sync_add_and_fetch(&numb_aborts, 1);

			if (status == _XABORT_CAPACITY) {
				budget = 0;
			}
		} else {
			// we started
			CHECK_GLOBAL_LOCK(global_lock_held_loc, true); // reads the lock, aborts on someone locking
			do_xact_f(mem_pool_loc, &cp_seed, write_prob, l, d);
			ts_commit = rdtsc();
			_xend();
			ts2 = rdtsc();

			__sync_add_and_fetch(&time_commit, ts2 - ts_commit);
			__sync_add_and_fetch(&numb_commit, 1);

			__sync_add_and_fetch(&time_xact, ts2 - ts1);
			__sync_add_and_fetch(&numb_runs, 1);

			// completed successfully
			count_commits[tid]++;
			break;
		}
	}

	if (!(budget > 0)) {

		ts1 = rdtsc();
		//lock = *global_lock_held_lock;
		ENTER_FALLBACK(global_lock_held, global_lock);
		do_xact_f(mem_pool_loc, &cp_seed, write_prob, l, d);
		EXIT_FALLBACK(global_lock_held, global_lock);
		count_serls[tid]++;
		ts2 = rdtsc();

		__sync_add_and_fetch(&time_serl, ts2 - ts1);
		__sync_add_and_fetch(&numb_serl, 1);
	}

	ts3 = rdtsc();

	if (last_ts[tid] == 0) {
		last_ts[tid] = ts3;
	} else {
		__sync_add_and_fetch(&time_notx, ts0 - last_ts[tid]);
		__sync_add_and_fetch(&numb_notx, 1);
		last_ts[tid] = ts3;
	}

	__sync_add_and_fetch(&time_total, ts3 - ts0);
	__sync_add_and_fetch(&numb_total, 1);
}

void * run_xacts(void * args) {
	int i, tid = *((int*) args);
	//  struct timespec start, finish, elapsed;
	unsigned long long start, finish, elapsed;
	unsigned int seed = seeds[tid];

	printf("[THR%i] Started\n", tid);

	__sync_add_and_fetch(&sync_enter, 1);
	while (!__sync_bool_compare_and_swap(&sync_enter, THREADS__, THREADS__));

	// ignore some initial data
	for (i = 0; i < 50; ++i) {
		xact(tid, &seed);
	}

	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		errors_a[tid][i] = 0;
	}

	count_aborts[tid] = 0;
	count_commits[tid] = 0;
	count_serls[tid] = 0;

	time_fill = 0;
	numb_fill = 0;

	time_xact = 0;
	numb_runs = 0;
	time_serl = 0;
	numb_serl = 0;
	time_total = 0;
	numb_total = 0;
	time_aborts = 0;
	numb_aborts = 0;
	time_notx = 0;
	numb_notx = 0;
	time_commit = 0;
	numb_commit = 0;

	printf("[THR%i] Start collecting data\n", tid);

	start = rdtsc();
	for (i = 0; i < EXPERIMENT_SAMPLES; ++i) {
		xact(tid, &seed);
	}
	finish = rdtsc();

	printf("[THR%i] Ended collecting data\n", tid);

	__sync_add_and_fetch(&number_of_completed_threads, 1);

	elapsed = finish - start;

	count_time[tid] = (double) elapsed;

	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		errors_a_snp[tid][i] = errors_a[tid][i];
	}
	count_aborts_snp[tid] = count_aborts[tid];
	count_commits_snp[tid] = count_commits[tid];
	count_serls_snp[tid] = count_serls[tid];
	count_time_snp[tid] = count_time[tid];

	time_fill_snp = time_fill;
	numb_fill_snp = numb_fill;

	time_xact_snp = time_xact;
	numb_runs_snp = numb_runs;
	numb_serl_snp = numb_serl;
	time_total_snp = time_total;
	numb_total_snp = numb_total;
	time_aborts_snp = time_aborts;
	numb_aborts_snp = numb_aborts;
	time_commit_snp = time_commit;
	numb_commit_snp = numb_commit;
	time_notx_snp = time_notx;
	numb_notx_snp = numb_notx;

	// keep running to get the other results
	while (number_of_completed_threads < THREADS__) {
		xact(tid, &seed);
	}

	printf("[THR%i] Exit\n", tid);

	pthread_exit(NULL);
}

int main(int argc, char ** argv) {
	int i = 1, j, k;
	while (i < argc) {

		if (strcmp(argv[i], "L") == 0) {
			L__ = atoi(argv[i + 1]);
		} else if (strcmp(argv[i], "D") == 0) {
			D__ = atoi(argv[i + 1]);
		} else if (strcmp(argv[i], "BUDGET") == 0) {
			BUDGET__ = atoi(argv[i + 1]);
		} else if (strcmp(argv[i], "THREADS") == 0) {
			THREADS__ = atoi(argv[i + 1]);
		} else if (strcmp(argv[i], "WRITE_PROB") == 0) {
			WRITE_PROB__ = atof(argv[i + 1]);
		}
		i += 2;
	}

	// TEST RAND
	//	for (k = 0; k < 100; ++k) {
	//		printf("%c", IS_WRITE(0.5, &i) ? 'W' : 'R');
	//	}
	//	printf("\n");

	pthread_t threads[THREADS__];
	cpu_set_t cpuset[THREADS__];
	void * res;
	FILE * data_file;
	double time_el = 0, aborts = 0, commits = 0, serls = 0, tot, ends,
			prob_commit, resp_time, throughput, aborts_avg, non_transac_avg,
			t_commit_avg, conflicts = 0, capacities = 0, other = 0,
			avg_r_trans, avg_r_serl, avg_r_xact, r_init;
	int explicit = 0;

	srand(time(NULL));

	mem_pool = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * D__);
	global_lock_held = (padded_scalar_t*) malloc(sizeof (padded_scalar_t));

	errors_a = (errors_t**) malloc(THREADS__ * sizeof (errors_t*));
	errors_a_snp = (errors_t**) malloc(THREADS__ * sizeof (errors_t*));
	seeds = (unsigned int*) malloc(THREADS__ * sizeof (unsigned int));
	for (i = 0; i < THREADS__; ++i) {
		errors_a[i] = (errors_t*) calloc(ERRORS_T_SIZE, sizeof (errors_t));
		errors_a_snp[i] = (errors_t*) calloc(ERRORS_T_SIZE, sizeof (errors_t));
		seeds[i] = rand();
	}
	count_aborts = (int*) calloc(THREADS__, sizeof (int));
	count_commits = (int*) calloc(THREADS__, sizeof (int));
	count_serls = (int*) calloc(THREADS__, sizeof (int));
	count_time = (double*) calloc(THREADS__, sizeof (double));
	count_aborts_snp = (int*) malloc(THREADS__ * sizeof (int));
	count_commits_snp = (int*) malloc(THREADS__ * sizeof (int));
	count_serls_snp = (int*) malloc(THREADS__ * sizeof (int));
	count_time_snp = (double*) malloc(THREADS__ * sizeof (double));
	tids = (int*) malloc(THREADS__ * sizeof (int));
	per_tid = (int*) malloc(THREADS__ * sizeof (int));

	printf("====================== Running TSX test ====================== \n");
	printf("PROB_WRITE=%.2f, THRS=%d, BUDG=%d, D=%d, L=%d\n",
			WRITE_PROB__, THREADS__, BUDGET__, D__, L__);

	pthread_mutex_init(&global_lock, NULL);

	global_lock_held->counter = false;
	for (i = 0; i < D__; ++i) {
		mem_pool[i].counter = -1;
		mem_pool[i].counter2 = -1;
	}

	for (i = 0; i < THREADS__; ++i) {
		CPU_ZERO(cpuset + i);
		CPU_SET(i, cpuset + i);
		pthread_setaffinity_np((pthread_t) threads + i,
				sizeof (cpu_set_t), cpuset + i);
		tids[i] = i;

		pthread_create(threads + i, NULL, run_xacts, (void *) (tids + i));
	}

	for (i = 0; i < THREADS__; ++i) {
		pthread_join(threads[i], &res);
	}

	for (i = 0; i < THREADS__; ++i) {
		time_el += count_time_snp[i];
		aborts += count_aborts_snp[i];
		commits += count_commits_snp[i];
		serls += count_serls_snp[i];
		conflicts += (double) errors_a_snp[i][CONFLICT];
		capacities += (double) errors_a_snp[i][CAPACITY];
		other += (double) errors_a_snp[i][OTHER];
		explicit += errors_a_snp[i][EXPLICIT];
	}

	//    time_el /= (double) THREADS__;
	//    aborts /= (double) THREADS__;
	//    commits /= (double) THREADS__;
	//    serls /= (double) THREADS__;

	tot = aborts + commits + serls;
	ends = commits + serls;
	prob_commit = ends / tot;
	resp_time = time_el / ends;
	throughput = (double) THREADS__ / resp_time;

	non_transac_avg = (double) time_notx_snp / (double) numb_notx_snp;
	t_commit_avg = (double) time_commit_snp / (double) numb_commit_snp;
	aborts_avg = (double) time_aborts_snp / (double) numb_aborts_snp;
	avg_r_trans = (double) time_xact_snp / (double) numb_runs_snp;
	avg_r_serl = (double) time_serl / (double) numb_serl_snp;
	avg_r_xact = (double) time_total_snp / (double) numb_total_snp;
	r_init = (double) time_fill_snp / (double) numb_fill_snp;

	data_file = fopen("transac_avg.txt", "a");
	//	fseek(data_file, 0, SEEK_END);
	//	if (ftell(data_file) < 8) {
	//		// not created yet
	//		fprintf(data_file, "%12s, %12s, %12s, %12s, %12s, %12s, %12s, "
	//				"%12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, %12s, "
	//				"%12s\n", "PROB_READ", "D", "L", "THREADS", "BUDGET",
	//				"ABORTS", "COMMITS", "SERLS", "CONFLICTS", "CAPACITY", "OTHER",
	//				"AVG_C_TRANS", "AVG_R_ABORTS", "AVG_R_SERL", "AVG_R_XACT",
	//				"NON_TRANSAC", "COMMIT_OP_LAT", "TOTAL_XACTS");
	//	}

	fprintf(data_file, "%12.2f, %12d, %12d, %12d, %12d, %12e, %12e, "
			"%12e, %12e, %12e, %12e, %12e, %12e, %12e, %12e, %12e, %12e, "
			"%12e\n", 1.0 - WRITE_PROB__, D__, L__, THREADS__, BUDGET__,
			aborts, commits, serls, conflicts,
			capacities, other, avg_r_trans, aborts_avg, avg_r_serl,
			avg_r_xact, non_transac_avg, t_commit_avg,
			(double) EXPERIMENT_SAMPLES);

	fclose(data_file);

	printf("Avg tran-commit time: %lf\n", avg_r_trans);
	printf("Avg serl-commit time: %lf\n", avg_r_serl);
	printf("Avg xact-aborts time: %lf\n", aborts_avg);
	printf("Avg non-transac time: %lf\n", non_transac_avg);
	printf("Avg R complete xact : %lf\n", avg_r_xact);
	printf("Avg _xend() time    : %lf\n", t_commit_avg);
	printf("Avg init        time: %lf\n", r_init);
	printf("throughput          : %lf\n", throughput);
	printf("prob_commit         : %lf\n", prob_commit);
	printf("EXPLICIT aborts     : %i\n", explicit);

	printf("======================== TESTS  ENDED ========================\n\n");

	pthread_mutex_destroy(&global_lock);
	pthread_exit(NULL);

	return 0;
}
