#!/bin/bash
gcc -O0 -mhtm ./capacity/P8_max_writes.c -o assoc.out -lpthread -I. -DPLOT_FILE="\"mem_cap_different_cores.plot\"" ;

for a in {1..30}
do
  for t in {1..500}
  do
    for e in 64
    do
      nice -n 19 ./assoc.out $t $e ;
    done
  done
done
