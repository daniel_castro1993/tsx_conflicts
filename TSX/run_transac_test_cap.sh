#!/bin/bash
gcc -g -O0 -mrtm ./conflicts/xact_test_vs_sim.c -o xact_test_vs_sim.out -lpthread -lrt -I. -DEXPERIMENT_SAMPLES=50000 -DACCESS_OFFSET=8 -DTHREAD_OFFSET=28
for i in {0..1000}
do
  for p in 1
  do
    for d in 10000
    do
      for l in 2 4 6 8 10 12 14 16 18 20 25 30 35 40 45 50 55 60 64
      do
        for t in 4 
        do
          for b in 5
          do
            timeout 1m nice -n 19 ./xact_test_vs_sim.out THREADS $t BUDGET $b D $d WRITE_PROB $p L $l ;
          done;
        done;
      done;
    done;
  done;
  wait ;
done




