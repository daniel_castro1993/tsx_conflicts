#include "rdtsc.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main () {
	unsigned long i, ts0, ts1, sum;

	for ( i = 0; i < 10; ++i) {
		ts0 = rdtsc();
		sleep(1);
		ts1 = rdtsc();
		sum += ts1 - ts0;
	}

	printf("cycles in 1s: %f\n", (double) sum / 10.0);

	return EXIT_SUCCESS;
}
