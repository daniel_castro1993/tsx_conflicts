#include "tm.h"

int *pool;

int main () {
	int status;
	int i, j, k;

	pool = (int*) malloc(sizeof (int) * 1000);

	// writes somewhere in the heap
	for (i = 0; i < 40; ++i) {
		pool[i] = i + 10;
	}

	asm volatile ("clflush (%0)" ::"r"(&(pool[10]))); // flushes and invalidate
	asm volatile ("clflush (%0)" ::"r"(&(pool[20]))); // flushes and invalidate
	asm volatile ("clflush (%0)" ::"r"(&(pool[30]))); // flushes and invalidate

	asm volatile ("mfence" :: : "memory");

	/*i = pool[10];
	j = pool[20];
	k = pool[30];
    
	i = j = k = 0;*/

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		printf("Aborted due to: ");

		if (status == _XABORT_CONFLICT) {
			printf("conflict!\n");
		} else if (status == _XABORT_CAPACITY) {
			printf("capacity!\n");
		} else {
			printf("other...\n");
		}

		return EXIT_FAILURE;
	}

	i = pool[10];
	j = pool[20];
	k = pool[30];

	asm volatile ("invd (%0)" ::"r"(&(pool[10])));
	asm volatile ("invd (%0)" ::"r"(&(pool[20])));
	asm volatile ("invd (%0)" ::"r"(&(pool[30])));

	asm volatile ("mfence" :: : "memory");
	// __sync_synchronize(); // same as before?

	_xend();

	printf("end 0=%i, 1=%i, 2=%i\n", pool[0], pool[1], pool[2]);

	free(pool);

	return EXIT_SUCCESS;
}
