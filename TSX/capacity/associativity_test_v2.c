#include "tm.h"

#define LARGE_NUMBER  0xFFFF

#ifndef NWAY_ACCESSES
#define NWAY_ACCESSES 9
#endif

/**
 * One transaction tries to write more memory
 * than allowed by the associativity.
 */

padded_scalar_t* mem_pool;
padded_scalar_t * addr_X;
padded_scalar_t * addr_X_[NWAY_ACCESSES];

void * W(void * arg) {
	register int j, status;

	// LOADS INTO CACHE
	addr_X->counter = 0;
	addr_X->counter2 = 0;
	for (j = 0; j < NWAY_ACCESSES; ++j) {
		addr_X_[j]->counter = 0;
		addr_X_[j]->counter2 = 0;
	}

	status = _xbegin();
	if (status != _XBEGIN_STARTED) {
		TSX_ERROR_INC(status, errors_1);
		pthread_exit(NULL);
	}

	addr_X->counter++;
	addr_X->counter2++;

	for (j = 0; j < NWAY_ACCESSES; ++j) {
		addr_X_[j]->counter++;
		addr_X_[j]->counter2++;
	}

	_xend();

	TSX_ERROR_INC(status, errors_1);
	pthread_exit(NULL);
}

int main() {
	pthread_t thread_w;
	cpu_set_t cpuset;

	int i, rc;
	void * res;

	mem_pool = (padded_scalar_t*) malloc(sizeof (padded_scalar_t) * LARGE_NUMBER);

	padded_scalar_t * begin_pool = (padded_scalar_t*) mem_pool,
			* end_pool = begin_pool + LARGE_NUMBER,
			* it;
	unsigned long mask = SET_MASK | WAY_MASK, addr_X_masked;
	unsigned long inc_addr = mask >> LINE_MASK;

	printf("USING ASSOC=%i\n\n", NWAY_ACCESSES);

	addr_X = begin_pool;
	printf("addr_X=[%p] ", addr_X);
	addr_X->counter = 0xff;

	addr_X_masked = mask & (unsigned long) addr_X;

	i = 0;
	for (it = end_pool - 1; it >= begin_pool; --it) {
		if (((unsigned long) it & mask) == addr_X_masked) {
			printf("addr_X_[%i]=[%p] ", i, it);
			addr_X_[i] = it;
			addr_X_[i]->counter = 0xff;
			i++;
			if (i == NWAY_ACCESSES) {
				break;
			}
		}
	}

	printf("\n\n");

	CPU_ZERO(&cpuset);
	CPU_SET(0, &cpuset);

	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpu_set_t), &cpuset);


	CPU_ZERO(&cpuset);
	CPU_SET(1, &cpuset);
	pthread_setaffinity_np((pthread_t) & thread_w, sizeof (cpuset), &cpuset);

	for (i = 0; i < EXPERIMENT_SAMPLES; i++) {
		pthread_create(&thread_w, NULL, W, (void *) 1);
		pthread_join(thread_w, &res);
	}

	check_file();
	fprintf(fp, "W1: ");
	for (i = 0; i < ERRORS_T_SIZE; ++i) {
		fprintf(fp, "%s=[ %i/ %.3f%%] ", TSX_ERROR_INDEX_STR(i),
				errors_1[i], (double) errors_1[i] / (double) EXPERIMENT_SAMPLES * 100.0);
	}
	fprintf(fp, "\n\n");
	fclose(fp);

	/* Last thing that main() should do */
	pthread_exit(NULL);

	return 0;
}
