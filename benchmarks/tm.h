#ifndef TM_H_GUARD
#define TM_H_GUARD

#include "rdtsc.h"

#if USE_P8 == 1
//#pragma message ( "USING_P8" )
#include <htmxlintrin.h>
#else
//#pragma message ( "USING_TSX" )
#include <immintrin.h>
#include <rtmintrin.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifndef CACHE_LINE_SIZE
#if USE_P8 == 1
#define CACHE_LINE_SIZE 128
#else
#define CACHE_LINE_SIZE 64
#endif
#endif /* CACHE_LINE_SIZE */

#ifndef THREAD_SPACING
#define THREAD_SPACING 1
#endif /* THREAD_SPACING */

#ifndef THREAD_OFFSET
#define THREAD_OFFSET 0
#endif /* THREAD_OFFSET */

    /**
     * _XBEGIN_STARTED    -1
     * _XABORT_EXPLICIT   0x01
     * _XABORT_RETRY      0x02
     * _XABORT_CONFLICT   0x04
     * _XABORT_CAPACITY   0x08
     * _XABORT_DEBUG      0x10
     * _XABORT_NESTED     0x20
     */

#if USE_POSIX == 1
#define ALLOC_FN(ptr, type, size) posix_memalign((void **) &ptr, CACHE_LINE_SIZE, (size) * CACHE_LINE_SIZE)
#else
#define ALLOC_FN(ptr, type, size) ptr = ((type*) aligned_alloc(CACHE_LINE_SIZE, (size) * CACHE_LINE_SIZE))
#endif

#ifdef DISABLE_LOCK 
#define TM_CHECK_GLOBAL_LOCK(is_in_tx) 
    //#define TM_ENTER_FALLBACK(tid) 
    //#define TM_EXIT_FALLBACK(tid) 
#else
#define TM_CHECK_GLOBAL_LOCK(is_in_tx) TM_check_global_lock(is_in_tx)
#endif
#define TM_ENTER_FALLBACK(tid) TM_enter_fallback(tid)
#define TM_EXIT_FALLBACK(tid) TM_exit_fallback(tid)

#if USE_P8 == 1

    typedef enum {
        SUCCESS = 0, ABORT, EXPLICIT, ILLEGAL, CONFLICT, CAPACITY,
        NESTING_DEPTH, NESTED_TOO_DEEP, OTHER, FALLBACK, PERSISTENT
    } errors_t;
#define SIZEOF_ERRORS_T 11
#define TM_STATUS_TYPE TM_buff_type
#define START_TRANSACTION(var) (__TM_begin(var))
#define CODE_SUCCESS _HTM_TBEGIN_STARTED
#define TM_ABORT() __TM_abort()
#define TM_COMMIT() __TM_end()
#else

    typedef enum {
        SUCCESS = 0, ABORT, EXPLICIT, RETRY, CONFLICT,
        CAPACITY, DEBUG, NESTED, OTHER, FALLBACK
    } errors_t;
#define SIZEOF_ERRORS_T 10
#define TM_STATUS_TYPE register int
#define START_TRANSACTION(var) (var = _xbegin())
#define CODE_SUCCESS _XBEGIN_STARTED
#define TM_ABORT() _xabort(30)
#define TM_COMMIT() _xend()
#endif

#define TM_begin(tid) \
do { \
    register int TM_budget_var = TM_get_budget(); \
    TM_STATUS_TYPE TM_status_var; \
    while (1) { \
        if (TM_budget_var > 0) { \
            TM_CHECK_GLOBAL_LOCK(0); \
            if (START_TRANSACTION(TM_status_var) != CODE_SUCCESS) { \
                TM_budget_var = TM_update_budget(tid, TM_budget_var, TM_status_var); \
                continue; \
            } \
            TM_CHECK_GLOBAL_LOCK(1); \
        } \
        else { \
            TM_ENTER_FALLBACK(tid); \
        }


#define TM_commit(tid) \
    if (TM_budget_var > 0) { \
            TM_COMMIT(); \
            TM_update_budget(tid, TM_budget_var, TM_status_var); \
        } \
        else { \
            TM_EXIT_FALLBACK(tid); \
        } \
        break; \
    } \
} \
while (0);

#define TM_begin_time_counters(tid, nb_aborts, time_aborts, time_commit, \
    time_xend, time_xbegin, time_fallback, ts_notx, time_notx) \
do { \
    register int TM_budget_var = TM_get_budget(); \
    TM_STATUS_TYPE TM_status_var; \
    /* unsigned long long TM_ts5_var; \
    TM_ts5_var = rdtsc(); \
    if(ts_notx != 0) time_notx += TM_ts5_var - ts_notx; */ \
    while (1) { \
        register unsigned long long TM_ts1_var, TM_ts2_var; \
        unsigned long long TM_ts3_var, TM_ts4_var; /* fall-back*/ \
        if (TM_budget_var > 0) { \
            TM_CHECK_GLOBAL_LOCK(0); \
            TM_ts1_var = rdtsc(); \
            if (START_TRANSACTION(TM_status_var) != CODE_SUCCESS) { \
                /* TM_ts2_var = rdtsc(); \
                nb_aborts++; \
                time_aborts += TM_ts2_var - TM_ts1_var; */ \
                TM_budget_var = TM_update_budget(tid, TM_budget_var, TM_status_var); \
                continue; \
            } \
            /* TM_ts2_var = rdtsc(); */ \
            TM_CHECK_GLOBAL_LOCK(1); \
        } \
        else { \
            /* TM_ts3_var = rdtsc(); \
            TM_ENTER_FALLBACK(tid); */ \
        }

#define TM_commit_time_counters(tid, nb_aborts, time_aborts, time_commit, \
    time_xend, time_xbegin, time_fallback, ts_notx, time_notx) \
        if (TM_budget_var > 0) { \
            /* TM_ts3_var = rdtsc(); */ \
            TM_COMMIT(); \
            /* TM_ts4_var = rdtsc(); */ \
            time_commit += TM_ts4_var - TM_ts1_var; \
            /* time_xend += TM_ts4_var - TM_ts3_var; */ \
            /* time_xbegin += TM_ts2_var - TM_ts1_var; */ \
            TM_update_budget(tid, TM_budget_var, TM_status_var); \
        } \
        else { \
            /* TM_EXIT_FALLBACK(tid); \
            TM_ts4_var = rdtsc(); */ \
            time_fallback += TM_ts4_var - TM_ts3_var; \
        } \
        break; \
    } \
    /* ts_notx = rdtsc(); */ \
    \
} \
while (0);

    int TM_exec(int nb_threads, int budget, int D, void *(*thread_routine) (void *));
    int TM_get_budget();
    void TM_set_budget(int budget);
    int TM_update_budget(int tid, int budget, TM_STATUS_TYPE status);
    void TM_set_record_errors(int tid, int is_record);
    int TM_get_error_count(int error);
    // don't call these
    // --

    void TM_block();
    int TM_read_lock();

#define TM_check_global_lock(is_in_tx) ({ \
    if (TM_read_lock()) { \
        if (is_in_tx) { \
            TM_ABORT(); \
        } else { \
            TM_block(); \
        } \
    } \
})

    void TM_thr_init_rnd_array(int D);
    int TM_get_rand();

    void TM_enter_fallback(int tid);
    void TM_exit_fallback(int tid);

    // --

#define RAND_R_FNC(seed) ({ \
    register unsigned long next = seed; \
    register unsigned long result; \
    next *= 1103515245; \
    next += 12345; \
    result = (unsigned long) (next / 65536) % 2048; \
    next *= 1103515245; \
    next += 12345; \
    result <<= 10; \
    result ^= (unsigned long) (next / 65536) % 1024; \
    next *= 1103515245; \
    next += 12345; \
    result <<= 10; \
    result ^= (unsigned long) (next / 65536) % 1024; \
    seed = next; \
    result; \
})

#define IS_WRITE(pw, seed) ({\
    (int) ((((unsigned) RAND_R_FNC(seed)) % 99999) < (pw * 99999.0)); \
})

#define ALIGNED_READ(type, pool, index) ({ \
    (pool)[(index) * CACHE_LINE_SIZE / sizeof(type)]; \
})

#define ALIGNED_RANDOM_READ(type, pool, seed, d) ({ \
    unsigned rnd = RAND_R_FNC(seed); \
    (pool)[(rnd % d) * CACHE_LINE_SIZE / sizeof(type)]; \
})

#define ALIGNED_WRITE(type, pool, index, value) ({ \
    (pool)[index * CACHE_LINE_SIZE / sizeof(type)] = value; \
})

#define ALIGNED_INC(type, pool, index, inc) ({ \
    (pool)[(index)* CACHE_LINE_SIZE / sizeof(type)] += inc; \
})

#define ALIGNED_RANDOM_WRITE(type, pool, seed, d, value) ({ \
    unsigned rnd = RAND_R_FNC(seed); \
    (pool)[(rnd % d) * CACHE_LINE_SIZE / sizeof(type)] = value; \
})

#define ALIGNED_RANDOM_INC(type, pool, seed, d, inc) ({ \
    unsigned rnd = RAND_R_FNC(seed); \
    (pool)[(rnd % d) * CACHE_LINE_SIZE / sizeof(type)] += inc; \
})

#define TSX_ERROR_INC(status, error_array) ({ \
  if (status == _XBEGIN_STARTED) { \
    error_array[SUCCESS] += 1; \
  } else { \
    error_array[ABORT] += 1; \
    int nb_errors = __builtin_popcount(status); \
    int tsx_error = status; \
    do { \
      int idx = tsx_error & _XABORT_EXPLICIT ? \
        ({tsx_error = tsx_error & ~_XABORT_EXPLICIT; EXPLICIT;}) : \
      tsx_error & _XABORT_RETRY ? \
        ({tsx_error = tsx_error & ~_XABORT_RETRY; RETRY;}) : \
      tsx_error & _XABORT_CONFLICT ? \
        ({tsx_error = tsx_error & ~_XABORT_CONFLICT; CONFLICT;}) : \
      tsx_error & _XABORT_CAPACITY ? \
        ({tsx_error = tsx_error & ~_XABORT_CAPACITY; CAPACITY;}) : \
      tsx_error & _XABORT_DEBUG ? \
        ({tsx_error = tsx_error & ~_XABORT_DEBUG; DEBUG;}) : \
      OTHER; \
      error_array[idx] += 1; \
      nb_errors--; \
    } while (nb_errors > 0); \
  } \
})

#define P8_ERROR_TO_INDEX(tm_buffer) ({ \
  errors_t code = OTHER; \
  if(*_TEXASRL_PTR (tm_buffer) == 0) code = SUCCESS; \
  else if(__TM_is_conflict(tm_buffer)) code = CONFLICT; \
  else if(__TM_is_illegal  (tm_buffer)) code = ILLEGAL; \
  else if(__TM_is_footprint_exceeded(tm_buffer)) code = CAPACITY; \
  else if(__TM_nesting_depth(tm_buffer)) code = NESTING_DEPTH; \
  else if(__TM_is_nested_too_deep(tm_buffer)) code = NESTED_TOO_DEEP; \
  else if(__TM_is_user_abort(tm_buffer)) code = EXPLICIT; \
  else if(__TM_is_failure_persistent(tm_buffer)) code = PERSISTENT; \
  code; \
})
#define P8_ERROR_INC(status, error_array) ({ \
  errors_t code = P8_ERROR_TO_INDEX(status); \
  if (code != SUCCESS) error_array[ABORT] += 1; \
  error_array[code] += 1; \
})

#if USE_P8 == 1
#define ERROR_INC P8_ERROR_INC
#else
#define ERROR_INC TSX_ERROR_INC
#endif

#ifdef __cplusplus
}
#endif

#endif /* TM_H_GUARD */
